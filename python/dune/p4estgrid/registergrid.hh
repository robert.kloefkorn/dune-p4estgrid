#pragma once

#include <string>
#include <sstream>
#include <dune/common/tupleutility.hh>

#include <dune/p4estgrid/grid.hh>
#include <dune/p4estgrid/dgf.hh>
#include <dune/python/grid/hierarchical.hh>

template <int dim, int dimworld, Dune::P4estType elType>
void registerP4estGrid(pybind11::module module)
#ifdef INCLUDE_REGALUGRID_INLINE
{
  // add commonly used P4estGrid variants
  using pybind11::operator""_a;
  std::string modname = std::string("_p4estgrid_" + std::to_string(dim) + std::to_string(dimworld) + "_" + std::to_string(elType) + "_double");
  std::string descr("Precompiled ");
  descr += modname;
  pybind11::module cls0 = module.def_submodule( modname.c_str(), descr.c_str());
  {
    using DuneType = Dune::P4estGrid< dim, dimworld, elType, double >;
    std::string gridTypeName;
    {
      std::stringstream gridStr;
      gridStr << "Dune::P4estGrid< " << dim << ", " << dimworld << ", Dune::P4estType::" << std::to_string(elType) << ", double>";
      gridTypeName = gridStr.str();
    }

    auto cls = Dune::Python::insertClass< DuneType, std::shared_ptr<DuneType> >( cls0, "HierarchicalGrid",pybind11::dynamic_attr(),
        Dune::Python::GenerateTypeName(gridTypeName),
        Dune::Python::IncludeFiles{"dune/p4estgrid/grid.hh","dune/p4estgrid/dgf.hh","dune/python/grid/hierarchical.hh"}).first;
    Dune::Python::registerHierarchicalGrid( cls0, cls );
  }
}
#else
;
#endif
