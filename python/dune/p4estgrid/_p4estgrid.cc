#include <config.h>

#include <dune/python/pybind11/pybind11.h>

#ifdef DUNE_ENABLE_PYTHONMODULE_PRECOMPILE
#include "registergrid.hh"
#endif

PYBIND11_MODULE( _p4estgrid, module )
{
#ifdef DUNE_ENABLE_PYTHONMODULE_PRECOMPILE
  // pre-compiled objects for P4estGrid cube
  registerP4estGrid< 2, 2, Dune::P4estType::cube > ( module );
  registerP4estGrid< 3, 3, Dune::P4estType::cube > ( module );

  // pre-compiled objects for P4estGrid cartesian
  registerP4estGrid< 2, 2, Dune::P4estType::cartesian > ( module );
  registerP4estGrid< 3, 3, Dune::P4estType::cartesian > ( module );
#endif
}
