import logging
logger = logging.getLogger(__name__)

#-------------------------------------------------------------------
# grid module loading
def checkModule(includes, typeName, typeTag):
    from importlib import import_module

    # check if pre-compiled module exists and if so load it
    try:
        gridModule = import_module("dune.p4estgrid._p4estgrid._p4estgrid_" + typeTag)
        return gridModule
    except ImportError:
        from dune.grid.grid_generator import module
        # otherwise proceed with generate, compile, load
        gridModule = module(includes, typeName)
        return gridModule

# creates a P4estGrid instance
def _p4estGrid(constructor, dimgrid=None, dimworld=None, elementType=None, comm=None, ctype=None, verbose=False,
              **parameters):
    """
    Create an P4EstGrid instance.

    Note: This functions has to be called on all cores and the parameters passed should be the same.
          Otherwise unexpected behavior will occur.

    Parameters:
    -----------

        constructor  means of constructing the grid, i.e. a grid reader or a
                     dictionary holding macro grid information
        dimgrid      dimension of grid, i.e. 2 or 3
        dimworld     dimension of world, i.e. 2 or 3 and >= dimension
        elementType  type of element, e.g. 'cube', 'cartesian' or 'simplex' (simplex not yet supported)
        comm         MPI communication (not yet implemented)
        verbose      adds some verbosity output (default False)

    Returns:
    --------

    An P4EstGrid instance.
    """
    from dune.grid.grid_generator import getDimgrid

    if not dimgrid:
        dimgrid = getDimgrid(constructor)

    if dimworld is None:
        dimworld = dimgrid

    if elementType is None:
        elementType = parameters.pop("type")

    assert elementType == 'cube' or elementType == 'cartesian', "Unsupported element type (simplex not supported yet)"

    if ctype is None:
        ctype = 'double'

    if not (2 <= dimgrid and dimgrid <= dimworld):
        raise KeyError("Parameter error in P4estGrid with dimgrid=" + str(dimgrid) + ": dimgrid has to be either 2 or 3")
    if not (2 <= dimworld and dimworld <= 3):
        raise KeyError("Parameter error in P4estGrid with dimworld=" + str(dimworld) + ": dimworld has to be either 2 or 3")

    typeTag = str(dimgrid) + str(dimworld) + "_" + elementType + "_" + ctype
    typeName = "Dune::P4estGrid< " + str(dimgrid) + ", " + str(dimworld) + ", Dune::P4estType::" + elementType + ", " + ctype + ">"
    includes = ["dune/p4estgrid/grid.hh", "dune/p4estgrid/dgf.hh"]
    gridModule = checkModule(includes, typeName, typeTag)

    if comm is not None:
        raise Exception("Passing communicator to grid construction is not yet implemented in Python bindings of dune-grid")
        # return gridModule.LeafGrid(gridModule.reader(constructor, comm))

    gridView = gridModule.LeafGrid(gridModule.reader(constructor))

    # in case of a Cartesian domain store if old or new boundary ids was used
    # this can be removed in later version - it is only used in dune-fem
    # to give a warning that the boundary ids for the Cartesian domains have changed
    try:
        gridView.hierarchicalGrid._cartesianConstructionWithIds = constructor.boundaryWasSet
    except AttributeError:
        pass
    return gridView
#### end p4estGrid #####

# return cube (unstructured) version of p4estGrid
def p4estCubeGrid(*args, **kwargs):
    return _p4estGrid(*args, **kwargs, elementType="cube")
p4estCubeGrid.__doc__ = _p4estGrid.__doc__

# return Cartesian version of p4estGrid
def p4estCartesianGrid(*args, **kwargs):
    return _p4estGrid(*args, **kwargs, elementType="cartesian")
p4estCartesianGrid.__doc__ = _p4estGrid.__doc__


grid_registry = {
        "p4est"      : _p4estGrid,
    }

if __name__ == "__main__":
    import doctest
    doctest.testmod(optionflags=doctest.ELLIPSIS)
