#include <config.h>
#define INCLUDE_REGALUGRID_INLINE
#include "registergrid.hh"
#if not defined(DIM) || not defined(GEOMTYPE)
#error "DIM and GEOMTYPE need to be defined!"
#endif
template void registerP4estGrid<DIM, DIM, GEOMTYPE>(pybind11::module);
