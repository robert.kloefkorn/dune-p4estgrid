/* begin dune-p4estgrid
   put the definitions for config.h specific to
   your project here. Everything above will be
   overwritten
*/
/* begin private */
/* Name of package */
#define PACKAGE "@DUNE_MOD_NAME@"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "@DUNE_MAINTAINER@"

/* Define to the full name of this package. */
#define PACKAGE_NAME "@DUNE_MOD_NAME@"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "@DUNE_MOD_NAME@ @DUNE_MOD_VERSION@"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "@DUNE_MOD_NAME@"

/* Define to the home page for this package. */
#define PACKAGE_URL "@DUNE_MOD_URL@"

/* Define to the version of this package. */
#define PACKAGE_VERSION "@DUNE_MOD_VERSION@"

/* end private */


#define DUNE_P4ESTGRID_VERSION "${DUNE_P4ESTGRID_VERSION}"

/* Define to the major version of dune-alugrid */
#define DUNE_P4ESTGRID_VERSION_MAJOR ${DUNE_P4ESTGRID_VERSION_MAJOR}

/* Define to the minor version of dune-alugrid */
#define DUNE_P4ESTGRID_VERSION_MINOR ${DUNE_P4ESTGRID_VERSION_MINOR}

/* Define to the revision of dune-alugrid*/
#define DUNE_P4ESTGRID_VERSION_REVISION ${DUNE_P4ESTGRID_VERSION_REVISION}

/* Define if we have ZLIB */
#cmakedefine HAVE_ZLIB 1

/* Define if we have p4est has been found */
#cmakedefine HAVE_P4EST 1

/* Grid type magic for DGF parser */
@P4ESTGRID_CONFIG_H_BOTTOM@
/* end dune-p4estgrid */
