#make libdunep4estgrid known locally
#define available p4estgrid types
dune_define_gridtype(GRIDSELECTOR_GRIDS GRIDTYPE P4ESTGRID_CUBE
  DUNETYPE "Dune::P4estGrid< dimgrid, dimworld, Dune::P4estType::cube >"
    HEADERS dune/p4estgrid/grid.hh dune/p4estgrid/dgf.hh)

  dune_define_gridtype(GRIDSELECTOR_GRIDS GRIDTYPE P4ESTGRID_CARTESIAN
  DUNETYPE "Dune::P4estGrid< dimgrid, dimworld, Dune::P4estType::cartesian >"
    HEADERS dune/p4estgrid/grid.hh dune/p4estgrid/dgf.hh)

# for P4estGrid module we write a separate grid selector file to avoid
# dependencies of the library files to all headers, for all other module
# the grid selection defs are written to config.h
if(DUNE_GRID_GRIDTYPE_SELECTOR AND P4ESTGRID_EXTRA_GRIDSELECTOR_FILE)
  file(WRITE "${CMAKE_BINARY_DIR}/gridselector.hh" "#include <config.h>\n${GRIDSELECTOR_GRIDS}")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -include${CMAKE_BINARY_DIR}/gridselector.hh")
else()
  set(P4ESTGRID_CONFIG_H_BOTTOM "${P4ESTGRID_CONFIG_H_BOTTOM} ${GRIDSELECTOR_GRIDS}")
endif()


if (ENABLE_GRID_SELECTOR)
  set(P4EST_CONFIG_H_BOTTOM "${P4EST_CONFIG_H_BOTTOM} ${GRIDSELECTOR_GRIDS}")
endif (ENABLE_GRID_SELECTOR)

# contained in cmake system modules
find_package(ZLIB)
#set HAVE_ZLIB for config.h
set(HAVE_ZLIB ${ZLIB_FOUND})
if(ZLIB_FOUND)
  dune_register_package_flags(INCLUDE_DIRS ${ZLIB_INCLUDE_DIR} LIBRARIES ${ZLIB_LIBRARIES})
endif()

find_package(LAPACK)
find_package(BLAS)

# set P4EST_ROOT from environment variable if set
# -DP4EST_ROOT overrules the env variable
if( NOT P4EST_ROOT )
  set(P4EST_ROOT $ENV{P4EST_ROOT})
endif()

find_package(p4est REQUIRED)
