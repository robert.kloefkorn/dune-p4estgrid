find_package(LAPACK)
find_package(BLAS)

if(NOT p4est_ROOT AND P4EST_ROOT)
  set(p4est_ROOT ${P4EST_ROOT})
  message(WARNING "Using deprecated P4EST_ROOT. Change variable name to p4est_ROOT!")
endif()


macro(_search_p4est_lib libvar libname doc)
  find_library(${libvar} ${libname}
    PATHS ${p4est_ROOT} ${p4est_ROOT}/lib PATH_SUFFIXES ${PATH_SUFFIXES}
    NO_DEFAULT_PATH
    DOC "${doc}")
  find_library(${libvar} ${libname})
endmacro(_search_p4est_lib)

find_path(P4EST_INCLUDE_DIR p4est.h
  PATHS ${p4est_ROOT} ${p4est_ROOT}/include
  PATH_SUFFIXES ${PATH_SUFFIXES}
  NO_DEFAULT_PATH
  DOC "Include directory of P4EST")
find_path(P4EST_INCLUDE_DIR p4est.h
  PATH_SUFFIXES ${PATH_SUFFIXES})

include(CMakePushCheckState)
cmake_push_check_state() # Save variables

_search_p4est_lib(P4EST_LIBRARY p4est "The main p4est library.")
_search_p4est_lib(SC_LIBRARY sc "The SC library.")

# behave like a CMake module is supposed to behave
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  "p4est"
  DEFAULT_MSG
  P4EST_INCLUDE_DIR
  P4EST_LIBRARY
  SC_LIBRARY
)
#restore old values
cmake_pop_check_state()

if(p4est_FOUND)
  set(P4EST_INCLUDE_DIRS ${P4EST_INCLUDE_DIR})
  set(P4EST_LIBRARIES ${P4EST_LIBRARY} ${SC_LIBRARY} ${MPI_DUNE_LIBRARIES}
    CACHE FILEPATH "All libraries needed to link programs using p4est")
  set(P4EST_LINK_FLAGS "${DUNE_MPI_LINK_FLAGS}"
    CACHE STRING "p4est link flags")
  set(HAVE_P4EST 1)

  message("Register p4est ${P4EST_INCLUDE_DIRS} ${P4EST_LIBRARIES}")
  dune_register_package_flags(LIBRARIES "${P4EST_LIBRARIES}"
                              INCLUDE_DIRS "${P4EST_INCLUDE_DIRS}")
endif()

#mark_as_advanced(P4EST_INCLUDE_DIRS P4EST_LIBRARIES HAVE_P4EST)
