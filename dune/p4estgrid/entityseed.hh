/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_ENTITYSEED_HH
#define DUNE_P4ESTGRID_ENTITYSEED_HH

#include <dune/common/typetraits.hh>

#include <dune/p4estgrid/p4est.hh>
#include <dune/p4estgrid/entityinfo.hh>

namespace Dune
{
  template< int codim, class Grd >
  class P4estGridEntitySeed
  {
    typedef typename std::remove_const< Grd >::type::Traits Traits;

    typedef Dune :: P4est< Grd :: dimension, Grd :: dimensionworld > PForest ;
    typedef typename PForest :: qcoord_t qcoord_t;
    typedef typename PForest :: topidx_t topidx_t;
  public:
    static const int codimension = codim;
    static const int dimension = Traits::dimension;
    static const int mydimension = dimension - codimension;
    static const int dimensionworld = Traits::dimensionworld;

    typedef typename Traits::Grid Grid;
    typedef typename Traits::template Codim< codim >::Entity Entity;

    typedef P4estEntityInfo< dimension > EntityInfo ;

    P4estGridEntitySeed ()
     :  info_()
        //x_(-1), y_(-1), z_(-1), level_(-1),
        //which_tree_( -1 )
    {}

    P4estGridEntitySeed ( const EntityInfo& info )
      /*topidx_t which_tree,
                          const qcoord_t x, const qcoord_t y,
                          const qcoord_t z, const int8_t level )
     :  x_( x ), y_( y ), z_( z ),
        level_ ( level ),
        which_tree_( which_tree )
        */
      : info_( info )
    {}

    /*
    P4estGridEntitySeed ( const EntityInfo& info )
     :  x_( info.x() ), y_( info.y() ), z_( info.z() ),
        level_ ( info.level() ),
        which_tree_( info.treeId() )
    {}
    */

    const EntityInfo& info() const { return info_; }

    bool isValid() const
    {
      return info_.valid();
      //return x_ != -1 && which_tree_ != -1;
    }

  private:
    EntityInfo info_;
    /*
    qcoord_t x_;
    qcoord_t y_;
    qcoord_t z_;
    int8_t level_;
    topidx_t which_tree_;
    */
  };

} // namespace Dune

#endif // #ifndef DUNE_P4ESTGRID_ENTITYSEED_HH
