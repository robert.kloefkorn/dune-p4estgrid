/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4EST_HH
#define DUNE_P4EST_HH

#include <type_traits>

extern "C" {

  #include <p4est.h>
  #include <p4est_base.h>
  #include <p4est_mesh.h>
  #include <p4est_ghost.h>
  #include <p4est_dune.h>
  #include <p4est_bits.h>
  #include <p4est_extended.h>
  #include <p4est_communication.h>

  #include <p8est.h>
  #include <p8est_mesh.h>
  #include <p8est_ghost.h>
  #include <p8est_dune.h>
  #include <p8est_bits.h>
  #include <p8est_extended.h>
  #include <p8est_communication.h>
}

namespace Dune
{
  template < int dim, int dimworld >
  struct P4est;

  struct P4estBaseTraits
  {
    typedef p4est_gloidx_t        gloidx_t;
    typedef p4est_topidx_t        topidx_t;
    typedef p4est_locidx_t        quadidx_t;
    typedef int8_t                level_t;
    typedef p4est_locidx_t        locidx_t ;
    typedef uint64_t              linearid_t ;
    typedef int                   adaptationmarker_t ;
    typedef std::vector< adaptationmarker_t > markervector_t;

#if HAVE_MPI
    typedef MPI_Comm  comm_t ;
#else
    typedef No_Comm   comm_t ;
#endif
  };

  template < int dim, int dimworld >
  struct P4estTraits;

  ////////////////////////////////////////////////////////////////
  //
  //  p4est specific typedefs for 2 dimensions
  //
  ////////////////////////////////////////////////////////////////
  template <>
  struct P4estTraits< 2, 2 > : public P4estBaseTraits
  {
    enum { Q_MAXLEVEL = P4EST_QMAXLEVEL };
    enum { CONNECT    = P4EST_CONNECT_FACE }; // connectivity over faces (previously FULL)
    static const int NUMCHILDREN  = 4;
    static const int dim = 2;
    static const int dimworld = 2;
    typedef p4est_t               forest_t ;
    typedef p4est_mesh_t          mesh_t ;
    typedef p4est_ghost_t         ghost_t ;
    typedef p4est_dune_numbers_t  numbers_t;
    typedef p4est_connect_type_t  connect_type_t ;
    typedef p4est_connectivity_t  connectivity_t;
    typedef p4est_transfer_context_t  transfer_context_t;
    typedef p4est_init_t          init_t;
    typedef p4est_qcoord_t        qcoord_t;
    typedef p4est_quadrant_t      quadrant_t;
    typedef p4est_tree_t          tree_t ;
    typedef typename P4estBaseTraits :: topidx_t  topidx_t;
    typedef typename P4estBaseTraits :: quadidx_t  quadidx_t;
    typedef typename P4estBaseTraits :: level_t    level_t;
    typedef typename P4estBaseTraits :: comm_t     comm_t ;
    typedef typename P4estBaseTraits :: linearid_t linearid_t;
    typedef p4est_mesh_face_neighbor_t  mesh_face_neighbor_t;
    typedef p4est_ghost_exchange_t ghost_exchange_t;

    //////////////////////////////////////////////////////////////////
    //
    // p4est specific functions
    //
    //////////////////////////////////////////////////////////////////
    static constexpr auto quadrant_linear_id = p4est_quadrant_linear_id;

    static constexpr auto _new_ext = p4est_new_ext;
    static constexpr auto _forest_copy = p4est_copy;
    static constexpr auto _mesh_new_ext = p4est_mesh_new_ext;

    // destroy a p-forest
    static constexpr auto forest_destroy = p4est_destroy;

    // create a unitcube
    static constexpr auto connectivity_new_unitcube = p4est_connectivity_new_unitsquare;
    static constexpr auto connectivity_is_valid     = p4est_connectivity_is_valid;
    static constexpr auto connectivity_bcast        = p4est_connectivity_bcast;
    static constexpr auto _connectivity_new_copy     = p4est_connectivity_new_copy;

    static constexpr auto tree_array_index = p4est_tree_array_index;
    static constexpr auto quadrant_array_index = p4est_quadrant_array_index;
    static constexpr auto mesh_destroy = p4est_mesh_destroy;

    static constexpr auto _ghost_new    = p4est_ghost_new;
    static constexpr auto ghost_destroy = p4est_ghost_destroy;

    static constexpr auto _numbers_new    = p4est_dune_numbers_new;
    static constexpr auto numbers_destroy = p4est_dune_numbers_destroy;

    static constexpr auto _balance = p4est_balance;
    static constexpr auto _partition_ext = p4est_partition_ext;
    static constexpr auto _refine      = p4est_refine;
    static constexpr auto _coarsen_ext = p4est_coarsen_ext;

    static constexpr auto ghost_exchange_custom_begin = p4est_ghost_exchange_custom_begin;
    static constexpr auto ghost_exchange_custom_end   = p4est_ghost_exchange_custom_end;

    static constexpr auto _transfer_fixed_begin = p4est_transfer_fixed_begin;
    static constexpr auto transfer_fixed_end    = p4est_transfer_fixed_end;

    // destroy connectivity
    static constexpr auto connectivity_destroy = p4est_connectivity_destroy;
    static constexpr auto quadrant_child_id = p4est_quadrant_child_id;

    static constexpr auto quadrant_ancestor  = p4est_quadrant_ancestor;
    static constexpr auto quadrant_parent    = p4est_quadrant_parent;
    static constexpr auto quadrant_is_parent = p4est_quadrant_is_parent;

    static constexpr auto _mesh_face_neighbor_init = p4est_mesh_face_neighbor_init;
    static constexpr auto _mesh_face_neighbor_next = p4est_mesh_face_neighbor_next;

    static constexpr auto _memory_used              = p4est_memory_used;
    static constexpr auto _connectivity_memory_used = p4est_connectivity_memory_used;
    static constexpr auto _ghost_memory_used        = p4est_ghost_memory_used;
    static constexpr auto _mesh_memory_used         = p4est_mesh_memory_used;
  };

  ////////////////////////////////////////////////////////////////
  //
  //  p8est specific typedefs for 3 dimensions
  //
  ////////////////////////////////////////////////////////////////
  template <>
  struct P4estTraits< 3, 3 > : public P4estBaseTraits
  {
    enum { Q_MAXLEVEL = P8EST_QMAXLEVEL };
    enum { CONNECT    = P8EST_CONNECT_FACE }; // connectivity over faces (previously FULL)
    static const int NUMCHILDREN = 8;
    static const int dim = 3;
    static const int dimworld = 3;
    typedef p8est_t               forest_t ;
    typedef p8est_mesh_t          mesh_t ;
    typedef p8est_ghost_t         ghost_t ;
    typedef p8est_dune_numbers_t  numbers_t;
    typedef p8est_connect_type_t  connect_type_t ;
    typedef p8est_connectivity_t  connectivity_t;
    typedef p8est_transfer_context_t  transfer_context_t;
    typedef p8est_init_t          init_t;
    typedef p4est_qcoord_t        qcoord_t;
    typedef p8est_quadrant_t      quadrant_t;
    typedef p8est_tree_t          tree_t ;
    typedef typename P4estBaseTraits :: topidx_t topidx_t;
    typedef typename P4estBaseTraits :: quadidx_t  quadidx_t;
    typedef typename P4estBaseTraits :: level_t    level_t;
    typedef typename P4estBaseTraits :: comm_t     comm_t ;
    typedef typename P4estBaseTraits :: linearid_t linearid_t;
    typedef p8est_mesh_face_neighbor_t  mesh_face_neighbor_t;
    typedef p8est_ghost_exchange_t ghost_exchange_t;

    //////////////////////////////////////////////////////////////////
    //
    // p8est specific functions
    //
    //////////////////////////////////////////////////////////////////
    static constexpr auto quadrant_linear_id = p8est_quadrant_linear_id;

    static constexpr auto _new_ext = p8est_new_ext;
    static constexpr auto _forest_copy = p8est_copy;
    static constexpr auto _mesh_new_ext = p8est_mesh_new_ext;

    // destroy a p-forest
    static constexpr auto forest_destroy = p8est_destroy;

    // create a unitcube
    static constexpr auto connectivity_new_unitcube = p8est_connectivity_new_unitcube;
    static constexpr auto connectivity_is_valid     = p8est_connectivity_is_valid;
    static constexpr auto connectivity_bcast        = p8est_connectivity_bcast;
    static constexpr auto _connectivity_new_copy     = p8est_connectivity_new_copy;

    static constexpr auto tree_array_index = p8est_tree_array_index;
    static constexpr auto quadrant_array_index = p8est_quadrant_array_index;
    static constexpr auto mesh_destroy = p8est_mesh_destroy;

    static constexpr auto _ghost_new    = p8est_ghost_new;
    static constexpr auto ghost_destroy = p8est_ghost_destroy;

    static constexpr auto _numbers_new    = p8est_dune_numbers_new;
    static constexpr auto numbers_destroy = p8est_dune_numbers_destroy;

    static constexpr auto _balance = p8est_balance;
    static constexpr auto _partition_ext = p8est_partition_ext;
    static constexpr auto _refine      = p8est_refine;
    static constexpr auto _coarsen_ext = p8est_coarsen_ext;
    static constexpr auto ghost_exchange_custom_begin = p8est_ghost_exchange_custom_begin;
    static constexpr auto ghost_exchange_custom_end   = p8est_ghost_exchange_custom_end;
    static constexpr auto _transfer_fixed_begin = p8est_transfer_fixed_begin;
    static constexpr auto transfer_fixed_end    = p8est_transfer_fixed_end;

    // destroy connectivity
    static constexpr auto connectivity_destroy = p8est_connectivity_destroy;
    static constexpr auto quadrant_child_id    = p8est_quadrant_child_id;

    static constexpr auto quadrant_ancestor  = p8est_quadrant_ancestor;
    static constexpr auto quadrant_parent    = p8est_quadrant_parent;
    static constexpr auto quadrant_is_parent = p8est_quadrant_is_parent;

    static constexpr auto _mesh_face_neighbor_init = p8est_mesh_face_neighbor_init;
    static constexpr auto _mesh_face_neighbor_next = p8est_mesh_face_neighbor_next;

    static constexpr auto _memory_used              = p8est_memory_used;
    static constexpr auto _connectivity_memory_used = p8est_connectivity_memory_used;
    static constexpr auto _ghost_memory_used        = p8est_ghost_memory_used;
    static constexpr auto _mesh_memory_used         = p8est_mesh_memory_used;
  };

  template <int dim, int dimworld >
  struct P4est : public P4estTraits< dim, dimworld >
  {
    typedef P4estTraits< dim, dimworld > BaseType;

    ////////////////////////////////////////////////////////////////
    //
    //  p4est specific typedefs
    //
    ////////////////////////////////////////////////////////////////
    static const int Q_MAXLEVEL   = BaseType::Q_MAXLEVEL;
    static const int CONNECT      = BaseType::CONNECT;
    static const int NUMCHILDREN  = BaseType::NUMCHILDREN;
    enum { NONE = 0, REFINE = 1, COARSEN = -1, NEW = -111 };
    typedef typename BaseType :: gloidx_t  gloidx_t;
    typedef typename BaseType :: topidx_t  topidx_t;
    typedef typename BaseType :: quadidx_t quadidx_t;
    typedef typename BaseType :: level_t  level_t;
    typedef typename BaseType :: locidx_t locidx_t;
    typedef typename BaseType :: linearid_t linearid_t;
    typedef int  adaptationmarker_t ;
    typedef std::vector< adaptationmarker_t > markervector_t;

    typedef typename BaseType :: comm_t  comm_t;

    typedef typename BaseType :: forest_t forest_t;
    typedef typename BaseType :: mesh_t mesh_t;
    typedef typename BaseType :: ghost_t ghost_t;
    typedef typename BaseType :: numbers_t numbers_t;
    typedef typename BaseType :: connect_type_t connect_type_t;
    typedef typename BaseType :: connectivity_t connectivity_t;
    typedef typename BaseType :: transfer_context_t transfer_context_t;

    typedef typename BaseType :: init_t init_t;
    typedef typename BaseType :: qcoord_t    qcoord_t;
    typedef typename BaseType :: quadrant_t  quadrant_t;
    typedef typename BaseType :: tree_t      tree_t ;
    typedef typename BaseType :: mesh_face_neighbor_t  mesh_face_neighbor_t;
    typedef typename BaseType :: ghost_exchange_t ghost_exchange_t;

    //////////////////////////////////////////////////////////////////
    //
    // p4est specific functions
    //
    //////////////////////////////////////////////////////////////////

  protected:
    // functions with underscore are protected because the there is an
    // overloaded function without underscore
    static constexpr auto _new_ext       = BaseType :: _new_ext ;
    static constexpr auto _forest_copy   = BaseType :: _forest_copy;
    static constexpr auto _mesh_new_ext  = BaseType :: _mesh_new_ext;
    static constexpr auto _ghost_new     = BaseType :: _ghost_new;

    static constexpr auto _numbers_new   = BaseType :: _numbers_new;

    static constexpr auto _balance       = BaseType :: _balance;
    static constexpr auto _partition_ext = BaseType :: _partition_ext;
    static constexpr auto _refine        = BaseType :: _refine;
    static constexpr auto _coarsen_ext   = BaseType :: _coarsen_ext;

    static constexpr auto _mesh_face_neighbor_init = BaseType :: _mesh_face_neighbor_init;
    static constexpr auto _mesh_face_neighbor_next = BaseType :: _mesh_face_neighbor_next;
    static constexpr auto _transfer_fixed_begin    = BaseType :: _transfer_fixed_begin;

    static constexpr auto _memory_used              = BaseType :: _memory_used;
    static constexpr auto _connectivity_memory_used = BaseType :: _connectivity_memory_used;
    static constexpr auto _ghost_memory_used        = BaseType :: _ghost_memory_used;
    static constexpr auto _mesh_memory_used         = BaseType :: _mesh_memory_used;
  public:
    static constexpr auto transfer_fixed_end     = BaseType :: transfer_fixed_end;
    static constexpr auto ghost_exchange_custom_begin = BaseType :: ghost_exchange_custom_begin;
    static constexpr auto ghost_exchange_custom_end   = BaseType :: ghost_exchange_custom_end;

    static constexpr auto quadrant_linear_id     = BaseType :: quadrant_linear_id;
    // destroy a p-forest
    static constexpr auto forest_destroy         = BaseType :: forest_destroy;

    // create a unitcube
    static constexpr auto connectivity_new_unitcube = BaseType :: connectivity_new_unitcube;
    static constexpr auto _connectivity_new_copy = BaseType :: _connectivity_new_copy;

    static constexpr auto tree_array_index     = BaseType :: tree_array_index;
    static constexpr auto quadrant_array_index = BaseType :: quadrant_array_index;
    static constexpr auto mesh_destroy         = BaseType :: mesh_destroy;

    static constexpr auto ghost_destroy        = BaseType :: ghost_destroy;
    static constexpr auto numbers_destroy      = BaseType :: numbers_destroy;

    // destroy connectivity
    static constexpr auto connectivity_destroy = BaseType :: connectivity_destroy;
    static constexpr auto quadrant_child_id    = BaseType :: quadrant_child_id;

    static constexpr auto quadrant_ancestor    = BaseType :: quadrant_ancestor;
    static constexpr auto quadrant_parent      = BaseType :: quadrant_parent;
    static constexpr auto quadrant_is_parent   = BaseType :: quadrant_is_parent;

    static void initVerbosity()
    {
      // SC_LP_DEFAULT   (-1)    /* this selects the SC default threshold */
      // SC_LP_ALWAYS      0     /* this will log everything */
      // SC_LP_TRACE       1     /* this will prefix file and line number */
      // SC_LP_DEBUG       2     /* any information on the internal state */
      // SC_LP_VERBOSE     3     /* information on conditions, decisions */
      // SC_LP_INFO        4     /* the main things a function is doing */
      // SC_LP_STATISTICS  5     /* important for consistency or performance */
      // SC_LP_PRODUCTION  6     /* a few lines for a major api function */
      // SC_LP_ESSENTIAL   7     /* this logs a few lines max per program */
      // SC_LP_ERROR       8     /* this logs errors only */
      // SC_LP_SILENT      9     /* this never logs anything */

      p4est_init(0, SC_LP_ERROR );
    }

    // return memory used by p4est structures
    static size_t memory_used( forest_t* forest, connectivity_t* connecitivity,
                               ghost_t* ghost, mesh_t* mesh )
    {
      return   _memory_used( forest )
             + _connectivity_memory_used( connecitivity )
             + _ghost_memory_used( ghost )
             + _mesh_memory_used( mesh );
    }

    template <class quadrant_t>
    static bool isNew( const quadrant_t* quad )
    {
      assert( quad );
      return quad->pad8 == NEW ;
    }

    template <class quadrant_t>
    static bool mightVanish( const quadrant_t* quad )
    {
      assert( quad );
      return quad->pad8 == COARSEN ;
    }

    template <class quadrant_t>
    static void resetQuad( quadrant_t* quad )
    {
      //int status =  quad->pad8;
      //std::cout << "Current element status = " << status << std::endl;
      assert( quad );
      quad->pad8 = NONE ;
    }

    template <class quadrant_t>
    static void initIndex( quadrant_t* quad )
    {
      assert( quad );
      quad->p.user_int = 0;
    }

    template <class quadrant_t>
    static void markAsNew( quadrant_t* quad )
    {
      // todo: use user_data pointer instead
      assert( quad );
      quad->pad8 = NEW;
    }

    template <class forest_t, class quadrant_t>
    static adaptationmarker_t adaptationMarker( forest_t* forest, quadrant_t * quadrant )
    {
      assert( forest );
      assert( quadrant );

      // cast user pointer to markervector_t and request marking
      const quadidx_t quadId = quadrant->p.user_long ;
      assert( quadId != -1 );
      return (*((markervector_t *) forest->user_pointer))[ quadId ];
    }

    //! return 1 when refinement should be done, 0 otherwise
    template <class forest_t, class quadrant_t>
    static int checkRefine( forest_t* forest, quadrant_t* quadrant )
    {
      // return 1 when refinement should be done
      return int(adaptationMarker( forest, quadrant ) == REFINE );
    }

    //! return 1 when coarsening should be done, 0 otherwise
    template <class forest_t, class quadrant_t>
    static int checkCoarsen( forest_t* forest, quadrant_t * quadrant[], const int numChildren )
    {
      // only coarsen when all children are marked for coarsening
      for( int i=0; i<numChildren; ++i )
      {
        if( ! int(adaptationMarker( forest, quadrant[i] ) == COARSEN ) )
          return 0;
      }
      return 1;
    }
    /////////////////////////////////////////////////////////////
    //
    //  generic functions
    //
    //////////////////////////////////////////////////////////////

    static qcoord_t zCoord( const quadrant_t& quadrant )
    {
      if constexpr ( std::is_same< quadrant_t, p8est_quadrant_t > :: value )
      {
        return quadrant.z;
      }

      return 0;
    }

    static void initCallBack (forest_t * forest,
                              topidx_t which_tree,
                              quadrant_t * quadrant)
    {
      quadrant->p.user_long = NONE;
    }

    static quadidx_t& refineCounter() {
      static quadidx_t refineCounter = 0 ;
      return refineCounter;
    }

    static quadidx_t& coarsenCounter() {
      static quadidx_t c = 0 ;
      return c;
    }

    static int refineCallBack( forest_t* forest, topidx_t which_tree,
                               quadrant_t * quadrant)
    {
      assert( quadrant->p.user_long == refineCounter() );
      ++ refineCounter();
      return checkRefine( forest, quadrant );
    }

    static int coarsenCallBack( forest_t* forest, topidx_t which_tree,
                                quadrant_t * quadrant[] )
    {
      bool check = quadrant[0]->p.user_long == -1;
      if( quadrant[1] == nullptr )
      {
        assert( ! check );
        ++coarsenCounter();
        return 0;
      }

#ifndef NDEBUG
      for( int i=1; i<NUMCHILDREN; ++i )
      {
        assert( check == (quadrant[i]->p.user_long == -1) );
      }
#endif

      if( check )
      {
        ++coarsenCounter();
        return 0;
      }
      else
        coarsenCounter() += NUMCHILDREN;

      return checkCoarsen( forest, quadrant, NUMCHILDREN );
    }

    template < class DuneComm >
    static forest_t* forest_new( DuneComm comm,
                                 connectivity_t* connectivity,
                                 void* user_pointer )
    {
      initVerbosity();

      MPI_Comm mpiComm = MPI_Comm(comm);
      return _new_ext( mpiComm, connectivity, 0/*minquad*/, 0 /*min level */, 1 /* fill uniform*/,
                       0, // data_size
                       nullptr,//&initCallBack,
                       user_pointer );
    }

    // copy a p-forest
    static forest_t* forest_copy( forest_t* forest ) { return _forest_copy( forest, 0 ); }

    static auto connectivity_new_brick ( const std::array<int, 2>& N, const std::array<int, 2>& periodic )
    {
      assert( dim == 2 );
      return p4est_connectivity_new_brick(N[0], N[1], periodic[0], periodic[1] );
    }

    static auto connectivity_new_brick ( const std::array<int, 3>& N, const std::array<int, 3>& periodic )
    {
      assert( dim == 3 );
      return p8est_connectivity_new_brick(N[0], N[1], N[2], periodic[0], periodic[1], periodic[2] );
    }

    static auto connectivity_new_copy( const topidx_t num_vertices,
                                       const topidx_t num_trees,
                                       const std::vector<double>& vertex_coords,
                                       const std::vector<topidx_t>& tree_to_vertex,
                                       const std::vector<topidx_t>& tree_to_tree,
                                       const std::vector<int8_t>& tree_to_face)
    {
      if constexpr ( dim == 2 )
      {
        const topidx_t num_ctt = 0;
        return _connectivity_new_copy( num_vertices, num_trees, 0 /* num_corners */,
                                       vertex_coords.data(),
                                       tree_to_vertex.data(),
                                       tree_to_tree.data(), tree_to_face.data(),
                                       NULL, &num_ctt, NULL, NULL);
      }

      if constexpr ( dim == 3 )
      {
        const topidx_t num_ctt = 0;
        const topidx_t eoff = 0;
        return _connectivity_new_copy( num_vertices, num_trees, 0 /* num_edges */, 0 /* num_corners */,
                                       vertex_coords.data(),
                                       tree_to_vertex.data(),
                                       tree_to_tree.data(), tree_to_face.data(),
                                       NULL, &eoff, NULL, NULL, NULL, &num_ctt, NULL, NULL);
      }

      std::abort();
      return (connectivity_t *)nullptr;
    }

    // qcoord_to_vertex mapping
    static void qcoord_to_vertex (connectivity_t * connectivity,
                                  topidx_t treeid,
                                  qcoord_t x,
                                  qcoord_t y,
                                  qcoord_t z,
                                  double vxyz[3] )
    {
      if constexpr ( std::is_same< connectivity_t, p8est_connectivity > :: value )
        return p8est_qcoord_to_vertex( connectivity, treeid, x , y, z, vxyz );

      if constexpr ( std::is_same< connectivity_t, p4est_connectivity > :: value )
        return p4est_qcoord_to_vertex( connectivity, treeid, x , y, vxyz );

      assert( false );
      std::abort();
    }

    static qcoord_t quadrant_len( const level_t level )
    {
      if constexpr ( dim == 2 )
        return P4EST_QUADRANT_LEN( level );
      else if constexpr ( dim == 3 )
        return P8EST_QUADRANT_LEN( level );
    }

    static mesh_t* mesh_new (forest_t* forest,
                             ghost_t*  ghost )
    {
      return _mesh_new_ext( forest, ghost, true, true, connect_type_t(CONNECT) );
    }

    static ghost_t* ghost_new(forest_t* forest)
    {
      return _ghost_new( forest, connect_type_t(CONNECT) );
    }

    static numbers_t* numbers_new(forest_t* forest, ghost_t* ghost)
    {
      return _numbers_new( forest, ghost, nullptr);
      //return _numbers_new( forest, nullptr, nullptr);
    }

    static void balance( forest_t* forest )
    {
      _balance( forest, connect_type_t(CONNECT), &initCallBack );
    }

    static void partition( forest_t* forest )
    {
      _partition_ext( forest, 1, nullptr ); // last arg weighting function
    }

    template <class Hierarchy>
    static void adapt ( forest_t* forest, const Hierarchy& hierarchy,
                        const bool /* refine*/, const bool /* coarsen */ )
    {
      void* oldUserPtr = forest->user_pointer ;
      forest->user_pointer = (void *) hierarchy.markerVector();

      // temporarily use user_long to store quadId for access to
      // adaptationMarker during adaptation
      quadidx_t quadId = 0;
      for( topidx_t treeid = forest->first_local_tree;
           treeid <= forest->last_local_tree; ++ treeid )
      {
        tree_t* tree = tree_array_index (forest->trees, treeid );

        /* index setup */
        const quadidx_t elemCount = tree->quadrants.elem_count ;
        for( quadidx_t count = 0; count < elemCount; ++count, ++quadId )
        {
          quadrant_t* quad = quadrant_array_index ( &tree->quadrants, count );
          quad->p.user_long = quadId;
        }
      }

      refineCounter()  = 0;
      coarsenCounter() = 0;

      const gloidx_t nGlobalBeforeRefine = forest->global_num_quadrants;
      // adapt procedure
      _refine ( forest, 0, &refineCallBack,  &initCallBack );

      // if global number of quadrants increased the grid was refined
      const bool refined = forest->global_num_quadrants > nGlobalBeforeRefine ;

      const gloidx_t nGlobalBeforeCoarsen = forest->global_num_quadrants;
      _coarsen_ext( forest, 0, 1, &coarsenCallBack, &initCallBack, nullptr );

      // if global number of quadrants decreased the grid was coarsened
      const bool coarsened = forest->global_num_quadrants < nGlobalBeforeCoarsen;

      // if grid was refined or coarsened it's necessary to balance
      if( refined || coarsened )
        balance( forest );

      // reset old user pointer
      forest->user_pointer = oldUserPtr ;
    }

    static transfer_context_t*
    transfer_fixed_begin( forest_t* forestDest, /// new
                          forest_t* forestSrc, // old
                          void *dest_data, // data_size * destForest->local_num_quadrants
                          const void *src_data, // data_size * srcForest->local_num_quadrants
                          size_t data_size)
    {
      return _transfer_fixed_begin( forestDest->global_first_quadrant,
                                    forestSrc->global_first_quadrant,
                                    forestSrc->mpicomm, 814, // tag
                                    dest_data, src_data, data_size );
    }

    static void mesh_face_neighbor_init( mesh_face_neighbor_t* mfn,
                                         forest_t* forest,
                                         ghost_t* ghost,
                                         mesh_t*  mesh,
                                         topidx_t which_tree,
                                         const quadrant_t* constQuad )
    {
      quadrant_t* quad = const_cast<quadrant_t *> (constQuad);
      _mesh_face_neighbor_init( mfn, forest, ghost, mesh, which_tree, quad );
    }

    static quadrant_t* mesh_face_neighbor_next( mesh_face_neighbor_t* mfn,
                                         topidx_t& ntreeid,
                                         locidx_t& nquadid,
                                         int& nface,
                                         int& outsideRank )
    {
      return _mesh_face_neighbor_next( mfn, &ntreeid, &nquadid, &nface, &outsideRank );
    }

    static void resetAdaptMarker( forest_t* forest )
    {
      for( topidx_t treeid = forest->first_local_tree;
           treeid <= forest->last_local_tree; ++ treeid )
      {
        tree_t* tree = tree_array_index (forest->trees, treeid );
        const quadidx_t elemCount = tree->quadrants.elem_count ;
        for( quadidx_t q = 0; q<elemCount; ++q )
        {
          quadrant_t* quad = quadrant_array_index ( &tree->quadrants, q );
          // implemented in P4estBase
          resetQuad( quad );
        }
      }
    }

    static void setNewElementMarker( forest_t* oldForest, forest_t* forest )
    {
      assert( oldForest->first_local_tree == forest->first_local_tree );
      assert( oldForest->last_local_tree  == forest->last_local_tree );

      for( topidx_t treeid = forest->first_local_tree;
           treeid <= forest->last_local_tree; ++ treeid )
      {
        tree_t* tree          = tree_array_index (forest->trees, treeid );
        tree_t* oldTree       = tree_array_index (oldForest->trees, treeid );

        quadidx_t oldCount = 0 ;
        quadidx_t count = 0;

        const quadidx_t elemCount = tree->quadrants.elem_count ;
        const quadidx_t oldElemCount = oldTree->quadrants.elem_count ;
        while( count < elemCount && oldCount < oldElemCount )
        {
          quadrant_t* quad = quadrant_array_index ( &tree->quadrants, count );
          const quadrant_t* oldQuad = quadrant_array_index ( &oldTree->quadrants, oldCount );

          // if new quadrant is finer, then it must be new
          if( quad->level > oldQuad->level )
          {
            // init index as new
            quad->p.user_int = 0;
            markAsNew( quad );
            ++count ;

            for( int i=1; i<NUMCHILDREN; ++i )
            {
              quad = quadrant_array_index ( &tree->quadrants, count );
              quad->p.user_int = 0;
              markAsNew( quad );
              ++ count ; // go to next new element
            }
            ++oldCount ;
          }
          // if old quadrant is finer then it was coarsened
          else if( quad->level < oldQuad->level )
          {
            quad->p.user_int = 0;
            ++ oldCount;
            for( int i=1; i<NUMCHILDREN; ++i)
            {
              oldQuad = quadrant_array_index ( &oldTree->quadrants, oldCount );
              ++ oldCount;
            }

            ++ count ; // got to next child of old element
          }
          else // nothing happend
          {
            // copy index
            quad->p.user_int = oldQuad->p.user_int;

            ++ count ;
            ++ oldCount ;
          }
        }
      }
    }

  };

}
#endif
