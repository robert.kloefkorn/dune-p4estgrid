/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_ENTITY_HH
#define DUNE_P4ESTGRID_ENTITY_HH

#include <dune/geometry/referenceelements.hh>
#include <dune/grid/common/grid.hh>

#include <dune/p4estgrid/entityseed.hh>
#include <dune/p4estgrid/geometry.hh>
#include <dune/p4estgrid/entityinfo.hh>

namespace Dune
{

  // External Forward Declarations
  // -----------------------------

  template< class > class P4estGridLeafIntersectionIterator;

  template< class > class P4estGridIterator;
  template< class > class P4estGridHierarchicIterator;



  // P4estGridEntity
  // ---------------

  /** \copydoc P4estGridEntity
   *
   *  \nosubgrouping
   */
  template< int codim, int dim, class Grid >
  class P4estGridEntityBase
  {
  protected:
    typedef typename std::remove_const< Grid >::type::Traits Traits;

    typedef Dune :: P4est< Grid :: dimension, Grid :: dimensionworld > PForest ;
    typedef typename PForest :: quadrant_t  quadrant_t;

  public:
    /** \name Attributes
     *  \{ */

    //! codimensioon of the entity
    static const int codimension = codim;
    //! dimension of the grid
    static const int dimension = Traits::dimension;
    //! dimension of the entity
    static const int mydimension = dimension - codimension;
    //! dimension of the world
    static const int dimensionworld = Traits::dimensionworld;

    typedef typename Traits::EntityInfo EntityInfo ;

    /** \} */

    /** \name Types Required by DUNE
     *  \{ */

    //! coordinate type of the grid
    typedef typename Traits::ctype ctype;

    //! type of corresponding entity seed
    typedef typename Grid::template Codim< codimension >::EntitySeed EntitySeed;
    //! type of corresponding geometry
    typedef typename Traits::template Codim< codimension >::Geometry Geometry;

    //! type of corresponding entity
    typedef typename Grid::template Codim< codimension >::Entity Entity;
    /** \} */

  protected:
    typedef typename Traits::ExtraData ExtraData;
    typedef P4estGridGeometry< mydimension, dimensionworld, Grid > GeometryImpl;

  public:
    typedef typename Traits :: ReferenceElementType  ReferenceElementType;

    /** \name Construction, Initialization and Destruction
     *  \{ */

    /** \brief construct a null entity */
    explicit P4estGridEntityBase ( ExtraData data )
    : data_( data ),
      info_(),
      geo_( GeometryImpl() )
    {
      geoImpl().invalidate();
    }

    /** \brief construct a null entity */
    explicit P4estGridEntityBase ( ExtraData data, const EntityInfo& info )
    : data_( data ),
      info_( info ),
      geo_( GeometryImpl() )
    {
      geoImpl().invalidate();
    }

    /** \brief copy constructor */
    P4estGridEntityBase ( const P4estGridEntityBase &other )
    : data_( other.data() ),
      info_( other.info() ),
      geo_( other.geo_ )
    {
    }

    const P4estGridEntityBase &operator= ( const P4estGridEntityBase &other )
    {
      data_ = other.data();
      info_ = other.info();
      geoImpl() = other.geoImpl();
      return *this;
    }

    /** \} */

    /** \name Methods Shared by Entities of All Codimensions
     *  \{ */

    /* \brief return true if two entities are the same */
    bool equals( const P4estGridEntityBase& other ) const
    {
      return info().equals( other.info() );
    }

    /** \brief obtain the name of the corresponding reference element
     *
     *  This type can be used to access the DUNE reference element.
     */
    GeometryType type () const
    {
      return geo_.type();
    }

    /** \brief obtain the level of this entity */
    int level () const
    {
      return info().level();
    }

    /** \brief obtain the partition type of this entity */
    PartitionType partitionType () const
    {
      if constexpr ( codimension == 0 )
      {
        return info().isGhost() ? GhostEntity : InteriorEntity ;
      }
      return InteriorEntity;
    }

    int subEntities( const int cd ) const
    {
      return geoImpl().referenceElement().size( cd - codimension );
    }

    /** obtain the geometry of this entity
     *
     *  Each DUNE entity encapsulates a geometry object, representing the map
     *  from the reference element to world coordinates. Wrapping the geometry
     *  is the main objective of the P4estGrid.
     *
     *  The P4estGrid provides geometries of order 1, obtained by
     *  interpolation of its corners \f$y_i\f$. There corners are calculated
     *  from the corners \f$x_i\f$ of the host geometry through the
     *  P4estGrid's coordinate function \f$c\f$, i.e.,
     *  \f$y_i = c( x_i )\f$.
     *
     *  \returns a const reference to the geometry
     */
    const Geometry &geometry () const
    {
      if( ! geoImpl().valid() )
        geoImpl().build( data(), info(), data()->referenceElement() );

      return geo_ ;
    }

    /** \brief return EntitySeed of host grid entity */
    EntitySeed seed () const { return EntitySeed( info() ); }

    /** \} */


    /** \name Methods Supporting the Grid Implementation
     *  \{ */

    ExtraData data() const
    {
      return data_;
    }

    const EntityInfo& info() const { return info_; }

    /* \brief update grid and entity info, will also invalidate geometry  */
    void update( ExtraData data, const EntityInfo& info )
    {
      data_ = data;
      update( info );
    }

    /* \brief update entity info, will also invalidate geometry */
    void update( const EntityInfo& info )
    {
      info_ = info;
      geoImpl().invalidate();
    }

    //! remove all entity data (mostly for end iterator)
    void clear()
    {
      info_ = EntityInfo();
      geoImpl().invalidate();
    }

    /** \} */
  protected:
    GeometryImpl& geoImpl() const
    {
      return geo_.impl();
    }

  protected:
    //! grid pointer
    ExtraData data_;
    //! necessary element information
    EntityInfo info_;

    //! geometry implementation
    mutable Geometry geo_;
  };


  /** \copydoc P4estGridEntity
   *
   *  \nosubgrouping
   */
  template< int codim, int dim, class Grid >
  class P4estGridEntity
    : public P4estGridEntityBase< codim, dim, Grid >
  {
    typedef P4estGridEntityBase< codim, dim, Grid > Base;
  public:
    typedef typename Base::ExtraData ExtraData;
    typedef typename Base::EntityInfo EntityInfo;

    /** \name Construction, Initialization and Destruction
     *  \{ */

    /** \brief construct a null entity */
    explicit P4estGridEntity ( ExtraData data )
    : Base( data )
    {}

    /** \brief construct a null entity */
    explicit P4estGridEntity ( ExtraData data, const EntityInfo& info )
    : Base( data, info )
    {}
  };


  // P4estGridEntity for codimension 0
  // -------------------------------------

  /** \copydoc P4estGridEntity
   *
   *  \nosubgrouping
   */
  template< int dim, class Grid >
  class P4estGridEntity< 0, dim, Grid > : public P4estGridEntityBase<0, dim, Grid >
  {
    typedef P4estGridEntityBase< 0, dim, Grid > Base;
    typedef typename Base::ExtraData  ExtraData;
    typedef typename Base::PForest    PForest;
  public:
    typedef typename Base::EntityInfo EntityInfo;
    typedef typename Base::Traits     Traits;

  public:
    /** \name Attributes
     *  \{ */

    //! codimension of the entity
    static const int codimension = 0;
    //! dimension of the grid
    static const int dimension = Traits::dimension;
    //! dimension of the entity
    static const int mydimension = dimension - codimension;
    //! dimension of the world
    static const int dimensionworld = Traits::dimensionworld;

    /** \} */

    typedef typename Base::Entity  Entity;

    typedef typename Traits::template Codim< codimension >::LocalGeometry LocalGeometry;

    //! coordinate type of the grid
    typedef typename Traits::ctype ctype;

    //! type of hierarchic iterator
    typedef typename Traits::HierarchicIterator HierarchicIterator;
    //! type of leaf intersection iterator
    typedef typename Traits::LeafIntersectionIterator LeafIntersectionIterator;
    //! type of level intersection iterator
    typedef typename Traits::LevelIntersectionIterator LevelIntersectionIterator;

    typedef typename Traits :: ReferenceElementType  ReferenceElementType;
    /** \} */

  protected:

    typedef P4estGridGeometry< mydimension, dimensionworld, Grid > GeometryImpl;

    typedef typename Traits::IntersectionIteratorImpl  IntersectionIteratorImpl;

    typedef IntersectionIteratorImpl LeafIntersectionIteratorImpl;
    typedef IntersectionIteratorImpl LevelIntersectionIteratorImpl;

    using Base::geoImpl;
  public:
    using Base::level;
    using Base::data;
    using Base::info;

    /** \name Construction, Initialization and Destruction
     *  \{ */

    /** \brief construct a null entity */
    explicit P4estGridEntity ( )
      : Base( nullptr )
    {}

    /** \brief construct a null entity */
    explicit P4estGridEntity ( ExtraData data )
      : Base( data )
    {}

    /** \brief construct an entity */
    P4estGridEntity ( ExtraData data, const EntityInfo& info )
      : Base( data, info )
    {}

    /** \brief copy constructor */
    P4estGridEntity ( const P4estGridEntity &other )
      : Base( other )
    {}

    /** \} */

    template< int codim >
    int count () const
    {
      return subEntities( codim );
    }

    int subEntities( const int codim ) const
    {
      return geoImpl().referenceElement().size( codim - codimension );
    }

    template< int codim >
    typename Grid::template Codim< codim >::Entity
    subEntity ( int subEn ) const
    {
      typedef typename Traits::template Codim< codim >::EntityImpl EntityImpl;
      return EntityImpl( data(), EntityInfo(info(), codim, subEn) );
    }

    LevelIntersectionIterator ilevelbegin () const
    {
      if constexpr ( Grid::enableHierarchy )
      {
        return ileafbegin();
        //return LevelIntersectionIteratorImpl( data(), info() );
        //return LevelIntersectionIteratorImpl( *data(), *this, level(), false );
      }
      else
        return ileafbegin();
    }

    LevelIntersectionIterator ilevelend () const
    {
      if constexpr ( Grid::enableHierarchy )
      {
        //return LevelIntersectionIteratorImpl( data() );
        //return LevelIntersectionIteratorImpl( *data(), *this, level(), true );
        return ileafend();
      }
      else
      {
        return ileafend();
      }
    }

    LeafIntersectionIterator ileafbegin () const
    {
      //return LeafIntersectionIteratorImpl( data(), info() );
      return LeafIntersectionIteratorImpl( data(), *this );
    }

    LeafIntersectionIterator ileafend () const
    {
      return LeafIntersectionIteratorImpl( data() );
      //return LeafIntersectionIteratorImpl( data() );
    }

    bool hasBoundaryIntersections () const
    {
      const LeafIntersectionIterator end = ileafend();
      for( LeafIntersectionIterator it = ileafbegin(); it != end ; ++ it )
      {
        if( it->boundary() ) return true ;
      }
      return false ;
    }

    bool isLeaf () const
    {
      //if constexpr ( Grid :: enableHierarchy )
      //  return data()->indices().getItem( info().userIndex() ).isLeaf();
      //else // if hierarchy is disabled all entities are leaf entities
      return info().isLeaf(); // this can only change during adaptation
    }

    bool hasFather () const
    {
      if constexpr ( Grid :: enableHierarchy )
      {
        return level() > 0;
      }
      else
        return false;
    }

    Entity father () const
    {
      typedef typename Traits::template Codim< 0 >::EntityImpl EntityImpl;
      if constexpr ( Grid :: enableHierarchy )
      {
        if( level() > 0 )
        {
          typedef typename PForest :: quadrant_t  quadrant_t;
          // construct farher
          quadrant_t parent ;
          PForest :: quadrant_ancestor( &info().quadrant(), (level() - 1), &parent );

          // this is not available
          parent.p.user_long = -1;

          return EntityImpl( data(),
                    EntityInfo( parent, info().treeId(), info().quadId() ) );
        }
      }
      DUNE_THROW(NotImplemented,"P4estGrid does not implement a hierarchy!");
      return EntityImpl( data() );
    }

    const LocalGeometry& geometryInFather () const
    {
      const int childIndex = PForest :: quadrant_child_id( &info().quadrant() );
      return data()->geometryInFather( childIndex );
    }

    HierarchicIterator hbegin ( int maxLevel ) const
    {
      if constexpr( Grid::enableHierarchy )
      {
        typedef P4estGridHierarchicIterator< Grid > HIterator;
        return HIterator( data(), info(), maxLevel );
      }
      else // since it's a leaf entity there exist no hierarchy
      {
        return hend( maxLevel );
      }
    }

    HierarchicIterator hend ( int maxLevel ) const
    {
      typedef P4estGridHierarchicIterator< Grid > HIterator;
      return HIterator( data() );
    }

    bool isRegular () const
    {
      return true ;
    }

    // return if element was created during last adaptation cycle
    bool isNew () const
    {
      return PForest :: isNew( &info().quadrant() );
    }

    // return true if element could be coarsened in next adaptation cycle
    bool mightVanish () const { return getMark() < 0 ;  }

    //! mark element
    bool mark ( const int adaptMarker ) const
    {
      if ( info().isGhost() ) return false ;

      typedef typename PForest :: adaptationmarker_t adaptationmarker_t;

      // get reference to current status
      adaptationmarker_t& status = data()->indices().getMark( info().quadId() );
      assert( status >= -1 && status <= 2 );

      status = adaptationmarker_t( adaptMarker );
      assert( status >= -1 && status <= 1 );

      return true;
    }

    int getMark() const
    {
      if ( info().isGhost() ) return 0 ;

      return int( data()->indices().getMark( info().quadId() ));
    }

    /** \} */
  };

}

#endif // #ifndef DUNE_P4ESTGRID_ENTITY_HH
