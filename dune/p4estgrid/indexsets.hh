/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_INDEXSETS_HH
#define DUNE_P4ESTGRID_INDEXSETS_HH

#include <vector>

#include <dune/common/typetraits.hh>

#include <dune/grid/common/gridenums.hh>
#include <dune/grid/common/indexidset.hh>

#include <dune/p4estgrid/capabilities.hh>
#include <dune/p4estgrid/declaration.hh>


namespace Dune
{
  // P4estGridIndexSet
  // --------------

  template< class Grid, int indexSetSpec >
  class P4estGridIndexSet
  : public IndexSet< Grid, P4estGridIndexSet< Grid, indexSetSpec >, size_t >
  {
    typedef P4estGridIndexSet< Grid, indexSetSpec > This;

    typedef typename std::remove_const< Grid >::type::Traits Traits;
    typedef P4estEntityInfo < Traits ::dimension > EntityInfo;

    static const bool isLeafSet = indexSetSpec == 1;
    static const bool isHSet = indexSetSpec == 2;
  public:

    static const int dimension = Traits :: dimension;
    typedef IndexSet< Grid, This, size_t > Base;

    typedef P4estIndex< Traits >  IndicesType;

    typedef typename Base::IndexType IndexType;

    //! type of geometry types
    typedef std::vector< GeometryType > Types;


    P4estGridIndexSet ( const Grid& grid , const int level = -1 )
    : grid_( &grid ),
      indices_( grid.indices() ),
      level_( level )
    {}

    using Base::index;
    using Base::subIndex;

    template< int cc >
    IndexType index ( const typename Traits::template Codim< cc >::Entity &entity ) const
    {
      return subIndex( entity, 0, cc );
    }

    template< int cc >
    IndexType subIndex ( const typename Traits::template Codim< cc >::Entity &entity, int i, unsigned int codim ) const
    {
      if constexpr ( isLeafSet )
      {
        // assert( cc == 0 );
        return indices_.leafIndex( entity.impl().info().userIndex(), i, codim );
      }
      else if constexpr ( isHSet )
      {
        // assert( cc == 0 );
        //return indices_.hIndex( entity.impl().info().userIndex(), i, codim );
        return entity.impl().info().userIndex();
      }
      else
      {
        // assert( cc == 0 );
        return indices_.levelIndex( entity.impl().info().userIndex(), i, codim, level_ );
      }
    }

    IndexType size ( GeometryType type ) const
    {
      const int codim = dimension - type.dim() ;
      if( type == geomTypes( codim )[0] ) return size( codim );

      return 0;
    }

    int size ( int codim ) const
    {
      if constexpr ( isLeafSet )
      {
        return indices_.leafSize( codim );
      }
      else if constexpr ( isHSet )
      {
        return grid_->hSize( codim );
      }
      else
        return indices_.levelSize( codim, level_ );
    }

    template< class Entity >
    bool contains ( const Entity &entity ) const
    {
      //static const int cc = Entity::codimension;
      return true ; //hostIndexSet().contains( Grid::template getHostEntity< cc >( entity ) );
    }

    const std::vector< GeometryType > &geomTypes ( int codim ) const
    {
      assert( grid_ );
      return grid_->geomTypes( codim );
    }

    //! deliver all geometry types used in this grid
    Types types( const int codim ) const
    {
      return geomTypes( codim );
    }

  private:
    const Grid *grid_;
    const IndicesType& indices_;
    const int level_ ;
  };

}

#endif // #ifndef DUNE_P4ESTGRID_INDEXSETS_HH
