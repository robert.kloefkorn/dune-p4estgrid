/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4EST_ENTITYINFO_HH
#define DUNE_P4EST_ENTITYINFO_HH

#include <dune/common/typetraits.hh>
#include <dune/grid/common/gridenums.hh>


namespace Dune
{

  // P4estEntityInfo
  // -----------------

  template< int dim >
  class P4estEntityInfo
  {
    typedef P4estEntityInfo< dim > This;

  public:
    static const int dimension   = dim ;
    // TODO change template list of entity info
    typedef Dune :: P4est< dimension, dimension > PForest ;
    typedef typename PForest :: qcoord_t   qcoord_t ;
    typedef typename PForest :: topidx_t   topidx_t ;
    typedef typename PForest :: quadidx_t  quadidx_t ;
    typedef typename PForest :: quadrant_t quadrant_t ;
    typedef typename PForest :: level_t    level_t ;

    static quadrant_t* empty() {
      static quadrant_t q;
      return &q;
    }

    P4estEntityInfo ( const This& other ) = default;

    P4estEntityInfo ( const quadrant_t* quad,
                      const topidx_t  treeId,
                      const quadidx_t quadId,
                      const quadidx_t userIndex,
                      const int codim = 0,
                      const int subEntity = -1 )
    : quad_( quad ),
      treeId_( treeId ),
      quadId_( quadId ),
      userIndex_( userIndex ),
      codim_( codim ),
      subEntity_( subEntity ),
      isGhost_( false ),
      isLeaf_( true )
    {
    }

    P4estEntityInfo ( const quadrant_t* quad,
                      const topidx_t  treeId,
                      const quadidx_t quadId,
                      const quadidx_t userIndex,
                      const bool isGhost )
    : quad_( quad ),
      treeId_( treeId ),
      quadId_( quadId ),
      userIndex_( userIndex ),
      codim_( 0 ),
      subEntity_( -1 ),
      isGhost_( isGhost ),
      isLeaf_( true )
    {
    }

    explicit P4estEntityInfo ( const This& other,
                               const int codim,
                               const int subEntity )
    : quad_( other.quad_ ),
      treeId_( other.treeId_ ),
      quadId_( other.quadId_ ),
      userIndex_( other.userIndex_ ),
      codim_( codim ),
      subEntity_( subEntity ),
      isGhost_( false ),
      isLeaf_( true )
    {
      assert( codim     >= -127 && codim     <= 128 ); // signed char range
      assert( subEntity >= -127 && subEntity <= 128 ); // signed char range
    }

    P4estEntityInfo ( )
    : quad_( empty() ),
      treeId_( -1 ),
      quadId_( -1 ),
      userIndex_( -1 ),
      codim_( -1 ),
      subEntity_( -1 ),
      isGhost_( false ),
      isLeaf_( true )
    {
    }

    bool valid() const { return userIndex_ >= 0; }
    bool isValid() const { return valid(); }

    quadidx_t userIndex() const
    {
      assert( userIndex_ >= 0 );
      return userIndex_ ;
    }

    quadidx_t freeUserIndex() const
    {
      quadidx_t idx = userIndex();
      userIndex_ = -1;
      return idx;
    }

    const quadrant_t&  quadrant() const { assert(quad_); return *quad_; }
    qcoord_t  x() const { return quadrant().x ; }
    qcoord_t  y() const { return quadrant().y ; }
    qcoord_t  z() const { return PForest :: zCoord( quadrant() ); }
    topidx_t treeId () const { return treeId_; }
    //qcoord_t  z() const { return quad_.z ; }
    level_t   level() const { return quadrant().level ; }
    quadidx_t quadId () const { return quadId_; }
    bool equals (const P4estEntityInfo& other) const
    {
      if( codim_ != other.codim_ && subEntity_ != other.subEntity_ ) return false ;
      if ( quadId_ != other.quadId() )
        return false ;
      if( level() != other.level() )
        return false ;

      return x() == other.x() &&
             y() == other.y() &&
             z() == other.z();
    }

    bool isGhost () const { return isGhost_; }
    bool isLeaf() const { return isLeaf_; }

    void setLeaf( const bool isLeaf ) { isLeaf_ = isLeaf; }

    int  subEntity() const { return int(subEntity_) ; }
    int  codimension() const { return int(codim_); }

    void print( std::ostream& out ) const
    {
      out << "quad " << quad_ << std::endl;
      out << "coord " << x() << " " << y() << " " << z() << std::endl;
      out << std::endl;
    }

    static void printSize( std::ostream& out )
    {
      out << "sizeof = " << sizeof(P4estEntityInfo) << std::endl;
    }

    quadrant_t* quadPtr() const { return const_cast< quadrant_t* > (quad_); }

  protected:
    const quadrant_t*  quad_;       //  8 bytes
    topidx_t           treeId_;     //  4 bytes
    quadidx_t          quadId_;     //  4 bytes
    mutable quadidx_t  userIndex_;  //  4 bytes
    int8_t             codim_ ;     //  1 byte
    int8_t             subEntity_ ; //  1 byte
    bool               isGhost_;    //  1 byte
    bool               isLeaf_;     //  1 byte
                                    //  = 24 bytes
  };

}
#endif // #ifndef DUNE_P4EST_ENTITYINFO_HH
