/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_ENTITYPOINTER_HH
#define DUNE_P4ESTGRID_ENTITYPOINTER_HH

#include <dune/grid/common/grid.hh>
#include <dune/p4estgrid/declaration.hh>

namespace Dune
{

  // EntityPointerTraits
  // -------------------

  template< int codim, class Grid >
  struct P4estGridEntityPointerTraits;

  /** \cond */
  template< int codim, class Grid >
  struct P4estGridEntityPointerTraits< codim, const Grid >
  : public P4estGridEntityPointerTraits< codim, Grid >
  {};
  /** \endcond */

  template< int codim, int dim, int dimworld, P4estType elType, class ct >
  struct P4estGridEntityPointerTraits< codim, P4estGrid< dim, dimworld, elType, ct  > >
  {
    typedef Dune::P4estGrid< dim, dimworld, elType, ct > Grid;

    typedef ct ctype;

    static const int dimension = dim;
    static const int codimension = codim;

    typedef Dune::Entity< codimension, dimension, const Grid, P4estGridEntity > Entity;
  };



  // P4estGridEntityPointer
  // ----------------------

  template< class Traits >
  class P4estGridEntityPointer
  {
    typedef P4estGridEntityPointer< Traits > This;

    typedef typename Traits::Grid Grid;

    typedef P4estGridEntityPointerTraits< Traits::codimension, const Grid > BaseTraits;
    friend class P4estGridEntityPointer< BaseTraits >;

  public:
    static const int dimension = Traits::dimension;
    static const int codimension = Traits::codimension;

    typedef typename Traits::Entity Entity;

    typedef P4estGridEntityPointer< BaseTraits > EntityPointerImp;
    typedef P4estEntityInfo< dimension > EntityInfo ;

  protected:
    typedef Dune :: P4est< Grid :: dimension, Grid :: dimensionworld > PForest ;
    typedef typename PForest :: quadrant_t  quadrant_t;
    typedef typename Grid::Traits::ExtraData  ExtraData;

  private:
    typedef P4estGridEntity< codimension, dimension, const Grid > EntityImpl;

  public:
    explicit P4estGridEntityPointer ( ExtraData data )
    : entity_( EntityImpl( data ) )
    {}

    explicit P4estGridEntityPointer ( const EntityImpl &entity )
    : entity_( entity )
    {}

    P4estGridEntityPointer ( ExtraData data, const EntityInfo& info )
    : entity_( EntityImpl( data, info ) )
    {}

    //! create new iterator based only data of other
    //! we do not copy here to avoid problems with thread safety.
    P4estGridEntityPointer ( const This &other )
    : P4estGridEntityPointer( other.entityImpl().data(), other.entityImpl().info() )
    {}

    //! create new iterator based only data of other
    //! we do not copy here to avoid problems with thread safety.
    template< class T >
    explicit P4estGridEntityPointer ( const P4estGridEntityPointer< T > &other )
    : P4estGridEntityPointer( other.data(), other.info() )
    {}

    operator const EntityPointerImp & () const
    {
      DUNE_THROW( NotImplemented, "Casting EntityPointers of P4estGrid is not supported." );
    }

    This &operator= ( const This &other )
    {
      // copy iterator based only data of other
      // we do not copy the entity here to avoid problems with thread safety.
      entityImpl().update( other.data(), other.info() );
      return *this;
    }

    const EntityInfo& info() const {  return entityImpl().info(); }

    template< class T >
    bool equals ( const P4estGridEntityPointer< T > &other ) const
    {
      //std::cout << "Compare " << std::endl;
      //info().print( std::cout );
      //other.info().print( std::cout );
      return info().equals( other.info() );
    }

    Entity &dereference () const {  return entity_;  }
    int level () const  { return entity_.level();  }

    void compactify () {}

  protected:
    void update( const EntityInfo& info )
    {
      //std::cout << "Update " ;
      //info.print( std::cout );
      //std::cout << std::endl;
      entityImpl().update( info );
    }

    void clear()
    {
      entityImpl().clear();
    }

    ExtraData data() const { return entityImpl().data(); }

    EntityImpl &entityImpl () const
    {
      return entity_.impl();
    }

    mutable Entity entity_;
  };

}

#endif // #ifndef DUNE_P4ESTGRID_ENTITYPOINTER_HH
