// (c) Robert Kloefkorn 2023
#ifndef DUNE_P4ESTGRID_INDEXSTACK_HH_INCLUDED
#define DUNE_P4ESTGRID_INDEXSTACK_HH_INCLUDED

#include <algorithm>
#include <cassert>
#include <iostream>
#include <stack>
#include <vector>

namespace Dune
{
  namespace detail {

    // using namespace std has always to be called inside the namespace
    // P4estGridSpace

    template<class T, int length>
    class P4estGridFiniteStack
    {
    public :
      // Makes empty stack
      P4estGridFiniteStack () : _f(0) {}

      // Returns true if the stack is empty
      bool empty () const { return _f <= 0; }

      // Returns true if the stack is full
      bool full () const { return (_f >= length); }

      // clear stack
      void clear() { _f = 0; }

      // Puts a new object onto the stack
      void push (const T& t)
      {
        assert ( _f < length );
        _s[_f++] = t;
      }

      // Removes and returns the uppermost object from the stack
      T pop () {
        assert ( _f > 0 );
        return _s[--_f];
      }

      // Returns the uppermost object on the stack
      T top () const {
        assert ( _f > 0 );
        return _s[_f-1];
      }

      // stacksize
      int size () const { return _f; }

      // backup stack to ostream
      void backup ( std::ostream & os ) const
      {
        os.write( ((const char *) &_f ), sizeof(int) ) ;
        for(int i=0; i<size(); ++i)
        {
          os.write( ((const char *) &_s[i] ), sizeof(int) ) ;
        }
      }

      // restore stack from istream
      void restore ( std::istream & is )
      {
        is.read ( ((char *) &_f), sizeof(int) );
        assert ( _f >= 0 );
        assert ( _f < length );
        for(int i=0; i<size(); ++i)
        {
          is.read ( ((char *) &_s[i]), sizeof(int) );
        }
      }

    private:
       T   _s[length]; // the stack
       int _f;         // actual position in stack
    };


    //******************************************************
    //
    //  P4estGridIndexStack providing indices via getIndex and freeIndex
    //  indices that are freed, are put on a stack and get
    //
    //******************************************************
    template <class T, int length>
    class P4estGridIndexStack
    {
      typedef P4estGridFiniteStack<T,length> StackType;
      typedef std::stack< StackType * > StackListType;

      StackListType fullStackList_;
      StackListType emptyStackList_;

      //typedef typename StackListType::Iterator DListIteratorType;
      StackType * stack_;

      // current maxIndex
      int maxIndex_;
      int dim_;
    public:
      //! Constructor, create new P4estGridIndexStack
      P4estGridIndexStack();

      //! Destructor, deleting all stacks
      inline ~P4estGridIndexStack ();

      //! set dimension of grid
      void setDimension ( const int dim );
      //! return dimension of grid
      int dimension () const;

      //! set index as maxIndex if index is bigger than maxIndex
      void checkAndSetMax(T index) { if(index > maxIndex_) maxIndex_ = index;  }

      //! set index as maxIndex
      void setMaxIndex(T index) { maxIndex_ = index; }

      //! returns the larges index used + 1, actually this is the size of the
      //! index set
      int getMaxIndex() const {  return maxIndex_;  }

      //! restore index from stack or create new index
      T getIndex ();

      //! store index on stack
      void freeIndex(T index);

      //! test stack functionality
      void test ();

      // backup set to out stream
      template <class ostream_t>
      void backupIndexSet ( ostream_t & os );

      // restore from in stream
      template <class istream_t>
      void restoreIndexSet ( istream_t & is ); //, RestoreInfo& restoreInfo );

      // all entries in vector with value true
      // are inserted as holes
      void generateHoles(const std::vector<bool> & isHole);

      // remove all indices that are not used (if possible)
      void compress ();

      // return size of used memory in bytes
      size_t memUsage () const ;

    private:
      //! push index to stack
      inline void pushIndex(T index);

      // no copy constructor allowed
      P4estGridIndexStack( const P4estGridIndexStack<T,length> & s);

      // no assignment operator allowed
      P4estGridIndexStack<T,length> & operator = ( const P4estGridIndexStack<T,length> & s);

      // clear all stored indices
      void clearStack ();

    };  // end class P4estGridIndexStack

    //****************************************************************
    // Inline implementation
    // ***************************************************************
    template <class T, int length>
    inline P4estGridIndexStack<T,length>::P4estGridIndexStack()
      : stack_ ( new StackType () ) , maxIndex_ (0), dim_(-1) {}

    template <class T, int length>
    inline P4estGridIndexStack<T,length>::~P4estGridIndexStack ()
    {
      if(stack_) delete stack_;
      stack_ = 0;

      while( !fullStackList_.empty() )
      {
        StackType * st = fullStackList_.top();
        fullStackList_.pop();
        delete st;
      }
      while( !emptyStackList_.empty() )
      {
        StackType * st = emptyStackList_.top();
        emptyStackList_.pop();
        delete st;
      }
    }

    template <class T, int length>
    inline void P4estGridIndexStack<T,length>::setDimension( const int dim )
    {
      dim_ = dim;
    }

    template <class T, int length>
    inline int P4estGridIndexStack<T,length>::dimension() const
    {
      assert( dim_ == 2 || dim_ == 3 );
      return dim_;
    }

    template <class T, int length>
    inline T P4estGridIndexStack<T,length>::getIndex ()
    {
      if((*stack_).empty())
      {
        if( fullStackList_.empty() )
        {
          assert ( fullStackList_.size() <= 0 );
          return maxIndex_++;
        }
        else
        {
          emptyStackList_.push( stack_ );
          stack_ = fullStackList_.top();
          fullStackList_.pop();
        }
      }
      return (*stack_).pop();
    }

    template <class T, int length>
    inline void P4estGridIndexStack<T,length>::freeIndex ( T index )
    {
      if(index == (maxIndex_ -1))
      {
        --maxIndex_;
        return ;
      }
      else
      {
        pushIndex(index);
      }
    }


    template <class T, int length>
    inline void P4estGridIndexStack<T,length>::pushIndex( T index )
    {
      if((*stack_).full())
      {
        fullStackList_.push( stack_ );
        if( emptyStackList_.empty() )
        {
          assert ( emptyStackList_.size() <= 0 );
          stack_ = new StackType ();
        }
        else
        {
          stack_ = emptyStackList_.top();
          emptyStackList_.pop();
        }
      }
      (*stack_).push(index);
    }

    template <class T, int length>
    inline void P4estGridIndexStack<T,length>::test ()
    {
      T vec[2*length];

      for(int i=0; i<2*length; i++)
        vec[i] = getIndex();

      for(int i=0; i<2*length; i++)
        freeIndex(vec[i]);

      for(int i=0; i<2*length; i++)
        vec[i] = getIndex();

      for(int i=0; i<2*length; i++)
        printf(" index [%d] = %d \n",i,vec[i]);
    }

    template <class T, int length>
    template <class ostream_t>
    inline void P4estGridIndexStack<T,length>::backupIndexSet ( ostream_t & os )
    {
      // holes are not stored at the moment
      // they are reconstructed when gitter is
      // restored
      os.write( ((const char *) &maxIndex_ ), sizeof(int) ) ;

      return ;
    }

    template <class T, int length>
    template <class istream_t>
    inline void P4estGridIndexStack<T,length>::
    restoreIndexSet ( istream_t & is ) //, RestoreInfo& restoreInfo)
    {
      // read maxIndex from stream
      is.read ( ((char *) &maxIndex_), sizeof(int) );

      /*
      // adjust byte order if necessary
      if( restoreInfo.toggleByteOrder() )
        restoreInfo.changeByteOrder( ((char *) &maxIndex_), sizeof(int) );
      */

      // clear stack for reconstruction of holes
      clearStack ();

      return ;
    }

    template <class T, int length>
    inline void P4estGridIndexStack<T,length>::clearStack ()
    {
      if(stack_)
      {
        delete stack_;
        stack_ = new StackType();
        assert (stack_);
      }

      while( !fullStackList_.empty() )
      {
        StackType * st = fullStackList_.top();
        fullStackList_.pop();
        if(st) delete st;
      }
      return;
    }
    template <class T, int length>
    inline void P4estGridIndexStack<T,length>::
    generateHoles(const std::vector<bool> & isHole)
    {
      const int idxsize = isHole.size();
      assert ( idxsize == maxIndex_ );
      // big indices are inserted first
      for(int i=idxsize-1; i>=0; --i)
      {
        // all entries marked true will be pushed to stack
        // to create the exact index manager status from before
        if(isHole[i] == true) pushIndex(i);
      }
    }

    template <class T, int length>
    inline void P4estGridIndexStack<T,length>::
    compress()
    {
      std::vector<int> tmpStorage;

      if( stack_ )
      {
        // StackType is of type FiniteStack
        StackType& stack = *stack_;

        // reserve memory for tmpStorage
        tmpStorage.reserve( stack.size() );

        // copy all values to the temporary storage
        while( ! stack.empty() )
        {
          tmpStorage.push_back( stack.pop() );
        }
        delete stack_; stack_ = 0;
      }

      while( !fullStackList_.empty() )
      {
        StackType * st = fullStackList_.top();
        fullStackList_.pop();
        // if stack is available
        if( st )
        {
          // StackType is of type FiniteStack
          StackType& stack = *st;

          // reserve memory for tmpStorage
          tmpStorage.reserve( tmpStorage.size() + stack.size() );

          // copy all values to the temporary storage
          while( ! stack.empty() )
          {
            tmpStorage.push_back( stack.pop() );
          }
          delete st;
        }
      }

      // sort so that the larges values is at the end
      // this sort is necessary to really free indices
      std::sort( tmpStorage.begin(), tmpStorage.end() );

      // now free all indices again, freeIndex
      // does remove the maxIndex in case of freed index is equal
      stack_ = new StackType();
      assert ( stack_ );
      // until tmpStorage is not empty, freeIndices
      while( ! tmpStorage.empty () )
      {
        // free index
        freeIndex( tmpStorage.back() );
        // remove index from tmpStorage
        tmpStorage.pop_back();
      }
    }

    template <class T, int length>
    inline size_t P4estGridIndexStack<T,length>::
    memUsage () const
    {
      size_t mySize = sizeof(P4estGridIndexStack<T,length>);
      size_t stackSize = sizeof(StackType);
      if(stack_) mySize += stackSize;
      mySize += stackSize * fullStackList_.size();
      mySize += stackSize * emptyStackList_.size();
      return mySize;
    }

    // define index stack tpye for all grids
    enum { lengthOfFiniteStack = 262144 }; // 2^18
    typedef P4estGridIndexStack<int,lengthOfFiniteStack> IndexManagerType;

  } // namespace detail

} // namespace Dune

#endif // #ifndef DUNE_P4ESTGRID_INDEXSTACK_HH_INCLUDED
