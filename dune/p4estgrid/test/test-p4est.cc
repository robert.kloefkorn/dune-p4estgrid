#include <config.h>

// #define NO_2D
// #define NO_3D
#define DISABLE_DEPRECATED_METHOD_CHECK 1


#include <iostream>
#include <sstream>
#include <string>

#include <dune/common/parallel/mpihelper.hh>

//#include <dune/grid/cartesiangrid/dgfparser.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>

#include <dune/grid/test/gridcheck.hh>

#include <dune/grid/test/checkgeometryinfather.hh>
#include <dune/grid/test/checkintersectionit.hh>
#include <dune/grid/test/checkcommunicate.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <dune/p4estgrid/grid.hh>
#include <dune/p4estgrid/dgf.hh>

using namespace Dune;

namespace Dune {

  template <int dim, int dimworld, class ct >
  struct EnableBoundarySegmentIndexCheck< P4estGrid< dim, dimworld, ct > >
  {
    static const bool value = false;
  };
}

template <class GridType>
void printSizeOf( GridType &grid )
{
  std::cout << "Entity           " << sizeof(typename GridType::template Codim<0>::Entity) << std::endl;
  std::cout << "Intersection     " << sizeof(typename GridType::Traits::IntersectionImpl) << std::endl;
  std::cout << "IntersectionImpl " << sizeof(P4estGridIntersection< GridType >) << std::endl;
}

template <class Entity>
void printGeom( const Entity& e )
{
  const auto geo = e.geometry();
  const int nVx = geo.corners();
  for( int vx = 0; vx<nVx; ++vx )
  {
    std::cout << "vx[ " << vx << " ] = " << geo.corner( vx ) << std::endl;
  }
}

template <class GV>
void printGrid( const GV& gv)
{
  std::cout << "EntityInfo " << sizeof(P4estEntityInfo< GV::dimension >) <<
    std::endl;

  P4estEntityInfo< GV::dimension >::printSize( std::cout );

  std::cout << "Printing grid view" << std::endl;
  for ( const auto& e : Dune::elements( gv ) )
  {
    std::cout << e.type() << std::endl;
    printGeom( e );

    const auto& refElem = Dune::referenceElement( e );
    for( int i=0; i<refElem.size(1); ++i )
    {
      auto face = e.template subEntity<1>( i );
      std::cout << "Face " << i << std::endl;
      printGeom( face );
    }
  }
}

template <class GridType>
void makeNonConfGrid(GridType &grid,int level,int adapt) {
  int myrank = grid.comm().rank();
  grid.loadBalance();
  grid.globalRefine(level);
  grid.loadBalance();
  for (int i=0;i<adapt;i++)
  {
    if (myrank==0)
    {
      typedef typename GridType :: template Codim<0> ::
            template Partition<Interior_Partition> :: LeafIterator LeafIterator;

      LeafIterator endit = grid.template leafend<0,Interior_Partition>   ();
      int nr = 0;
      int size = grid.size(0);
      for(LeafIterator it    = grid.template leafbegin<0,Interior_Partition> ();
          it != endit ; ++it,nr++ )
      {
        grid.mark(1, *it );
        if (nr>size*0.8) break;
      }
    }
    grid.adapt();
    grid.postAdapt();
    grid.loadBalance();
  }

  //printGrid( grid.leafGridView() );
}

template <class GridView>
void writeFile( const GridView& gridView )
{
  DGFWriter< GridView > writer( gridView );
  writer.write( "dump.dgf" );
}

template <class GridType>
void checkSerial(GridType & grid, int mxl = 2)
{
  printSizeOf( grid );

  // be careful, each global refine create 8 x maxlevel elements
  std::cout << "  CHECKING: Macro" << std::endl;
  gridcheck(grid);
  std::cout << "  CHECKING: Macro-intersections" << std::endl;
  checkIntersectionIterator(grid);

  for(int i=0; i<mxl; i++)
  {
    grid.globalRefine( 1 );//DGFGridInfo<GridType> :: refineStepsForHalf() );
    std::cout << "  CHECKING: Refined" << std::endl;
    gridcheck(grid);
    std::cout << "  CHECKING: intersections" << std::endl;
    checkIntersectionIterator(grid);
  }

  // check also non-conform grids
  makeNonConfGrid(grid,0,1);

  std::cout << "  CHECKING: non-conform" << std::endl;
  gridcheck(grid);
  std::cout << "  CHECKING: twists " << std::endl;
  // if( checkTwist )
  //  checkTwists( grid.leafView(), NoMapTwist() );

  // check the method geometryInFather()
  std::cout << "  CHECKING: geometry in father" << std::endl;
  //checkGeometryInFather(grid);
  // check the intersection iterator and the geometries it returns
  std::cout << "  CHECKING: intersections" << std::endl;
  //checkIntersectionIterator(grid);

  std::cout << std::endl << std::endl;
}

template <class GridType>
void checkParallel(GridType & grid, int gref, int mxl = 3, const bool display = false )
{
#if HAVE_MPI
  makeNonConfGrid(grid,gref,mxl);

  // -1 stands for leaf check
  checkCommunication(grid, -1, std::cout);

  for(int l=0; l<= mxl; ++l)
    checkCommunication(grid, l , Dune::dvverb);
#endif
}


int main (int argc , char **argv)
{

  // this method calls MPI_Init, if MPI is enabled
  MPIHelper & mpihelper = MPIHelper::instance(argc,argv);
  int myrank = mpihelper.rank();
  int mysize = mpihelper.size();

  try {
    /* use grid-file appropriate for dimensions */
  /*
    if( argc < 2 )
    {
      std::cerr << "Usage: " << argv[0] << " <dgf file of hostgrid>" << std::endl;
      return 1;
    }
    */

    typedef P4estGrid< 2 , 2 > GridType ;
    GridPtr< GridType > gridPtr = GridPtr< GridType >( "2dcube.dgf" );
    GridType& grid = *gridPtr;
    std::cout << "Grid size = " << grid.size( 0 ) << std::endl;

    //printGrid( grid.leafGridView() );
    checkSerial( grid );


    VTKWriter< GridType :: LeafGridView > vtk( grid.leafGridView() );
    vtk.write( "p4est" );

  }
  catch (const Dune::Exception &e)
  {
    std::cerr << e << std::endl;
    return 1;
  }
  catch (...)
  {
    std::cerr << "Generic exception!" << std::endl;
    return 2;
  }

  return 0;
}
