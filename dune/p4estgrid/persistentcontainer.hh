/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_PERSISTENTCONTAINER_HH
#define DUNE_P4ESTGRID_PERSISTENTCONTAINER_HH

#include <unordered_map>

#include <dune/grid/utility/persistentcontainer.hh>
#include <dune/grid/utility/persistentcontainervector.hh>

namespace Dune
{
  // PersistentContainer for P4estGrid
  // -------------------------------------

  template< class G, class T >
  class P4estPersistentContainer
  : public PersistentContainerVector< G, typename G::HierarchicIndexSet, std::vector< T > >
  {
    typedef PersistentContainerVector< G, typename G::HierarchicIndexSet, std::vector< T > > Base;

  public:
    typedef typename Base::Grid Grid;
    typedef typename Base::Value Value;

    /** \brief \deprecated typedef of class Grid */
    typedef typename Base::Grid GridType;

    P4estPersistentContainer( const Grid &grid, int codim, const Value &value = Value() )
    : Base( grid.hierarchicIndexSet(), codim, value )
    {}
  };


  template< int dim, int dimworld, P4estType elType, class ctype, class T >
  class PersistentContainer< P4estGrid< dim, dimworld, elType, ctype > , T >
    : public P4estPersistentContainer< P4estGrid< dim, dimworld, elType, ctype >, T>
  {
    typedef P4estPersistentContainer< P4estGrid< dim, dimworld, elType, ctype >, T > Base;

  public:
    typedef typename Base::Grid Grid;
    typedef typename Base::Value Value;

    /** \brief \deprecated typedef of class Grid */
    typedef typename Base::Grid GridType;

    PersistentContainer ( const Grid &grid, int codim, const Value &value = Value() )
    : Base( grid, codim, value )
    {}
  };

} // end namespace Dune

#endif // end DUNE_P4ESTGRID_PERSISTENTCONTAINER_HH
