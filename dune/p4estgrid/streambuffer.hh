/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_STREAMBUFFER_HH
#define DUNE_P4ESTGRID_STREAMBUFFER_HH

#include <cassert>
#include <algorithm>
#include <vector>
#include <set>
#include <map>

namespace Dune
{
  namespace detail
  {
    class SimpleStreamBuffer
    {
      typedef std::vector< char >  BufferType;

      mutable BufferType buffer_;
      const double factor_;
      mutable size_t pos_;
  public:
      /** \brief constructor taking overestimation factor */
      SimpleStreamBuffer( const double factor = 1.1 )
        : buffer_(), factor_( factor )
      {
        resetReadPosition();
      }

      /** \brief constructor taking size and overestimation factor */
      SimpleStreamBuffer( const size_t size, const double factor = 1.1 )
        : buffer_( size ), factor_( factor )
      {
        resetReadPosition();
      }

      /** \brief clear the buffer */
      void clear() { buffer_.clear(); resetReadPosition(); }
      /** \brief reset read position of buffer to beginning */
      void resetReadPosition() { pos_ = 0 ; }

      size_t position() const { return pos_; }

      /** \brief return size of buffer */
      size_t size() const { return buffer_.size(); }

      /** \brief reserve memory for 'size' entries  */
      void reserve( const size_t size )
      {
        buffer_.reserve( size );
      }

      /** \brief resize buffer to 'size' entries times data size */
      void resize( const size_t size )
      {
        buffer_.resize( size );
      }

    protected:
      template <class T>
      void writeToBuffer( const T& value, const size_t tsize, const size_t pos )
      {
        // copy value to buffer
        std::copy_n( reinterpret_cast<const char *> (&value), tsize, buffer_.data()+pos );
      }

    public:
      /** \brief write value to buffer, value must implement the operator= correctly (i.e. no internal pointers etc.) */
      template <class T>
      void write( const T& value )
      {
        // union to access bytes in value
        const size_t tsize = sizeof( T );
        size_t pos = buffer_.size();
        const size_t sizeNeeded = pos + tsize ;
        // reserve with some 10% overestimation
        if( buffer_.capacity() < sizeNeeded )
        {
          reserve( size_t(factor_ * sizeNeeded) ) ;
        }
        // resize to size need to store value
        buffer_.resize( sizeNeeded );

        // copy value to buffer
        writeToBuffer( value, tsize, pos );
      }

      void write( const std::string& str)
      {
        int size = str.size();
        write(size);
        for (int k = 0; k < size; ++k)
        {
          write(str[k]);
        }
      }

      /** \brief read value from buffer, value must implement the operator= correctly (i.e. no internal pointers etc.) */
      template <class T>
      void read( T& value ) const
      {
        // read bytes from stream and store in value
        const size_t tsize = sizeof( T );
        assert( pos_ + tsize <= buffer_.size() );
        std::copy_n( buffer_.data()+pos_, tsize, reinterpret_cast<char *> (&value) );
        pos_ += tsize;
      }

      void read( std::string& str) const
      {
        int size = 0;
        read(size);
        str.resize(size);
        for (int k = 0; k < size; ++k)
        {
          read(str[k]);
        }
      }

      /** \brief return pointer to buffer and size for use with MPI functions */
      std::pair< char* , int > buffer() const
      {
        return std::make_pair( buffer_.data(), int(buffer_.size()) );
      }

      /** \brief return pointer to buffer */
      char* data ()
      {
        return buffer_.data();
      }

      /** \brief return pointer to buffer and size for use with MPI functions */
      std::vector< void* > dataPointers(const size_t length, const size_t dataSize )
      {
        std::vector< void* > vec( length, nullptr );
        for( size_t i=0; i<length; ++ i )
          vec[ i ] = buffer_.data() + i*dataSize;
        return vec;
      }
    };
  } // namespace P4Est
} // namespace Dune
#endif // DUNE_P4ESTGRID_STREAMBUFFER_HH
