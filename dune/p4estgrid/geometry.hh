/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_GEOMETRY_HH
#define DUNE_P4ESTGRID_GEOMETRY_HH

#include <dune/geometry/multilineargeometry.hh>
#include <dune/grid/common/geometry.hh>
#include <dune/p4estgrid/entityinfo.hh>

#include <dune/p4estgrid/memory.hh>

#include <dune/p4estgrid/mappings.hh>

namespace Dune
{

  namespace detail
  {
    template <class ctype, int cdim, bool cartesian>
    class MyP4estGeometryImplementation
    {
    public:
      typedef FieldVector<ctype, cdim> CoordinateVectorType;

      static const signed char invalid      = -1; // means geometry is not meaningful
      static const signed char updated      =  0; // means the point values have been set
      static const signed char buildmapping =  1; // means updated and mapping was build

      template <int dim, int corners, class Mapping>
      class GeometryImplBase
      {
      private:
        // prohibited due to reference counting
        GeometryImplBase( const GeometryImplBase& );

      public:
        //! number of corners
        static const int corners_ = corners ;

        // type of container for reference elements
        typedef ReferenceElements< ctype, dim > ReferenceElementContainerType;

        typedef std::decay_t< decltype( ReferenceElementContainerType::general( std::declval< const Dune::GeometryType & >() ) ) > ReferenceElementType;
      protected:
        //! the vertex coordinates
        typedef FieldMatrix<ctype, corners , cdim>  CoordinateMatrixType;

        // select coordinate storage for coord_ (pointer for dim == 3)
        typedef CoordinateMatrixType  CoordinateStorageType;

        //! the type of the mapping
        typedef Mapping     MappingType;

        //! to coordinates
        CoordinateStorageType coord_ ;

        //! the mapping
        MappingType map_;

        //! volume of element
        double volume_ ;

        const ReferenceElementType& refElem_;

        //! the status (see different status above)
        signed char status_ ;

      public:
        //! reference counter used by SharedPointer
        unsigned int refCount_;

        //! default constructor
        GeometryImplBase()
          : coord_(),
            map_(),
            volume_( 1.0 ),
            refElem_( ReferenceElementContainerType::cube() )
        {
          invalidate();
        }

        const ReferenceElementType& refElem() const { return refElem_; }

        // return coordinate vector
        inline const CoordinateVectorType& corner(const int i) const
        {
          assert ( valid() );
          assert ( i>=0 && i<corners_ );
          return coord_[i];
        }

        template <class CoordProvider>
        void update(const CoordProvider& ) const
        {
          DUNE_THROW(InvalidStateException,"This method should not be called!");
        }

        // update geometry in father coordinates (default impl)
        template <class GeometryImp>
        inline void updateInFather(const GeometryImp &fatherGeom ,
                                   const GeometryImp &myGeom)
        {
          // this version is only for the 2d elements
          assert( dim == 2 );

          // compute the local coordinates in father refelem
          for(int i=0; i < myGeom.corners() ; ++i)
          {
            // calculate coordinate
            coord_[i] = fatherGeom.local( myGeom.corner( i ) );

            // to avoid rounding errors
            for(int j=0; j<cdim; ++j)
            {
              if ( coord_[i][j] < 1e-16) coord_[i][j] = 0.0;
            }
          }

          status_ = updated ;
        }

        // set status to invalid
        void invalidate () { status_ = invalid ; }

        // return true if geometry is valid
        bool valid () const { return status_ != invalid ; }

        // set volume
        void setVolume( const double volume ) { volume_ = volume ; }

        // return volume
        double volume() const { return volume_; }
      };

      //! general type of geometry implementation
      template <int dummy, int dim> class GeometryImpl;
    public:
      // geometry implementation for edges and vertices
      template <int dummy, int dim>
      class GeometryImpl : public GeometryImplBase< dim, dim+1, Dune::detail::LinearMapping<cdim, dim> >
      {
      protected:
        typedef GeometryImplBase< dim, dim+1, detail::LinearMapping<cdim, dim> > BaseType;

        using BaseType :: coord_ ;
        using BaseType :: map_ ;
        using BaseType :: status_ ;

        typedef typename BaseType :: MappingType  MappingType ;
      public:
        using BaseType :: corners_ ;
        using BaseType :: corner ;
        using BaseType :: update ;
        using BaseType :: valid ;

        inline MappingType& mapping()
        {
          assert ( valid() );
          if( status_ == buildmapping ) return map_;

          map_.buildMapping( coord_[0] );
          status_ = buildmapping ;
          return map_;
        }

        // update vertex
        template <class CoordProvider>
        inline void update(const CoordProvider& coords)
        {
          assert ( corners_ == 1 );
          coord_[ 0 ] = coords[ 0 ];
          // we need to update the mapping
          status_ = updated ;
        }
      };

      // geometry implementation for edges and vertices
      template <int dummy>
      class GeometryImpl<dummy,1>
        : public GeometryImplBase< 1, 2, Dune::detail::LinearMapping<cdim, 1> >
      {
      protected:
        enum { dim = 1 };
        typedef GeometryImplBase< dim, dim+1, Dune::detail::LinearMapping<cdim, dim> > BaseType;

        using BaseType :: coord_ ;
        using BaseType :: map_ ;
        using BaseType :: status_ ;

        typedef typename BaseType :: MappingType  MappingType;
      public:
        using BaseType :: corners_ ;
        using BaseType :: corner ;
        using BaseType :: update ;
        using BaseType :: valid ;

        inline MappingType& mapping()
        {
          assert ( valid() );
          if( status_ == buildmapping ) return map_;

          map_.buildMapping( coord_[0], coord_[1] );
          status_ = buildmapping ;
          return map_;
        }

        // update edge
        // update geometry coordinates
        template <class CoordProvider>
        inline void update(const CoordProvider& coords )
        {
          for( int i=0; i<corners_; ++i )
          {
            coord_[ i ] = coords[ i ];
          }
          status_ = updated;
        }
      };

      ///////////////////////////////////////////////////////////////
      //
      //  hexa specializations
      //
      ///////////////////////////////////////////////////////////////

      // geom impl for quadrilaterals (also hexa faces)
      template <int dummy>
      class GeometryImpl<dummy, 2>
        : public GeometryImplBase< 2, 4, Dune::detail::BilinearMapping< cdim > >
      {
      protected:
        // dim = 2, corners = 4
        typedef GeometryImplBase< 2, 4, Dune::detail::BilinearMapping< cdim > > BaseType;

        using BaseType :: coord_ ;
        using BaseType :: map_ ;
        using BaseType :: status_ ;

        typedef typename BaseType :: MappingType  MappingType ;
      public:
        using BaseType :: corners_ ;
        using BaseType :: corner ;
        using BaseType :: update ;
        using BaseType :: valid ;

        // update geometry coordinates
        template <class CoordProvider>
        inline void update(const CoordProvider& coords )
        {
          for( int i=0; i<corners_; ++i )
          {
            coord_[ i ] = coords[ i ];
          }
          status_ = updated;
        }

        // return mapping (always up2date)
        inline MappingType& mapping()
        {
          assert ( valid() );
          if( status_ == buildmapping ) return map_;

          map_.buildMapping( coord_[0], coord_[1], coord_[2], coord_[3] );
          status_ = buildmapping ;
          return map_;
        }
      };

      // geometry impl for hexahedrons
      template <int dummy>
      class GeometryImpl<dummy, 3>
        : public GeometryImplBase< 3, 8, Dune::detail::TrilinearMapping >
      {
      protected:
        // dim = 3, corners = 8
        typedef GeometryImplBase< 3, 8, Dune::detail::TrilinearMapping > BaseType;

        using BaseType :: coord_ ;
        using BaseType :: map_ ;
        using BaseType :: status_ ;

        typedef typename BaseType :: MappingType  MappingType ;
        typedef typename BaseType :: CoordinateMatrixType CoordinateMatrixType;
      public:
        using BaseType :: corners_ ;
        using BaseType :: corner ;
        using BaseType :: update ;
        using BaseType :: valid ;

        //! constructor creating geo impl
        GeometryImpl() : BaseType()
        {
        }

        // update geometry coordinates
        template <class CoordProvider>
        inline void update(const CoordProvider& coords )
        {
          for( int i=0; i<corners_; ++i )
          {
            coord_[ i ] = coords[ i ];
          }
          status_ = updated;
        }

        // update geometry in father coordinates
        template <class GeometryImp>
        inline void updateInFather(const GeometryImp &fatherGeom ,
                                   const GeometryImp &myGeom)
        {
          CoordinateMatrixType& coord = coord_;
          // compute the local coordinates in father refelem
          for(int i=0; i < myGeom.corners() ; ++i)
          {
            // calculate coordinate
            coord[i] = fatherGeom.local( myGeom.corner( i ) );

            // to avoid rounding errors
            for(int j=0; j<cdim; ++j)
            {
              if ( coord[i][j] < 1e-16) coord[i][j] = 0.0;
            }
          }

          status_ = updated ;
        }

        // return mapping (always up2date)
        inline MappingType& mapping()
        {
          assert ( valid() );
          if( status_ == buildmapping ) return map_;

          map_.buildMapping( corner( 0 ), corner( 1 ), corner( 2 ), corner( 3 ),
                             corner( 4 ), corner( 5 ), corner( 6 ), corner( 7 ) );

          status_ = buildmapping;
          return map_;
        }

        // set status to invalid
        void invalidate () { status_ = invalid ; }

        // return true if geometry is valid
        bool valid () const { return status_ != invalid ; }
      };

    }; // end of class GeometryImplementation

#if 1
    // specialization for Cartesian grids
    template <class ctype, int cdim>
    class MyP4estGeometryImplementation< ctype, cdim, true>
    {
    public:
      typedef FieldVector<ctype, cdim> CoordinateVectorType;

      static const signed char invalid      = -1; // means geometry is not meaningful
      static const signed char updated      =  0; // means the point values have been set
      static const signed char buildmapping =  1; // means updated and mapping was build

      //! general type of geometry implementation
      template <int dummy, int dim>
      class GeometryImpl
      {
      private:
        // prohibited due to reference counting
        GeometryImpl( const GeometryImpl& );

      public:
        //! number of corners
        static const int corners_ = std::pow( 2, dim );

        // type of container for reference elements
        typedef ReferenceElements< ctype, dim > ReferenceElementContainerType;

        typedef std::decay_t< decltype( ReferenceElementContainerType::general( std::declval< const Dune::GeometryType & >() ) ) > ReferenceElementType;
      protected:
        typedef CartesianMapping< ctype, cdim, dim > Mapping;

        //! the type of the mapping
        typedef Mapping     MappingType;

        //! the mapping
        MappingType map_;

        //! volume of element
        double volume_ ;

        const ReferenceElementType& refElem_;

        //! the status (see different status above)
        signed char status_ ;

      public:
        //! reference counter used by SharedPointer
        unsigned int refCount_;

        //! default constructor
        GeometryImpl()
          : map_(),
            volume_( 1.0 ),
            refElem_( ReferenceElementContainerType::cube() )
        {
          invalidate();
        }

        const ReferenceElementType& refElem() const { return refElem_; }

        // return coordinate vector
        inline auto corner(const int i) const
        {
          assert ( valid() );
          assert ( i>=0 && i<corners_ );
          return map_.impl().corner( i );
        }

        // return mapping (always up2date)
        inline MappingType& mapping()
        {
          assert ( valid() );
          assert ( status_ == buildmapping );
          return map_;
        }

        template <class CoordProvider>
        inline void update(const CoordProvider& coords )
        {
          if constexpr ( dim == 1 )
          {
            std::bitset<cdim> axes;
            map_.buildMapping( coords[0], coords[1], axes );
          }

          if constexpr ( dim == 2 )
          {
            std::bitset<cdim> axes;
            map_.buildMapping( coords[0], coords[3] );
          }

          if constexpr ( dim == 3 )
            map_.buildMapping( coords[0], coords[7] );

          status_ = buildmapping;
        }

        void update(const CoordinateVectorType& lower, const CoordinateVectorType& upper, const std::bitset<cdim>& axes)
        {
          map_.buildMapping( lower, upper );
          status_ = buildmapping;
        }

        // update geometry in father coordinates (default impl)
        template <class GeometryImp>
        inline void updateInFather(const GeometryImp &fatherGeom ,
                                   const GeometryImp &myGeom)
        {
          /*
          // this version is only for the 2d elements
          assert( dim == 2 );

          // compute the local coordinates in father refelem
          for(int i=0; i < myGeom.corners() ; ++i)
          {
            // calculate coordinate
            coord_[i] = fatherGeom.local( myGeom.corner( i ) );

            // to avoid rounding errors
            for(int j=0; j<cdim; ++j)
            {
              if ( coord_[i][j] < 1e-16) coord_[i][j] = 0.0;
            }
          }

          status_ = updated ;
          */
        }

        // set status to invalid
        void invalidate () { status_ = invalid ; }

        // return true if geometry is valid
        bool valid () const { return status_ != invalid ; }

        // set volume
        void setVolume( const double volume ) { volume_ = volume ; }

        // return volume
        double volume() const { return volume_; }
      };
    };
#endif
  } // end namespace detail


  template< int mydim, int cdim, class Grid >
  class P4estGridGeometry :
    public GeometryDefaultImplementation<mydim, cdim, Grid, P4estGridGeometry>
  {
    typedef typename std::remove_const< Grid >::type::Traits Traits;
    typedef P4estGridGeometry< mydim, cdim, Grid > This;

  public:
    typedef typename Traits :: ctype ctype;

    static const bool cartesian = false ; // Grid :: cartesian ;

    // type of specialized geometry implementation
    typedef typename detail::MyP4estGeometryImplementation<ctype, cdim, cartesian> ::
        template GeometryImpl<0, mydim> GeometryImplType;

    typedef typename GeometryImplType :: ReferenceElementType  ReferenceElementType;

    static const int mydimension = mydim ;
    static const int dimension   = Traits :: dimension;
    static const int codimension = dimension - mydimension ;

    static const int corners_ = GeometryImplType::corners_;

    typedef Dune :: P4est< dimension, Grid::dimensionworld > PForest ;
    typedef typename PForest :: connectivity_t connectivity_t;
    typedef P4estEntityInfo< dimension > EntityInfo ;

    typedef typename Traits :: ExtraData  ExtraData;

#if 0
    struct P4estMultiLinearGeometryTraits
      : public MultiLinearGeometryTraits< ctype >
    {
      template< int dim >
      struct hasSingleGeometryType
      {
        static const bool v = true;
        static const unsigned int topologyId = GeometryTypes::cube(dim).id();
      };
    };

    typedef MultiLinearGeometry< ctype, mydim, cdim, P4estMultiLinearGeometryTraits > Mapping;
    typedef typename Mapping :: GlobalCoordinate   GlobalCoordinate;
    typedef typename Mapping :: LocalCoordinate    LocalCoordinate;
    /** \brief Type used for Jacobian matrices
     *
     * \note This is not a FieldMatrix but a proxy type that can be assigned
     *       to a FieldMatrix.
     */
    typedef typename Mapping::JacobianTransposed JacobianTransposed;
    /** \brief Type used for Jacobian matrices
     *
     * \note This is not a FieldMatrix but a proxy type that can be assigned
     *       to a FieldMatrix.
     */
    typedef typename Mapping::JacobianInverseTransposed JacobianInverseTransposed;

#endif

    //typedef GenericGeometry::DefaultGeometryTraits< ctype, dimension, Traits :: dimensionworld, false > GeometryTraits ;
    //typedef GenericGeometry::CachedMapping< Topology, GeometryTraits > Mapping;

    //! type of local coordinates
    typedef FieldVector<ctype, mydim> LocalCoordinate;

    //! type of the global coordinates
    typedef FieldVector<ctype, cdim > GlobalCoordinate;

    //! type of jacobian inverse transposed
    typedef FieldMatrix<ctype,cdim,mydim> JacobianInverseTransposed;

    //! type of jacobian transposed
    typedef FieldMatrix< ctype, mydim, cdim > JacobianTransposed;

  protected:
    typedef typename Traits::ReferenceElementType Codim0ReferenceElementType;

    struct CoordReader
    {
      CoordReader( ExtraData data,
                   const EntityInfo& info,
                   const Codim0ReferenceElementType& refElem )
        : data_( data ),
          info_( info ),
          connectivity_( data->connectivity() ),
          // here we need the element's reference element (i.e. dim = Grid::dimension)
          refElem_( refElem ),
          factor_( PForest :: quadrant_len( info.level() ) )
          //, gridWidth_( data_->gridWidth() )
      {
        /*
        if constexpr ( cartesian )
        {
          // first coordinate get as usual
          origin_ = coord( 0, std::false_type() );
        }
        */
      }

      typedef typename PForest :: qcoord_t  qcoord_t;
      ExtraData data_;
      const EntityInfo& info_;
      connectivity_t* connectivity_;
      const Codim0ReferenceElementType& refElem_;
      const qcoord_t factor_;
      mutable double vxyz_[3];

      //GlobalCoordinate origin_;
      //const GlobalCoordinate& gridWidth_;

      template <bool cart>
      GlobalCoordinate coord( const int pvx, std::integral_constant<bool, cart> ) const
      {
        assert( info_.valid() );
        const int subEntity_ = info_.subEntity();
        const int vx = ( subEntity_ < 0 ) ? pvx :
          refElem_.subEntity( subEntity_, codimension, pvx, dimension );

        const qcoord_t x = (vx&1) * factor_;
        const qcoord_t y = ((vx&2) >> 1) * factor_ ;
        const qcoord_t z = ((vx&4) >> 2) * factor_ ;

        /*
        if ( cart && subEntity_ < 0 )
        {
          // does not work yet
          GlobalCoordinate crd( origin_ );
          //std::cout << origin_ << std::endl;
          //std::cout << gridWidth_ << std::endl;
          //std::cout << x << " " << y << std::endl;

          crd[0] += x * gridWidth_[0];
          crd[1] += y * gridWidth_[1];
          if constexpr ( dimension == 3 )
            crd[2] += z * gridWidth_[2];
          //auto c2 = coord( pvx, std::false_type() );
          //std::cout << crd << " " << c2 << std::endl;
          //assert( (crd - c2).two_norm() < 1e-8 );
          return crd;
        }
        else
        */
        {
          //std::cout << "Got " << vx << " for sub " << subEntity_ << std::endl;
          assert( connectivity_ );
          PForest :: qcoord_to_vertex( connectivity_,
                                       info_.treeId(),
                                       info_.x() + x,
                                       info_.y() + y,
                                       info_.z() + z,
                                       vxyz_);

          // copy to coordinate type
          GlobalCoordinate c;
          for( int i=0; i<cdim; ++i )
            c[i] = vxyz_[i];
          return c;

          // apply mapping from p4est cube to real world coordinates
          //return data_->vertex2world( info_.treeId(), vxyz_ );
        }
      }

      GlobalCoordinate operator [] ( const int pvx ) const
      {
        //return coord( pvx, std::integral_constant<bool, cartesian> () );
        return coord( pvx, std::false_type() );
      }
    };

    template <class ElementGeometry, class FaceGeometry>
    struct FaceCoords
    {
      FaceCoords( const ElementGeometry& elementGeo,
                  const FaceGeometry& faceGeo )
        : elementGeo_( elementGeo ),
          faceGeo_( faceGeo )
      {
      }

      const ElementGeometry& elementGeo_;
      const FaceGeometry& faceGeo_;

      GlobalCoordinate operator [] ( const int vx ) const
      {
        GlobalCoordinate coord;
        assert( vx < faceGeo_.corners() );
        coord = elementGeo_.local( faceGeo_.corner( vx ) );
        return coord;
      }
    };

    template <class ElementGeometry>
    struct CoordMapper
    {
      CoordMapper( const ElementGeometry& elementGeo,
                   const std::array<int, corners_>& vxMap )
        : elementGeo_( elementGeo ),
          vxMap_( vxMap )
      {
      }

      const ElementGeometry& elementGeo_;
      const std::array<int, corners_>& vxMap_;

      GlobalCoordinate operator [] ( const int vx ) const
      {
        return elementGeo_.corner( vxMap_[ vx ] );
      }
    };

    struct UnitCoord
    {
      UnitCoord () {}

      GlobalCoordinate operator [] ( const int vx ) const
      {
        static const double vxyz[8][3] = { {0,0,0},
                                           {1,0,0},
                                           {0,1,0},
                                           {1,1,0},
                                           {0,0,1},
                                           {1,0,1},
                                           {0,1,1},
                                           {1,1,1} };
        GlobalCoordinate coord ;
        assert( vx >= 0 && vx < 8 );
        for(int i=0; i<dimension; ++ i) coord[ i ] = vxyz[ vx ][ i ];
        return coord ;
      }
    };

    struct ChildCoord
    {
      std::vector< GlobalCoordinate > coords_;
      const int childIndex_ ;
      ChildCoord( const int childIndex )
        : coords_( 8 ), childIndex_( childIndex )
      {
        static const double vxyz[8][3] = { {0,0,0},
                                           {1,0,0},
                                           {0,1,0},
                                           {1,1,0},
                                           {0,0,1},
                                           {1,0,1},
                                           {0,1,1},
                                           {1,1,1} };

        GlobalCoordinate origin ;
        for( int i = 0; i < mydimension; ++i )
          origin[ i ] = 0.5 * ((childIndex_ >> i) & 1);

        const int numChildren = ( 1 << mydimension );
        for( int i=0; i<numChildren; ++i )
        {
          for( int j = 0; j<mydimension; ++j )
          {
            coords_[ i ][ j ] = vxyz[ i ][ j ];
          }
          coords_[ i ] *= 0.5 ;
          coords_[ i ] += origin;

          //std::cout << "Child " << childIndex_ << " Coord[ " << i << " ] = " << coords_[ i ] << std::endl;
        }
      }

      GlobalCoordinate operator [] ( const int vx ) const
      {
        return coords_[ vx ];
      }

      operator const std::vector<GlobalCoordinate>& () const {return coords_; }
    };

  public:
    P4estGridGeometry () : geoImplPtr_()
    {
      invalidate();
    }

    explicit P4estGridGeometry ( const int childIndex )
      : geoImplPtr_()
    {
      invalidate();
      ChildCoord coords( childIndex );
      geoImpl().update( coords );
    }

    GeometryType type () const
    {
      return GeometryTypes::cube( mydimension );
    }

    /** \brief Return the number of corners */
    int corners () const
    {
      return corners_;
    }

    /** \brief Return the world coordinates of the i-th corner */
    GlobalCoordinate corner ( const int i ) const
    {
      return geoImpl().corner( i );
    }

    /** \brief Map local to global coordinates */
    GlobalCoordinate global ( const LocalCoordinate &local ) const
    {
      GlobalCoordinate global;
      geoImpl().mapping().map2world(local, global);
      return global;
    }

    /** \brief Map global to local coordinates */
    LocalCoordinate local ( const GlobalCoordinate &global ) const
    {
      LocalCoordinate local;
      geoImpl().mapping().world2map(global, local);
      return local;
    }

    /** \brief return center of element */
    GlobalCoordinate center () const
    {
      return global( LocalCoordinate(0.5) );
    }

    /** \brief Return true if this is an affine geometry */
    bool affine () const
    {
      return geoImpl().mapping().affine();
    }

    /** \brief Return the factor \$|det F|\$ that appears in the integral transformation formula */
    ctype integrationElement ( const LocalCoordinate &local ) const
    {
      return geoImpl().mapping().det( local );
    }

    /** \brief Return the volume of the element */
    ctype volume () const
    {
      // TODO: Needs to be faster
      return integrationElement(LocalCoordinate(0.5));
    }


    /** \brief Compute the transpose of the Jacobian matrix of the
     *         transformation from the reference element into the world
     *         space
     */
    const JacobianTransposed jacobianTransposed ( const LocalCoordinate &local ) const
    {
      return geoImpl().mapping().jacobianTransposed( local );
    }

      /** \brief Compute the transpose of the inverse Jacobian matrix of the transformation
          from the reference element into the world space */
    const JacobianInverseTransposed jacobianInverseTransposed ( const LocalCoordinate &local ) const
    {
      return geoImpl().mapping().jacobianInverseTransposed( local );
    }

    //! return reference element
    const ReferenceElementType& referenceElement () const { return geoImpl().refElem(); }

    //! build geometry
    void build (ExtraData data, const EntityInfo& info, const Codim0ReferenceElementType& refElem  )
    {
      CoordReader coordReader( data, info, refElem );
      geoImpl().update( coordReader );
    }

    template <class ElementGeometry, class FaceGeometry>
    void build( const ElementGeometry& elementGeo,
                const FaceGeometry& faceGeo )
    {
      typedef FaceCoords< ElementGeometry, FaceGeometry > FaceCoordsType;
      FaceCoordsType coords ( elementGeo, faceGeo ) ;
      geoImpl().update( coords );
    }

    template <class ElementGeometry>
    void build( const ElementGeometry& elementGeo,
                const std::array<int, corners_ >& vxMap )
    {
      CoordMapper<ElementGeometry> coordMap( elementGeo, vxMap );
      geoImpl().update( coordMap );
    }

    //! invalidate geometry implementation to avoid errors
    void invalidate ()
    {
      geoImplPtr_.invalidate();
    }

    //! invalidate geometry implementation to avoid errors
    bool valid () const { return geoImpl().valid(); }

  protected:
    // return reference to geometry implementation
    GeometryImplType& geoImpl() const
    {
      return *geoImplPtr_;
    }

    // const ReferenceElementType& refElem_;
    mutable detail::P4estSharedPointer< GeometryImplType > geoImplPtr_;
  };
}

#endif // #ifndef DUNE_P4ESTGRID_GEOMETRY_HH
