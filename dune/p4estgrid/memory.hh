#ifndef DUNE_P4ESTGRID_MEMORY_HH
#define DUNE_P4ESTGRID_MEMORY_HH

#include <cassert>
#include <cstdlib>
#include <vector>

#include <dune/p4estgrid/indexstack.hh>

namespace Dune
{
  namespace detail
  {
    //! organize the memory management for entities used by the IntersectionIterator
    template <class Object>
    class P4estMemoryProvider
    {
      static const int maxStackObjects = 256;
      typedef P4estGridFiniteStack< Object *, maxStackObjects > StackType;

      // stack to store object pointers
      StackType objStack_;

      // return reference to object stack
      StackType &objStack () { return objStack_; }
    public:
      // type of object to be stored
      typedef Object ObjectType;

      //! default constructor
      P4estMemoryProvider()
        : objStack_()
      {}

      //! copy constructor
      P4estMemoryProvider( const P4estMemoryProvider& org )
        : objStack_()
      {}

      //! call deleteEntity
      ~P4estMemoryProvider ();

      //! return object, if created default constructor is used
      ObjectType* getEmptyObject ();

      //! free, move element to stack, returns NULL
      void freeObject (ObjectType * obj);

    protected:
      inline ObjectType * stackObject()
      {
        // make sure stack is not empty
        assert ( ! objStack().empty() );
        // finite stack does also return object on pop
        return objStack().pop();
      }
    };


    //************************************************************************
    //
    //  P4estMemoryProvider implementation
    //
    //************************************************************************
    template <class Object>
    inline typename P4estMemoryProvider<Object>::ObjectType *
    P4estMemoryProvider<Object>::getEmptyObject ()
    {
      if( objStack().empty() )
      {
        return new Object () ;
      }
      else
      {
        return stackObject();
      }
    }

    template <class Object>
    inline P4estMemoryProvider<Object>::~P4estMemoryProvider()
    {
      StackType& objStk = objStack();
      while ( ! objStk.empty() )
      {
        ObjectType * obj = objStk.pop();
        delete obj;
      }
    }

    template <class Object>
    inline void P4estMemoryProvider<Object>::freeObject( Object * obj )
    {
      // make sure we operate on the correct thread
      StackType& stk = objStack();
      if( stk.full() )
      {
        delete obj;
      }
      else
        stk.push( obj );
    }

    template <class ObjectImp>
    class ReferenceCountedObject
    {
    protected:
      // type of object to be reference counted
      typedef ObjectImp    ObjectType;

      // object (e.g. geometry impl or intersection impl)
      ObjectType object_;

      unsigned int& refCount() { return object_.refCount_; }
      const unsigned int& refCount() const { return object_.refCount_; }

    public:
      //! reset status and reference count
      void reset()
      {
        // reset reference counter
        refCount() = 1;

        // reset status of object
        object_.invalidate();
      }

      //! increase reference count
      void operator ++ () { ++ refCount(); }

      //! decrease reference count
      void operator -- () { assert ( refCount() > 0 ); --refCount(); }

      //! return true if object has no references anymore
      bool operator ! () const { return refCount() == 0; }

      //! return true if there exists more then on reference
      bool unique () const { return refCount() == 1 ; }

      const ObjectType& object() const { return object_; }
            ObjectType& object()       { return object_; }
    };

    template <class ObjectImp>
    class P4estSharedPointer
    {
    protected:
      typedef ObjectImp                                           ObjectType;
      typedef ReferenceCountedObject< ObjectType >                ReferenceCountedObjectType;
      typedef P4estMemoryProvider< ReferenceCountedObjectType >   MemoryPoolType;

      static MemoryPoolType& memoryPool()
      {
        static thread_local MemoryPoolType pool;
        return pool;
      }

    public:
      // default constructor
      P4estSharedPointer()
      {
        getObject();
      }

      // copy contructor making shallow copy
      P4estSharedPointer( const P4estSharedPointer& other )
      {
        assign( other );
      }

      // destructor clearing pointer
      ~P4estSharedPointer()
      {
        removeObject();
      }

      void getObject()
      {
        ptr_ = memoryPool().getEmptyObject();
        ptr().reset();
      }

      void assign( const P4estSharedPointer& other )
      {
        // copy pointer
        ptr_ = other.ptr_;

        // increase reference counter
        ++ ptr();
      }

      void removeObject()
      {
        // decrease reference counter
        -- ptr();

        // if reference count is zero free the object
        if( ! ptr() )
        {
          memoryPool().freeObject( ptr_ );
        }

        // reset pointer
        ptr_ = nullptr;
      }

      void invalidate()
      {
        // if pointer is unique, invalidate status
        if( ptr().unique() )
        {
          ptr().object().invalidate();
        }
        else
        {
          // if pointer is used elsewhere remove the pointer
          // and get new object
          removeObject();
          getObject();
        }
      }

      P4estSharedPointer& operator = ( const P4estSharedPointer& other )
      {
        if( ptr_ != other.ptr_ )
        {
          removeObject();
          assign( other );
        }
        return *this;
      }

      operator bool () const { return bool( ptr_ ); }

      bool operator == (const P4estSharedPointer& other ) const { return ptr_ == other.ptr_; }

      bool unique () const { return ptr().unique(); }

      // dereferencing
            ObjectType& operator* ()       { return ptr().object(); }
      const ObjectType& operator* () const { return ptr().object(); }

    protected:
      ReferenceCountedObjectType& ptr() { assert( ptr_ ); return *ptr_; }
      const ReferenceCountedObjectType& ptr() const { assert( ptr_ ); return *ptr_; }

      ReferenceCountedObjectType* ptr_;
    };

  }// namespace detail

} // namespace Dune

#endif // #ifndef DUNE_P4ESTGRID_MEMORY_HH
