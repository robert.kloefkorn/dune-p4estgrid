/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_BACKUPRESTORE_HH
#define DUNE_P4ESTGRID_BACKUPRESTORE_HH

#include <dune/p4estgrid/capabilities.hh>

namespace Dune
{

  // BackupRestoreFacilities
  // -----------------------

  template< class Grid, bool hasBackupRestoreFacilities = Capabilities::hasBackupRestoreFacilities< Grid > ::v >
  class P4estGridBackupRestoreFacilities
  {};

  template< class Grid >
  class P4estGridBackupRestoreFacilities< Grid, true >
  {
    typedef P4estGridBackupRestoreFacilities< Grid, true > This;

  protected:
    P4estGridBackupRestoreFacilities ()
    {}

  private:
    P4estGridBackupRestoreFacilities ( const This & );
    This &operator= ( const This & );

  public:
    bool writeGrid ( const std::string &filename, double time ) const
    {
      return false ; // asImp().hostGrid().template writeGrid< type >( filename, time );
    }

    bool readGrid ( const std::string &filename, double &time )
    {
      return false ;/*
      const bool success
        = asImp().hostGrid().template readGrid< type >( filename, time );
      asImp().update();
      return success;
      */
    }

  protected:
    const Grid &asImp () const
    {
      return static_cast< const Grid & >( *this );
    }

    Grid &asImp ()
    {
      return static_cast< Grid & >( *this );
    }
  };

}

#endif // #ifndef DUNE_P4ESTGRID_BACKUPRESTORE_HH
