/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_CAPABILITIES_HH
#define DUNE_P4ESTGRID_CAPABILITIES_HH

#include <dune/grid/common/capabilities.hh>
#include <dune/p4estgrid/declaration.hh>


namespace Dune
{

  // Capabilities
  // ------------

  namespace Capabilities
  {

    // Capabilities from dune-grid
    // ---------------------------

    template< int dim, int dimworld, P4estType elType, class ct >
    struct hasSingleGeometryType< P4estGrid< dim, dimworld, elType, ct > >
    {
      /** \brief all elements in \ref Dune::P4estGrid "P4estGrid" have the same geometry type */
      static const bool v = true;
      static const unsigned int topologyId = GeometryTypes::cube(dim).id();
    };

    /** \brief Is the grid Cartesian?
     *
     *  Cartesian grids satisfy the following properties:
     *  - all geometries are affine
     *  - The unit outer normal can be computed by the following code:
     *  \code
     *  FieldVector< ctype, dim > n( 0 );
     *  n[ face / 2 ] = ctype( 2*(face % 2) - 1 );
     *  \endcode
     *  .
     *
     *  \tparam  Grid  grid for which the information is desired
     */
    template< int dim, int dimworld, P4estType elType, class ct >
    struct isCartesian< P4estGrid< dim, dimworld, elType, ct > >
    {
      /** \brief \ref Dune::P4estGrid "P4estGrid" is a Cartesian grid */
      static const bool v = P4estGrid< dim, dimworld, elType, ct >::cartesian ;
    };

    template< int dim, int dimworld, P4estType elType, class ct, int codim >
    struct hasEntity< P4estGrid< dim, dimworld, elType, ct >, codim >
    {
      static const bool v = true ;
    };

    template< int dim, int dimworld, P4estType elType, class ct, int codim >
    struct hasEntityIterator< P4estGrid< dim, dimworld, elType, ct >, codim >
    {
      // iterators are only available for codimension 0
      static const bool v = (codim == 0);
    };

    template< int dim, int dimworld, P4estType elType, class ct, int codim >
    struct canCommunicate< P4estGrid< dim, dimworld, elType, ct >, codim >
    {
      static const bool v = false ; // canCommunicate< HostGrid, codim >::v;
    };


    template< int dim, int dimworld, P4estType elType, class ct >
    struct canCommunicate< P4estGrid< dim, dimworld, elType, ct >, 0 >
    {
      static const bool v = true ; // canCommunicate< HostGrid, codim >::v;
    };


    template< int dim, int dimworld, P4estType elType, class ct >
    struct hasBackupRestoreFacilities< P4estGrid< dim, dimworld, elType, ct > >
    {
      static const bool v = false ;
    };

    template< int dim, int dimworld, P4estType elType, class ct >
    struct isLevelwiseConforming< P4estGrid< dim, dimworld, elType, ct > >
    {
      static const bool v = true ;
    };

    template< int dim, int dimworld, P4estType elType, class ct >
    struct isLeafwiseConforming< P4estGrid< dim, dimworld, elType, ct > >
    {
      static const bool v = false ;
    };

    template< int dim, int dimworld, P4estType elType, class ct >
    struct threadSafe< P4estGrid< dim, dimworld, elType, ct > >
    {
      static const bool v = false;
    };

    // this means iterating over the grid is thread safe
    template< int dim, int dimworld, P4estType elType, class ct >
    struct viewThreadSafe< P4estGrid< dim, dimworld, elType, ct > >
    {
      static const bool v = true;
    };

  }

}

#endif // #ifndef DUNE_P4ESTGRID_CAPABILITIES_HH
