/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_DOMAINGEOMETRY_HH
#define DUNE_P4ESTGRID_DOMAINGEOMETRY_HH

#include <dune/common/typetraits.hh>
#include <dune/common/fvector.hh>

#include <dune/p4estgrid/p4est.hh>

#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#include <dune/alugrid/dgf.hh>
#endif

namespace Dune
{

  // DomainGeometry for P4estGrid
  // --------------------------------

  template< class ct, int dim, int dimworld >
  class P4estDomainGeometry
  {
    typedef P4estDomainGeometry< ct, dim, dimworld > ThisType;
  public:
    static const int dimension = dim;
    static const int dimensionworld = dimworld;

    typedef ct ctype;
    typedef FieldVector< ctype, dimensionworld > CoordinateType;

    typedef typename P4est< dimension, dimensionworld > :: topidx_t  topidx_t;
  protected:

    P4estDomainGeometry() {}
  public:
    virtual CoordinateType vertex2world(const topidx_t treeId, const double* vxyz) const = 0 ;
    virtual CoordinateType h() const = 0;
    // virtual ThisType* clone() const = 0;
  };

  template< class ct, int dim, int dimworld >
  class P4estIdentityDomain : public P4estDomainGeometry< ct, dim, dimworld>
  {
    typedef P4estDomainGeometry< ct, dim, dimworld > BaseType;
    typedef P4estIdentityDomain< ct, dim, dimworld > ThisType;

  public:
    using BaseType::dimension;
    using BaseType::dimensionworld;
    typedef typename BaseType :: CoordinateType CoordinateType;
    typedef typename BaseType :: topidx_t       topidx_t;

    P4estIdentityDomain()
    {
    }

    CoordinateType vertex2world(const topidx_t treeId, const double* vxyz) const final
    {
      CoordinateType coord;
      for(int i=0; i<dimensionworld; ++i)
      {
        coord[i] = vxyz[i];
      }
      return coord;
    }
    CoordinateType h () const final
    {
      return CoordinateType(0);
    }
  };

  template< class ct, int dim, int dimworld >
  class P4estCartesianDomain : public P4estDomainGeometry< ct, dim, dimworld>
  {
    typedef P4estDomainGeometry< ct, dim, dimworld > BaseType;
    typedef P4estCartesianDomain< ct, dim, dimworld > ThisType;

  public:
    using BaseType::dimension;
    using BaseType::dimensionworld;
    typedef typename BaseType :: CoordinateType CoordinateType;
    typedef typename BaseType :: topidx_t       topidx_t;

    P4estCartesianDomain( const std::array<int,dimension>& N,
                          const std::vector< double >& h )
      : N_( N ),
        lower_( 0. ),
        h_(),
        originZero_( true )
    {
      assert( int(h.size()) == dimensionworld );
      for(int i=0; i<dimensionworld; ++i)
      {
        h_[ i ]   = h[i];
      }
    }

    P4estCartesianDomain( const std::array<int,dimension>& N,
                          const std::vector< double >& lower,
                          const std::vector< double >& h )
      : N_( N ),
        lower_(),
        h_(),
        originZero_( false )
    {
      assert( int(h.size()) == dimensionworld );
      for(int i=0; i<dimensionworld; ++i)
      {
        lower_[i] = lower[i];
        h_[ i ]   = h[i];
      }
    }

    CoordinateType vertex2world(const topidx_t treeId, const double* vxyz) const final
    {
      CoordinateType coord;
      for(int i=0; i<dimensionworld; ++i)
      {
        coord[i] = vxyz[i]*h_[i];
      }

      if( ! originZero_ )
        coord += lower_;
      return coord;
    }

    CoordinateType h () const final
    {
      return h_;
    }

    // return copy of this class
    // BaseType* clone () const { return new ThisType(*this); }

  protected:
    std::array<int,dimension> N_;
    CoordinateType lower_;
    CoordinateType h_;
    const bool originZero_;
  };

  template< class ct, int dim, int dimworld >
  class P4estUnstructuredDomain : public P4estDomainGeometry< ct, dim, dimworld>
  {
    typedef P4estDomainGeometry< ct, dim, dimworld >      BaseType;
    typedef P4estUnstructuredDomain< ct, dim, dimworld >  ThisType;

  public:
    using BaseType::dimension;
    using BaseType::dimensionworld;
    typedef typename BaseType :: CoordinateType CoordinateType;
    typedef typename BaseType :: topidx_t       topidx_t;

    typedef Dune::ALUGrid<dim, dimworld, Dune::cube, Dune::nonconforming, Dune::ALUGridNoComm > ALUGridType;

    P4estUnstructuredDomain( const std::shared_ptr< ALUGridType >& gridPtr )
      : gridPtr_( gridPtr )
    {
    }

    CoordinateType vertex2world(const topidx_t treeId, const double* vxyz) const final
    {
      //std::cout << "treeid " << treeId << std::endl;
      //for( int i=0; i<dim; ++i)
      //  std::cout << vxyz[i] << " ";
      //std::cout << std::endl;

      CoordinateType coord;
      for(int i=0; i<dimensionworld; ++i)
      {
        coord[i] = vxyz[i];
      }
      return coord;
    }

    CoordinateType h () const final
    {
      return CoordinateType(0.0);
    }

    // return copy of this class
    // BaseType* clone () const { return new ThisType(*this); }

  protected:
    std::shared_ptr< ALUGridType > gridPtr_;
  };

} // namespace Dune

#endif // #ifndef DUNE_P4ESTGRID_DOMAINGEOMETRY_HH
