#ifndef DUNE_P4ESTGRID_DECLARATION_HH
#define DUNE_P4ESTGRID_DECLARATION_HH

#include <iostream>
#include <string>

namespace Dune
{
  //! \brief basic element types for P4estGrid
  enum class P4estType
  {
    simplex,   //!< use only simplex elements (i.e., triangles or tetrahedra)
    cube,      //!< use only cube elements (i.e., quadrilaterals or hexahedra)
    cartesian  //!< use only Cartesian type geometries (i.e., quadrilaterals or hexahedra)
  };

  /**
   * \brief unstructured parallel implementation of the DUNE grid interface
   *
   * P4estGrid implements the DUNE grid interface for 2D quadrilateral and 3D
   * hexahedral meshes using the p4est library. This grid can be locally adapted
   * (non-conforming) and used in parallel computations using dynamic load balancing.
   *
   * \tparam  dim         dimension of the grid (2 or 3)
   * \tparam  dimworld    dimension of the surrounding space (dim <= dimworld <=3)
   * \tparam  elType      type of elements (Dune::simplex or Dune::cube or Dune::cartesian)
   * \tparam  ctype       type of coordinate field (default is double)
   *
   */
  template< int dim, int dimworld, P4estType elType, class ctype = double >
  class P4estGrid;

  // remove at some point....
  struct P4estGridLoadBalanceHandleWithReserveAndCompress {};

} // end namespace Dune

namespace std {

  inline string to_string( const Dune::P4estType elType )
  {
    switch( elType )
    {
      case Dune::P4estType::simplex:
        return string("simplex");
      case Dune::P4estType::cube:
        return string("cube");
      case Dune::P4estType::cartesian:
        return string("cartesian");
    }
    cerr << "elementType2String: Unrecognized type" << endl;
    abort();
  }
}

#endif // #ifndef DUNE_P4ESTGRID_DECLARATION_HH
