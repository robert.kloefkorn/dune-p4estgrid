/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_INTERSECTION_HH
#define DUNE_P4ESTGRID_INTERSECTION_HH

#include <dune/p4estgrid/entitypointer.hh>

#include <dune/p4estgrid/memory.hh>

namespace Dune
{

  // P4estGridIntersection
  // ------------------

  template< class Grid >
  class P4estGridIntersection
  {
    typedef typename std::remove_const< Grid >::type::Traits Traits;
  protected:
    typedef typename Traits :: ExtraData   ExtraData ;

  public:
    typedef typename Traits::ctype ctype;

    struct TW
    {
      typedef int Twist;
    };
    typedef TW Twists;


    static const int dimension = Traits::dimension;
    static const int dimensionworld = Traits::dimensionworld;
    static const int mydimension = dimension-1;

    typedef P4est< dimension, dimensionworld > PForest ;
    typedef typename PForest :: topidx_t   topidx_t;
    typedef typename PForest :: quadidx_t  quadidx_t;
    typedef typename PForest :: quadrant_t quadrant_t;

    typedef P4estEntityInfo< dimension > EntityInfo ;

    typedef typename PForest :: mesh_face_neighbor_t mesh_face_neighbor_t ;

    typedef typename Traits::template Codim< 0 >::Entity Entity;
    typedef typename Traits::template Codim< 1 >::Geometry Geometry;
    typedef typename Traits::template Codim< 1 >::LocalGeometry LocalGeometry;

    typedef typename Entity :: Geometry :: GlobalCoordinate  NormalVector ;
    typedef typename Traits :: ReferenceElementType  ReferenceElementType;
    //typedef typename Traits::ReferenceCube::NormalVector NormalVector;

  private:
    typedef typename Traits::template Codim< 0 >::EntityImpl EntityImpl;

    typedef P4estGridGeometry< mydimension, dimensionworld, Grid > GeometryImpl;
    typedef P4estGridGeometry< dimension, dimensionworld, Grid > ElementGeometryImpl;

  public:
    P4estGridIntersection ()
      : P4estGridIntersection( nullptr )
    {}

    explicit P4estGridIntersection ( ExtraData data )
    : data_( data ),
      info_(),
      outsideInfo_(),
      geo_( GeometryImpl() ),
      geoInInside_( GeometryImpl() ),
      geoInOutside_( GeometryImpl() ),
      geoInside_(),
      //mappingGlobal_(),
      //outerNormal_(),
      indexInInside_( 0 ),
      indexInOutside_( 0 )
      //, mappingGlobalUp2Date_( false )
    {}

    //! start iterator
    P4estGridIntersection ( ExtraData data, const EntityInfo& info )
    : data_( data ),
      info_( info ),
      outsideInfo_(),
      geo_( GeometryImpl() ),
      geoInInside_( GeometryImpl() ),
      geoInOutside_( GeometryImpl() ),
      geoInside_(),
      //mappingGlobal_(),
      //outerNormal_(),
      indexInInside_( 0 ),
      indexInOutside_( 0 )
      //, mappingGlobalUp2Date_( false )
    {
      first( data, info, 0 );
      // increment();
    }

    //! start iterator
    P4estGridIntersection ( const EntityImpl& en, const int level, ExtraData data )
    : data_( data ),
      info_( en.info() ),
      outsideInfo_(),
      geo_( GeometryImpl() ),
      geoInInside_( GeometryImpl() ),
      geoInOutside_( GeometryImpl() ),
      geoInside_(),
      //mappingGlobal_(),
      //outerNormal_(),
      indexInInside_( 0 ),
      indexInOutside_( 0 )
      //, mappingGlobalUp2Date_( false )
    {
      first( en, 0, *data );
      // increment();
    }

    P4estGridIntersection ( const P4estGridIntersection &other )
    : data_( other.data_ ),
      info_( other.info_ ),
      outsideInfo_( other.outsideInfo_ ),
      geo_( GeometryImpl( other.geoImpl() ) ),
      geoInInside_( GeometryImpl( other.geoInsideImpl() ) ),
      geoInOutside_( GeometryImpl( other.geoOutsideImpl() ) ),
      geoInside_( other.geoInside_ ),
      faceNeighbor_( other.faceNeighbor_ ),
      indexInInside_( other.indexInInside_ ),
      indexInOutside_( other.indexInOutside_ )
      //, mappingGlobalUp2Date_( false )
    {}

    const P4estGridIntersection &operator= ( const P4estGridIntersection &other )
    {
      assign( other );
      return *this;
    }

    void assign( const P4estGridIntersection &other )
    {
      data_              = other.data_;
      info_              = other.info_;
      geoImpl()          = other.geoImpl() ;
      geoInsideImpl()    = other.geoInsideImpl() ;
      geoOutsideImpl()   = other.geoOutsideImpl() ;
      geoInside_         = other.geoInside_ ;
      outsideInfo_       = other.outsideInfo_;
      faceNeighbor_      = other.faceNeighbor_ ;
      indexInInside_     = other.indexInInside_;
      indexInOutside_    = other.indexInOutside_ ;
    }

    void done (const EntityImpl& entity)
    {
      done();
    }

    void done ()
    {
      info_ = EntityInfo();
      outsideInfo_ = EntityInfo();
      //geoInside_.invalidate();
    }

    void first( ExtraData d, const EntityImpl& entity )
    {
      info_ = entity.info();
      assert( info_.valid() );

      if( info_.isGhost() )
      {
        done();
        return ;
      }

      data_ = d;
      geoInside_ = entity.geometry().impl();

      typedef typename PForest :: forest_t forest_t;
      forest_t* forest = data()->forest() ;

      typedef typename PForest :: mesh_t   mesh_t;
      mesh_t* mesh = data()->mesh() ;

      /////////////////////////////////////////////////
      //  NOTE !!!
      //  the qaud needs to be one of the list
      //  stored in the p4est structures
      /////////////////////////////////////////////////

      quadrant_t* leafQuad = info_.quadPtr();
      assert( leafQuad );
      //indexInInside_ = -1;

      PForest ::
      mesh_face_neighbor_init( &faceNeighbor_,
                               forest,
                               data()->ghostLayer(),
                               mesh,
                               mesh->quad_to_tree[ info_.quadId() ],
                               leafQuad
                             );

      indexInInside_ = faceNeighbor_.face ;

      // increment to first intersection
      increment();
    }

    void increment()
    {
      // reset flags
      geoInsideImpl().invalidate();
      geoOutsideImpl().invalidate();
      geoImpl().invalidate();

      topidx_t  treeId = -1;
      quadidx_t quadId = -1;

      const int rank = data()->comm().rank();
      int nbRank = rank;

      indexInInside_ = faceNeighbor_.face ; // local face number
      // faceNeighbor_.face  points to next face
      // for quad to face lookup table see p4est_mesh.h
      // this is stored in indexInOutside_ soon !!!

      quadrant_t* quad = PForest :: mesh_face_neighbor_next( &faceNeighbor_,
                                                  // the following are return values
                                                  treeId, quadId,
                                                  indexInOutside_ ,
                                                  nbRank );

      // if quad == 0 then the iterator is finished
      if( ! quad )
      {
        done();
        return ;
      }

      // ghost neighbor if the rank is different from our own
      if( nbRank != rank )
      {
        // in this case the quadId has to be adjusted
        // by the number of interior quads
        quadId += faceNeighbor_.mesh->local_num_quadrants;
      }
      else
      {
        typedef typename PForest :: tree_t   tree_t;
        tree_t* tree = PForest :: tree_array_index (faceNeighbor_.p4est->trees, treeId );
        // add offset to make quadId consistent with element storage
        quadId += tree->quadrants_offset;
      }

      indexInOutside_ += 8;
      indexInOutside_ %= dimension * 2 ;

      assert( indexInOutside_ >= 0 && indexInOutside_ < dimension*2 );
      outsideInfo_ = data()->element( quadId );

      /*
      std::cout << "boundary " << boundary() << std::endl;
      std::cout << "indexInInsidee " << indexInInside_ << "  lvl " << int(info_.level()) << std::endl;
      std::cout << "indexInOutside " << indexInOutside_ << "  lvl " << int(outsideInfo_.level()) << std::endl;
      std::cout << "Intersection: " << geometry().corner(0) << " " << geometry().corner(1) << std::endl;
      */
    }

    bool equals(const P4estGridIntersection& other ) const
    {
      return (info_.equals( other.info_ )) && (outsideInfo_.equals( other.outsideInfo_ ));
    }

    const Entity inside () const
    {
      return Entity( EntityImpl( data(), info_ ));
    }

    Entity outside () const
    {
      return Entity( EntityImpl( data(), outsideInfo_ ));
    }

    bool boundary () const
    {
      // this is p4est convention: On the domain boundary we get inside as
      // neighbor from  mesh_face_neighbor_next
      return (outsideInfo_.equals( info_ )) && (indexInInside_ == indexInOutside_);
    }

    bool conforming () const
    {
      return info_.level() == outsideInfo_.level() ;
    }

    bool neighbor () const
    {
      return ! boundary() ;
    }

    int boundaryId () const
    {
      return ( boundary() ? indexInInside_+1 : 0 );
    }

    size_t boundarySegmentIndex () const
    {
      return 1 ; //hostIntersection().boundarySegmentIndex();
    }

    const LocalGeometry &geometryInInside () const
    {
      if( ! geoInsideImpl().valid() )
      {
        geoInsideImpl().build( inside().geometry(), geometry() );
      }
      return geoInInside_ ;
    }

    const LocalGeometry &geometryInOutside () const
    {
      if( ! geoOutsideImpl().valid() )
      {
        assert( neighbor() );
        geoOutsideImpl().build( outside().geometry(), geometry() );
      }
      return geoInOutside_ ;
    }

    const Geometry &geometry () const
    {
      if( ! geoImpl().valid() )
      {
        const ReferenceElementType& refElement = geoInside_.referenceElement();
        if( info_.level() >= outsideInfo_.level() )
        {
          static const int nCorners = GeometryImpl :: corners_;
          std::array< int, nCorners > vx;

          for( int i=0; i<nCorners; ++i )
          {
           vx[ i ] = refElement.subEntity( indexInInside_, 1, i, dimension );
          }
          geoImpl().build( geoInside_, vx );

          //geoImpl().build( data_, EntityInfo( info_, 1, indexInInside_), refElement );
        }
        else
          geoImpl().build( data_, EntityInfo( outsideInfo_, 1, indexInOutside_), refElement );
      }
      return geo_ ;
    }

    GeometryType type () const
    {
      return GeometryTypes::cube(mydimension);
    }

    int indexInInside () const
    {
      return indexInInside_ ;
    }

    int indexInOutside () const
    {
      return indexInOutside_  ;
    }

    NormalVector
    integrationOuterNormal ( const FieldVector< ctype, dimension-1 > &local ) const
    {
      if constexpr ( dimension == 3 )
      {
#if 0
        // if mapping calculated and affine, nothing more to do
        if ( mappingGlobal_.affine () && mappingGlobalUp2Date_ )
          return outerNormal_ ;

        // update surface mapping
        if( ! mappingGlobalUp2Date_ )
        {
          // update mapping to actual face
          // flip point 2 and 3 because of ALU refelement
          mappingGlobal_.buildMapping( geometry().corner( 0 ),
              geometry().corner( 1 ), geometry().corner( 3 ), geometry().corner( 2 ) );
          mappingGlobalUp2Date_ = true;
        }

        // calculate new normal
        mappingGlobal_.normal(local, outerNormal_ );

        return outerNormal_;
#else
        ///////////////////////////////////////
        //
        //  replace this code by use of bilinear face mapping
        //
        ///////////////////////////////////////

        const ReferenceElementType& refElement = data()->referenceElement();

        typedef typename Entity :: Geometry ElementGeometry ;
        typedef typename ElementGeometry :: Jacobian JacobianInverseTransposed;
        typedef typename ElementGeometry :: LocalCoordinate  LocalCoordinate ;

        LocalCoordinate insideLocal = geometryInInside().global( local );

        if( ! geoInside_.valid() )
          geoInside_.build( data_, info_, refElement );

        const JacobianInverseTransposed& jit = geoInside_.jacobianInverseTransposed( insideLocal );
        const LocalCoordinate &refNormal = refElement.integrationOuterNormal( indexInInside_ );

        NormalVector normal;
        jit.mv( refNormal, normal );
        normal *= geoInside_.integrationElement( insideLocal );
        // apply factor in non-conforming situation
        if( info_.level() < outsideInfo_.level() )
          normal *= 1.0/( 1 << mydimension ); // multiply with 0.5 or 0.25
        //normal *= ctype( 1 ) / jit.det(); // only for a generic geometry (impl)
        return normal;
#endif
#if 1
      }
      else // 2d
      {
        const auto& geom = geometry();
        const auto p0 = geom.corner( 0 );
        const auto p1 = geom.corner( 1 );

        NormalVector normal;
        const double factor = indexInInside_ % 2 == 0 ? -1.0 : 1.0;
        // we want the length of the intersection and orthogonal to it
        normal[0] = factor * (p1[1] - p0[1]);
        normal[1] = factor * (p1[0] - p0[0]);

        //if( info_.level() < outsideInfo_.level() )
        //  normal *= 1.0/( 1 << mydimension ); // multiply with 0.5 or 0.25
        return normal;
      }
#endif
    }

    NormalVector
    outerNormal ( const FieldVector< ctype, dimension-1 > &local ) const
    {
      return integrationOuterNormal( local ) ;
    }

    NormalVector
    unitOuterNormal ( const FieldVector< ctype, dimension-1 > &local ) const
    {
      NormalVector outerNormal ( integrationOuterNormal( local ) );
      outerNormal *= 1.0/outerNormal.two_norm();
      return outerNormal ;
    }

    NormalVector centerUnitOuterNormal () const
    {
      return unitOuterNormal( FieldVector< ctype, dimension-1 >( 0.5 ) );
    }

    const Grid &grid () const
    {
      return *data();
    }

    ExtraData data () const
    {
      assert( data_ );
      return data_ ;
    }

  private:
    GeometryImpl& geoImpl() const { return geo_.impl(); }

    GeometryImpl& geoInsideImpl() const { return geoInInside_.impl(); }
    GeometryImpl& geoOutsideImpl() const { return geoInOutside_.impl(); }

    ExtraData data_;
    EntityInfo info_;
    EntityInfo outsideInfo_ ;
    mutable Geometry geo_;
    mutable LocalGeometry geoInInside_;
    mutable LocalGeometry geoInOutside_;
    mutable ElementGeometryImpl geoInside_;
    //mutable SurfaceNormalCalculator  mappingGlobal_;
    //mutable NormalVector outerNormal_;

    mesh_face_neighbor_t faceNeighbor_;

    int indexInInside_ ;     // inside face index
    int indexInOutside_ ;    // outside face index

    int insideLevel_, outsideLevel_;
    //mutable bool mappingGlobalUp2Date_;
  public:
    // used by SharedPointer
    void invalidate() { done(); }
    // refCount used by SharedPointer
    unsigned int refCount_;

  };


//! \brief Class that wraps IntersectionImp of a grid and gets it's
//! internal object from a object stack hold by the grid
template <class GridImp, class IntersectionImpl>
class P4estIntersectionWrapper
{
  enum { dim = GridImp :: dimension };
  enum { dimworld = GridImp :: dimensionworld };

  typedef P4estIntersectionWrapper<GridImp,IntersectionImpl> ThisType;

  typedef IntersectionImpl IntersectionImp;

  typedef detail::P4estSharedPointer< IntersectionImp > SharedPointerType;

public:
  //! dimension
  enum { dimension      = dim };
  //! dimensionworld
  enum { dimensionworld = dimworld };

  //! define type used for coordinates in grid module
  typedef typename GridImp :: ctype ctype;

  //! Entity type
  typedef typename GridImp::template Codim<0>::Entity Entity;

  //! type of intersectionGlobal
  typedef typename GridImp::template Codim<1>::Geometry Geometry;
  //! type of intersection*Local
  typedef typename GridImp::template Codim<1>::LocalGeometry LocalGeometry;

  //! type of normal vector
  typedef FieldVector<ctype , dimworld> NormalType;

  typedef typename IntersectionImpl::Twists Twists;
  typedef typename Twists::Twist Twist;

  P4estIntersectionWrapper() : itPtr_() { it().done(); }

  //! constructor called from the ibegin and iend method
  template <class ExtraData, class EntityImp>
  P4estIntersectionWrapper(ExtraData data, const EntityImp & en)
    : itPtr_()
  {
    it().first( data, en );
  }

  operator bool () const { return bool( itPtr_ ); }

  //! the equality method
  bool equals ( const ThisType &other ) const
  {
    return (itPtr_ && other.itPtr_ ) ? it().equals( other.it() ) : itPtr_ == other.itPtr_;
  }

  //! increment iterator
  void increment ()
  {
    // if the shared pointer is unique we can increment
    if( itPtr_.unique() )
    {
      it().increment();
    }
    else
    {
      // otherwise make a copy and assign the same intersection
      // and then increment
      SharedPointerType copy( itPtr_ );
      itPtr_.invalidate();
      it().assign( *copy );
      it().increment();
    }
  }

  //! access neighbor
  Entity outside() const { return it().outside(); }

  //! access entity where iteration started
  Entity inside() const { return it().inside(); }

  //! return true if intersection is with boundary. \todo connection with
  //! boundary information, processor/outer boundary
  bool boundary () const { return it().boundary(); }

  //! return true if across the intersection a neighbor on this level exists
  bool neighbor () const { return it().neighbor(); }

  //! return information about the Boundary
  int boundaryId () const { return it().boundaryId(); }

  //! return the boundary segment index
  size_t boundarySegmentIndex() const { return it().boundarySegmentIndex(); }

  //! return the segment index (non-consecutive)
  int segmentId() const { return it().segmentId(); }


  //! intersection of codimension 1 of this neighbor with element where
  //! iteration started.
  //! Here returned element is in LOCAL coordinates of the element
  //! where iteration started.
  LocalGeometry geometryInInside () const
  {
    return it().geometryInInside();
  }

  //! intersection of codimension 1 of this neighbor with element where
  //!  iteration started.
  //! Here returned element is in GLOBAL coordinates of the element where
  //! iteration started.
  Geometry geometry () const
  {
    return it().geometry();
  }

  /** \brief obtain the type of reference element for this intersection */
  GeometryType type () const
  {
    return it().type();
  }

  //! local index of codim 1 entity in self where intersection is contained
  //!  in
  int indexInInside () const
  {
    return it().indexInInside();
  }

  //! intersection of codimension 1 of this neighbor with element where
  //! iteration started.
  //! Here returned element is in LOCAL coordinates of neighbor
  LocalGeometry geometryInOutside () const
  {
    return it().geometryInOutside();
  }

  //! local index of codim 1 entity in neighbor where intersection is
  //! contained
  int indexInOutside () const
  {
    return it().indexInOutside();
  }

  //! return unit outer normal, this should be dependent on local
  //! coordinates for higher order boundary
  const NormalType unitOuterNormal ( const FieldVector< ctype, dim-1 > &local ) const
  {
    return it().unitOuterNormal( local );
  }

  //! return unit outer normal, this should be dependent on local
  //! coordinates for higher order boundary
  const NormalType centerUnitOuterNormal ( ) const
  {
    const auto& refElement = GridImp::faceReferenceElement();
    assert( refElement.type() == type() );
    return unitOuterNormal(refElement.position(0,0));
  }

  //! return outer normal, this should be dependent on local
  //! coordinates for higher order boundary
  const NormalType outerNormal ( const FieldVector< ctype, dim-1 > &local ) const
  {
    return it().outerNormal( local );
  }

  //! return outer normal, this should be dependent on local
  //! coordinates for higher order boundary
  const NormalType integrationOuterNormal ( const FieldVector< ctype, dim-1 > &local ) const
  {
    return it().integrationOuterNormal( local );
  }

  //! return level of iterator
  int level () const { return it().level(); }

  //! return true if intersection is conform (i.e. only one neighbor)
  bool conforming () const { return it().conforming(); }

  //! returns reference to underlying intersection iterator implementation
  IntersectionImp & it() { return *itPtr_; }
  const IntersectionImp & it() const { return *itPtr_; }

protected:
  mutable SharedPointerType itPtr_;
}; // end class P4estIntersectionWrapper

template< class Grid >
using P4estGridLeafIntersection = P4estIntersectionWrapper< Grid, P4estGridIntersection< Grid > >;

template< class Grid >
using P4estGridLevelIntersection = P4estIntersectionWrapper< Grid, P4estGridIntersection< Grid > >;

}

#endif // #ifndef DUNE_P4ESTGRID_INTERSECTION_HH
