/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_GRID_HH
#define DUNE_P4ESTGRID_GRID_HH

#include <string>

#include <dune/common/visibility.hh>
#include <dune/geometry/referenceelements.hh>

#include <dune/grid/common/grid.hh>
#include <dune/grid/common/sizecache.hh>
#include <dune/grid/common/adaptcallback.hh>

#include <dune/p4estgrid/p4est.hh>
#include <dune/p4estgrid/indices.hh>

#include <dune/p4estgrid/declaration.hh>
#include <dune/p4estgrid/capabilities.hh>
#include <dune/p4estgrid/domaingeometry.hh>
#include <dune/p4estgrid/entity.hh>
#include <dune/p4estgrid/entitypointer.hh>
#include <dune/p4estgrid/entityseed.hh>
#include <dune/p4estgrid/geometry.hh>
#include <dune/p4estgrid/hierarchiciterator.hh>
#include <dune/p4estgrid/intersection.hh>
#include <dune/p4estgrid/intersectioniterator.hh>
#include <dune/p4estgrid/iterator.hh>
#include <dune/p4estgrid/idset.hh>
#include <dune/p4estgrid/indexsets.hh>
#include <dune/p4estgrid/indexstack.hh>
#include <dune/p4estgrid/gridview.hh>
#include <dune/p4estgrid/backuprestore.hh>
#include <dune/p4estgrid/entityinfo.hh>
#include <dune/p4estgrid/streambuffer.hh>

#include <dune/grid/geometrygrid/identity.hh>

namespace Dune
{

  // P4estGridFamily
  // -------------------

  template< int dim, int dimworld, P4estType elType, class ct >
  struct P4estGridFamily
  {
    struct Traits
    {
      typedef P4estGrid< dim, dimworld, elType, ct > Grid;

      typedef ct ctype;

      // type of data passed to entities, intersections, and iterators
      typedef const Grid* ExtraData;

      static const bool enableHierarchy = false ;
      static const bool cartesian = (elType == P4estType::cartesian);
      static const P4estType elementType = elType;

      static const int dimension      = dim ;
      static const int dimensionworld = dimworld ;

      typedef P4estEntityInfo< dimension > EntityInfo;

      // type of container for reference elements
      typedef ReferenceElements< ctype, dimension > ReferenceElementContainerType;

      // type of container for reference faces
      typedef ReferenceElements< ctype, dimension-1 > ReferenceFaceContainerType;

      // type of reference element
      typedef std::decay_t< decltype( ReferenceElementContainerType::general( std::declval< const Dune::GeometryType & >() ) ) > ReferenceElementType;

      // type of reference face
      typedef std::decay_t< decltype( ReferenceFaceContainerType::general( std::declval< const Dune::GeometryType & >() ) ) > ReferenceFaceType;

      //typedef SPReferenceCube< ctype, dimension > ReferenceCube;

      typedef P4estGridLeafIntersection< const Grid >          IntersectionImpl;
      typedef P4estGridLeafIntersectionIterator< const Grid >  IntersectionIteratorImpl;

      typedef Dune::Intersection< const Grid, IntersectionImpl > LeafIntersection;
      //typedef Dune::Intersection< const Grid, LevelIntersectionWrapper< const Grid > > LevelIntersection;


      typedef Dune::IntersectionIterator< const Grid, IntersectionIteratorImpl, IntersectionImpl > IntersectionIterator;

      typedef IntersectionIterator LeafIntersectionIterator;
      typedef IntersectionIterator LevelIntersectionIterator;

      //typedef Dune::Intersection< const Grid, P4estGridLeafIntersection< const Grid > > LeafIntersection;
      typedef LeafIntersection  LevelIntersection;

      //typedef Dune::Intersection< const Grid, P4estGridLevelIntersection< const Grid > > LevelIntersection;

      //typedef Dune::IntersectionIterator< const Grid, P4estGridLeafIntersectionIterator< const Grid >, P4estGridLeafIntersection< const Grid > >
      //   LeafIntersectionIterator;

      //typedef Dune::IntersectionIterator< const Grid, P4estGridLevelIntersectionIterator< const Grid >, P4estGridLevelIntersection< const Grid > >
      //  LevelIntersectionIterator;

      typedef Dune::EntityIterator< 0, const Grid, P4estGridHierarchicIterator< const Grid > >
        HierarchicIterator;

      template< int codim >
      struct Codim
      {
        typedef P4estGridGeometry< dimension-codim, dimensionworld, const Grid >  GeometryImpl;
        typedef P4estGridGeometry< dimension-codim, dimension, const Grid >  LocalGeometryImpl;

        typedef Dune::Geometry< dimension-codim, dimensionworld, const Grid, P4estGridGeometry > Geometry;
        typedef Dune::Geometry< dimension-codim, dimension, const Grid, P4estGridGeometry > LocalGeometry;

        typedef P4estGridEntityPointerTraits< codim, const Grid > EntityPointerTraits;
        typedef P4estGridEntityPointer< EntityPointerTraits > EntityPointerImpl;

        typedef P4estGridEntity< codim, dimension, const Grid> EntityImpl;
        typedef typename EntityPointerTraits::Entity Entity;
        typedef P4estGridEntitySeed< codim, const Grid > EntitySeed;

        template< PartitionIteratorType pitype >
        struct Partition
        {
          typedef P4estGridLeafIteratorTraits< codim, pitype, const Grid > LeafIteratorTraits;
          typedef Dune::EntityIterator< codim, const Grid, P4estGridIterator< LeafIteratorTraits > >
            LeafIterator;

          //typedef P4estGridLevelIteratorTraits< codim, pitype, const Grid > LevelIteratorTraits;
          //typedef Dune::EntityIterator< codim, const Grid, P4estGridLevelIterator< LevelIteratorTraits > >
          //  LevelIterator;
          typedef LeafIterator LevelIterator;
        };

        typedef typename Partition< All_Partition >::LeafIterator LeafIterator;
        typedef typename Partition< All_Partition >::LevelIterator LevelIterator;
      };

      //! Type of the level index set
      //typedef DefaultIndexSet< Grid, typename Codim< 0 > :: LevelIterator > LevelIndexSetImp;
      //typedef IndexSet< Grid, LevelIndexSetImp > LevelIndexSet;

      //! Type of the level index set
      //typedef DefaultIndexSet< Grid, typename Codim< 0 > :: LeafIterator > LeafIndexSetImp;
      //typedef IndexSet< Grid, LeafIndexSetImp > LeafIndexSet;
      typedef P4estGridIndexSet< const Grid, 1 > LeafIndexSet;
      typedef LeafIndexSet  LeafIndexSetImp;

      typedef LeafIndexSet   LevelIndexSet;
      typedef LevelIndexSet  LevelIndexSetImp;

      typedef P4estGridIndexSet< const Grid, 2 > HierarchicIndexSet;

      typedef P4estGridIdSet< const Grid > GlobalIdSet;
      typedef P4estGridIdSet< const Grid > LocalIdSet;

#if HAVE_MPI
      typedef MPI_Comm  CommImplType;
#else
      typedef No_Comm   CommImplType;
#endif
      typedef CommImplType MPICommunicatorType;

      typedef Dune::Communication< CommImplType > Communication;
      typedef Communication CollectiveCommunication ;

      template< PartitionIteratorType pitype >
      struct Partition
      {
        //typedef Dune::GridView< P4estLevelGridViewTraits< const Grid, pitype > > LevelGridView;
        typedef Dune::GridView< P4estLeafGridViewTraits< const Grid, pitype > >  LeafGridView;
        typedef LeafGridView LevelGridView;
      };

      //typedef Dune::GridView< P4estLevelGridViewTraits< const Grid, All_Partition > > LevelGridView;
      typedef Dune::GridView< P4estLeafGridViewTraits< const Grid, All_Partition > >  LeafGridView;
      typedef LeafGridView LevelGridView;
    };
  };

  struct P4estGridObjectStream
  {
    typedef detail::SimpleStreamBuffer    ObjectStreamType;
    typedef ObjectStreamType              SmallObjectStreamType;
    typedef ObjectStreamType              InStreamType;
    typedef ObjectStreamType              OutStreamType;
  };

  // P4estGrid
  // -------------

  /** \class P4estGrid
   *  \brief identical grid wrapper
   *  \ingroup P4estGrid
   *
   *  \nosubgrouping
   */
  template< int dim, int dimworld, P4estType elType, class ct >
  class P4estGrid
  /** \cond */
  : public GridDefaultImplementation
      < dim, dimworld, ct, P4estGridFamily< dim, dimworld, elType, ct > >,
    public P4estGridObjectStream
  /** \endcond */
  {
    typedef P4estGrid< dim, dimworld, elType, ct > Grid;


  public:
    /** \cond */
    typedef P4estGridFamily< dim, dimworld, elType, ct > GridFamily;
    /** \endcond */

    static const bool enableHierarchy  = GridFamily::Traits::enableHierarchy;
    static const bool cartesian        = GridFamily::Traits::cartesian;
    static const P4estType elementType = GridFamily::Traits::elementType;

  private:
    typedef GridDefaultImplementation
      < dim, dimworld, ct, GridFamily >  Base;

    template< int, int, class > friend class P4estGridEntity;
    template< class > friend class P4estGridEntityPointer;
    template< class > friend class P4estGridIntersection;
    template< class > friend class P4estGridIntersectionIterator;
    template< class > friend class P4estGridIdSet;
    template< class , int > friend class P4estGridIndexSet;
    template< class > friend class HostGridAccess;
    template< class > friend class DGFGridFactory;

    template< class, class > friend class P4estGridDataHandle;

    template< int, PartitionIteratorType, class > friend class P4estGridLevelIteratorTraits;
    template< int, PartitionIteratorType, class > friend class P4estGridLeafIteratorTraits;

    typedef Dune :: P4est< dim, dimworld > PForest ;

    typedef typename PForest :: forest_t        forest_t ;
    typedef typename PForest :: connectivity_t  connectivity_t ;
    typedef typename PForest :: mesh_t          mesh_t ;
    typedef typename PForest :: ghost_t         ghost_t ;
    typedef typename PForest :: numbers_t       numbers_t ;
    typedef typename PForest :: quadrant_t      quadrant_t;

    typedef typename detail::IndexManagerType       IndexManagerType;

  public:
    typedef P4estGridIntersection<const Grid>
      IntersectionIteratorImp;
    typedef IntersectionIteratorImp  LeafIntersectionIteratorImp;
    typedef IntersectionIteratorImp  LevelIntersectionIteratorImp;

    /** \name Traits
     *  \{ */

    //! type of the grid traits
    typedef typename GridFamily::Traits Traits;

    /** \brief traits structure containing types for a codimension
     *
     *  \tparam codim  codimension
     *
     *  \nosubgrouping
     */
    template< int codim >
    struct Codim;

    /** \} */

    /** \name Iterator Types
     *  \{ */

    //! iterator over the grid hierarchy
    typedef typename Traits::HierarchicIterator HierarchicIterator;
    //! iterator over intersections with other entities on the leaf level
    typedef typename Traits::LeafIntersectionIterator LeafIntersectionIterator;
    //! iterator over intersections with other entities on the same level
    typedef typename Traits::LevelIntersectionIterator LevelIntersectionIterator;

    /** \} */

    /** \name Index and Id Set Types
     *  \{ */

    /** \brief type of leaf index set
     *
     *  The index set assigns consecutive indices to the entities of the
     *  leaf grid. The indices are of integral type and can be used to access
     *  arrays.
     *
     *  The leaf index set is a model of Dune::IndexSet.
     */
    typedef typename Traits::LeafIndexSet     LeafIndexSet;
    typedef typename Traits::LeafIndexSetImp  LeafIndexSetImp;

    /** \brief type of level index set
     *
     *  The index set assigns consecutive indices to the entities of a grid
     *  level. The indices are of integral type and can be used to access
     *  arrays.
     *
     *  The level index set is a model of Dune::IndexSet.
     */
    typedef typename Traits::LevelIndexSet      LevelIndexSet;
    typedef typename Traits::LevelIndexSetImp   LevelIndexSetImp;
    typedef typename Traits::HierarchicIndexSet HierarchicIndexSet;

    /** \brief type of global id set
     *
     *  The id set assigns a unique identifier to each entity within the
     *  grid. This identifier is unique over all processes sharing this grid.
     *
     *  \note Id's are neither consecutive nor necessarily of an integral
     *        type.
     *
     *  The global id set is a model of Dune::IdSet.
     */
    typedef typename Traits::GlobalIdSet GlobalIdSet;

    /** \brief type of local id set
     *
     *  The id set assigns a unique identifier to each entity within the
     *  grid. This identifier needs only to be unique over this process.
     *
     *  Though the local id set may be identical to the global id set, it is
     *  often implemented more efficiently.
     *
     *  \note Ids are neither consecutive nor necessarily of an integral
     *        type.
     *  \note Local ids need not be compatible with global ids. Also, no
     *        mapping from local ids to global ones needs to exist.
     *
     *  The global id set is a model of Dune::IdSet.
     */
    typedef typename Traits::LocalIdSet LocalIdSet;

    /** \brief Types for GridView */
    template <PartitionIteratorType pitype>
    struct Partition
    {
      typedef typename GridFamily::Traits::template Partition<pitype>::LevelGridView
         LevelGridView;
      typedef typename GridFamily::Traits::template Partition<pitype>::LeafGridView
         LeafGridView;
    };
    /** \brief View types for All_Partition */
    typedef typename Partition< All_Partition > :: LevelGridView LevelGridView;
    typedef typename Partition< All_Partition > :: LeafGridView LeafGridView;


    /** \} */

    /** \name Miscellaneous Types
     * \{ */

    //! type of vector coordinates (e.g., double)
    typedef typename Traits::ctype ctype;

    static const int dimension = Traits::dimension;
    static const int dimensionworld = Traits::dimensionworld;

    //! communicator with all other processes having some part of the grid
    typedef typename Traits::Communication        Communication;
    typedef typename Traits::CommImplType         CommImplType;
    typedef typename Traits::MPICommunicatorType  MPICommunicatorType;

    class P4estCommunicationFutureIF
    {
      protected:
        P4estCommunicationFutureIF() {}
      public:
        virtual ~P4estCommunicationFutureIF() {}
        virtual bool ready() const = 0;
        virtual void wait () = 0;
    };

    class P4estCommunicationFuture
    {
    protected:
      std::shared_ptr< P4estCommunicationFutureIF > commFuture_;
    public:
      P4estCommunicationFuture( const std::shared_ptr< P4estCommunicationFutureIF >& future )
        : commFuture_( future )
      {}

      P4estCommunicationFuture() = default;
      P4estCommunicationFuture( const P4estCommunicationFuture& other ) = default;

      ~P4estCommunicationFuture() { wait(); }

      bool ready() const { return ( commFuture_ ) ? commFuture_->ready() : true; }

      void wait ()
      {
        if( commFuture_ )
          commFuture_->wait();
      }

      [[deprecated]]
      bool pending () const { return ! ready(); }
    };

    typedef P4estCommunicationFuture LeafCommunication ;
    typedef P4estCommunicationFuture LevelCommunication ;

    typedef typename Traits:: ReferenceElementType  ReferenceElementType ;
    typedef typename Traits:: ReferenceFaceType     ReferenceFaceType ;

    /*
    typedef typename Traits::ReferenceCube ReferenceCube;
    typedef SPGeometricGridLevel< const Grid > GridLevel;
    */
    typedef typename Codim< 0 >::LocalGeometry  LocalGeometry;

    // the type of our size cache
    typedef SizeCache< Grid > SizeCacheType;

    typedef P4estIndex< Traits > P4estIndexType ;
    typedef P4estEntityInfo< dimension > EntityInfo;

    typedef std::vector< EntityInfo > ElementVectorType;

    /** \} */

    static const int MAXL = PForest :: Q_MAXLEVEL;
    static const int numChildren = PForest :: NUMCHILDREN;

    //typedef P4estDomainGeometry< ctype, dimension, dimensionworld > DomainGeometryType;

    /** \brief Copy construction is disabled */
    P4estGrid ( const P4estGrid& other ) = delete ;

    /** \name Construction and Destruction
     *  \{ */

    /** \brief constructor
     *
     *  The pointer to connectivity is taken over and will be destroyed upon grid destruction.
     *
     *  \param[in]  connectivity  p4est connectivity describing the macro grid
     *
     */
    explicit P4estGrid (
            // const std::shared_ptr< const DomainGeometryType >& domain,
            connectivity_t* connectivity = PForest :: connectivity_new_unitcube () )
    : comm_( initMPIComm( MPIHelper :: getCommunicator() ) ),
      //domain_( domain ),
      //gridWidth_( cartesian ? domain_->h() : GlobalCoordinate(0) ),
      gridWidth_( GlobalCoordinate(0) ),
      referenceElement_( Dune::ReferenceElements< ctype, dimension >::cube() ),
      referenceFace_( Dune::ReferenceElements< ctype, dimension-1 >::cube() ),
      connectivity_( connectivity ),
      forest_( PForest :: forest_new( comm_,
                                      connectivity_, this) ),
      ghost_( PForest :: ghost_new( forest_ ) ),
      //numbers_( PForest :: numbers_new( forest_, ghost_ ) ),
      //numbers_( PForest :: numbers_new( forest_, nullptr ) ),
      mesh_( PForest :: mesh_new( forest_, ghost_ ) ),
      maxLevel_( 0 ),
      indices_( extraData() ),
      geomTypes_( dimension + 1 ),
      sizeCache_( *this ),
      levelIndexVec_( MAXL, nullptr ),
      leafIndexSet_(),
      hIndexSet_( *this ),
      globalIdSet_( 0 ),
      localIdSet_( 0 ),
      indexManager_( dimension+1 ),
      markerCounter_( 3, int(0) )
    {
      for( int codim = 0; codim <= dimension; ++codim )
      {
        geomTypes_[ codim ].push_back( GeometryTypes::cube(dimension - codim ) );
      }

      buildGeometryInFather();

      // create hIndex
      init();

      update();

      // clear all markers
      postAdapt();
    }

    /** \brief destructor
     */
    ~P4estGrid ()
    {
      PForest :: mesh_destroy( mesh_ );
      //PForest :: numbers_destroy( numbers_ );
      PForest :: ghost_destroy( ghost_ );
      PForest :: forest_destroy ( forest_ );
      PForest :: connectivity_destroy ( connectivity_ );

      for( int i=0; i<numChildren; ++i )
      {
        delete geometryInFather_[ i ];
        geometryInFather_[ i ] = 0;
      }
#if HAVE_MPI
      MPI_Comm_free ( &commImpl_ );
#endif
    }

    /** \} */

    /** \name Grid Identification Methods
     *  \{ */

    /** \brief obtain a string naming the grid
     *
     *  \returns ''P4estGrid\< \em host \em grid \em name \>''
     */
    static std::string name ()
    {
      return std::string( "P4estGrid" );
    }

    /** \} */


    /** \name Size Methods
     *  \{ */

    /** \brief obtain maximal grid level
     *
     *  Grid levels are numbered 0, ..., L, where L is the value returned by
     *  this method.
     *
     *  \returns maximal grid level
     */
    int maxLevel () const
    {
      return maxLevel_ ;
    }

    /** \brief obtain number of entites on a level
     *
     *  \param[in]  level  level to consider
     *  \param[in]  codim  codimension to consider
     *
     *  \returns number of entities of codimension \em codim on grid level
     *           \em level.
     */
    int size ( int level, int codim ) const
    {
      return sizeCache_.size( level, codim );
    }

    /** \brief obtain number of leaf entities
     *
     *  \param[in]  codim  codimension to consider
     *
     *  \returns number of leaf entities of codimension \em codim
     */
    int size ( int codim ) const
    {
      return sizeCache_.size( codim );
    }

    /** \brief obtain number of entites on a level
     *
     *  \param[in]  level  level to consider
     *  \param[in]  type   geometry type to consider
     *
     *  \returns number of entities with a geometry of type \em type on grid
     *           level \em level.
     */
    int size ( int level, GeometryType type ) const
    {
      return sizeCache_.size( level, type );
    }

    /** \brief returns the number of boundary segments within the macro grid
     *
     *  \returns number of boundary segments within the macro grid
     */
    int size ( GeometryType type ) const
    {
      return sizeCache_.size( type );
    }

    /** \brief obtain number of leaf entities
     *
     *  \param[in]  type   geometry type to consider
     *
     *  \returns number of leaf entities with a geometry of type \em type
     */
    size_t numBoundarySegments () const
    {
      DUNE_THROW(NotImplemented,"P4estGrid::numBoundarySegments: this is not implemented!");
      return 0 ;
    }
    /** \} */

    template< int codim >
    typename Codim< codim >::LevelIterator lbegin ( int level ) const
    {
      if ( codim == 0 )
      {
        //typedef P4estGridLevelIteratorTraits< codim, All_Partition, const Grid > T;
        //typedef P4estGridLevelIterator< T > Impl;
        //return Impl( extraData(), level, 0, elements_.size() );
        return this->leafbegin< codim >();
      }
      else
        return this->lend< codim > (level);
    }

    template< int codim >
    typename Codim< codim >::LevelIterator lend ( int level ) const
    {
      /*
      typedef P4estGridLevelIteratorTraits< codim, All_Partition, const Grid > T;
      typedef P4estGridLevelIterator< T > Impl;
      return Impl( extraData() );
      */
      return this->leafend< codim >();
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LevelIterator
    lbegin ( int level ) const
    {
      if ( codim == 0 )
      {
        /*
        typedef P4estGridLevelIteratorTraits< codim, pitype, const Grid > T;
        typedef P4estGridLevelIterator< T > Impl;
        const size_t start = (pitype == Ghost_Partition) ? ghostNumQuadrants() : 0 ;
        const size_t end   = (pitype <  All_Partition) ? localNumQuadrants() : elements_.size() ;
        return Impl( extraData(), level, start, end );
        */
        return this->leafbegin< codim, pitype > ();
      }
      else
        return this->lend< codim, pitype > (level);
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LevelIterator
    lend ( int level ) const
    {
      /*
      typedef P4estGridLevelIteratorTraits< codim, pitype, const Grid > T;
      typedef P4estGridLevelIterator< T > Impl;
      return Impl( extraData() );
      */
      return this->leafend< codim, pitype >();
    }

    template< int codim >
    typename Codim< codim >::LeafIterator leafbegin () const
    {
      typedef P4estGridLeafIteratorTraits< codim, All_Partition, const Grid > T;
      typedef P4estGridIterator< T > Impl;
      if ( codim == 0 )
        return Impl( extraData(), 0, elements_.size() );
      else
        return this->leafend< codim > ();
    }

    template< int codim >
    typename Codim< codim >::LeafIterator leafend () const
    {
      typedef P4estGridLeafIteratorTraits< codim, All_Partition, const Grid > T;
      typedef P4estGridIterator< T > Impl;
      return Impl( extraData() );
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LeafIterator
    leafbegin () const
    {
      typedef P4estGridLeafIteratorTraits< codim, pitype, const Grid > T;
      typedef P4estGridIterator< T > Impl;
      if ( codim == 0 )
      {
        const size_t start = (pitype == Ghost_Partition) ? ghostNumQuadrants() : 0 ;
        const size_t end   = (pitype <  All_Partition) ? localNumQuadrants() : elements_.size() ;
        // start at first ghost, or at 0
        // end after interior or at the end
        return Impl( extraData(), start, end );
      }
      else
        return this->leafend< codim, pitype > ();
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LeafIterator
    leafend () const
    {
      typedef P4estGridLeafIteratorTraits< codim, pitype, const Grid > T;
      typedef P4estGridIterator< T > Impl;
      return Impl( extraData() );
    }

    //! TODO: Implement global id set
    const GlobalIdSet &globalIdSet () const
    {
      if( !globalIdSet_ )
        globalIdSet_ = new GlobalIdSet( *this );

      assert( globalIdSet_ );
      return *globalIdSet_;
    }

    const LocalIdSet &localIdSet () const
    {
      if( !localIdSet_ )
        localIdSet_ = new LocalIdSet( *this );
      assert( localIdSet_ );
      return *localIdSet_;
    }

    const LevelIndexSet &levelIndexSet ( int level ) const
    {
      if constexpr ( enableHierarchy )
      {
        //assert( levelIndexSets_.size() == (size_t)(maxLevel()+1) );
        if( (level < 0) || (level > maxLevel()) )
        {
          DUNE_THROW( GridError, "LevelIndexSet for nonexisting level " << level
                                 << " requested." );
        }

        if( ! levelIndexVec_[ level ] )
        {
          levelIndexVec_[ level ] = std::make_shared< LevelIndexSetImp > ( *this, lbegin< 0 >( level ), lend< 0 >( level ), level );
        }
        return (*levelIndexVec_[ level ]);
      }
      else
        return leafIndexSet();
    }

    const LeafIndexSet &leafIndexSet () const
    {
      if( ! leafIndexSet_ )
      {
        leafIndexSet_.reset(  new LeafIndexSetImp ( *this ) );
      }
      return *leafIndexSet_;
    }

    const HierarchicIndexSet &hierarchicIndexSet () const
    {
      return hIndexSet_;
    }

    const std::vector< GeometryType >& geomTypes ( int codim ) const
    {
      return geomTypes_[ codim ];
    }

    void globalRefine ( int refCount )
    {
      for( int i=0; i<refCount; ++i )
      {
        typedef typename Traits :: LeafGridView GridView;
        GridView view ( this->leafGridView() );
        auto end = view.template end<0, Interior_Partition>();
        //int count = 0;
        for( auto it = view.template begin<0, Interior_Partition>(); it != end; ++it)
        {
          this->mark( 1, *it );
        }

        // do adaptation cycle
        preAdapt();
        adapt();
        postAdapt();
      }
    }

    bool mark ( int refCount, const typename Codim< 0 >::Entity &entity )
    {
      assert( refCount >= -1 && refCount <= 1 );
      ++markerCounter_[ refCount+1 ];
      return entity.impl().mark( refCount );
    }

    int getMark ( const typename Codim< 0 >::Entity &entity ) const
    {
      return entity.impl().getMark();
    }

    bool preAdapt ()
    {
      return true; //markerCounter_[ 0 ] > 0 ;
    }

    struct DoNothingDataHandle : public CommDataHandleIF< DoNothingDataHandle, char >
    {
      typedef typename Codim< 0 >::Entity Entity;

      void preCoarsening ( const Entity &father ) {}
      void postRefinement ( const Entity &father ) {}
      void restrictLocal( const Entity &father, const Entity& son, bool initialize ) {}
      void prolongLocal( const Entity &father, const Entity& son, bool initialize ) {}
    };


    /** \brief standard adapt method, call preAdapt before and postAdapt after this
        method */
    //-  --adapt
    bool adapt ()
    {
      DoNothingDataHandle dh;
      return adapt( dh );
    }

    /** \brief  @copydoc Dune::Grid::adapt()
        \param handle handler for restriction and prolongation operations
        which is a Model of the AdaptDataHandleInterface class.
    */
    //template< class GridImp, class DataHandle >
    //bool adapt ( AdaptDataHandleInterface< GridImp, DataHandle > &datahandle )
    template< class DataHandle >
    bool adapt ( DataHandle &datahandle )
    {
      // check whether there has been marking for refinement
      const bool refine  = true; // markerCounter_[ 2 ] > 0;
      const bool coarsen = true; //preAdapt();

      // make copy of old forest
      forest_t* oldForest = forest_;
      forest_t* newForest = PForest :: forest_copy( forest_ );

      // adapt the forest
      PForest :: adapt( newForest, indices_, refine, coarsen );

      ElementVectorType oldElements;
      oldElements.swap( elements_ );

      typedef typename PForest :: tree_t   tree_t ;
      typedef typename PForest :: topidx_t topidx_t ;
      typedef typename PForest :: quadidx_t quadidx_t ;
      typedef typename PForest :: quadrant_t quadrant_t ;

      // reserve for new elements
      {
        elements_.clear();
        tree_t* tree = PForest :: tree_array_index (newForest->trees, forest_->last_local_tree );
        elements_.reserve( tree->quadrants.elem_count + tree->quadrants_offset );
      }

      // free all ghost indices
      freeGhostIndices( oldElements, oldForest->local_num_quadrants );

      //typedef typename PForest :: mesh_face_neighbor_t mesh_face_neighbor_t ;

      typedef typename Traits :: template Codim< 0 > :: Entity     Entity ;
      typedef typename Traits :: template Codim< 0 > :: EntityImpl EntityImpl;
      typedef P4estEntityInfo< dimension > EntityInfo;

      assert( oldForest->first_local_tree == newForest->first_local_tree );
      assert( oldForest->last_local_tree  == newForest->last_local_tree );

      PForest :: setNewElementMarker( oldForest, newForest );

      /// create entities
      EntityImpl dummy( extraData() );
      Entity father ( dummy );
      Entity son ( dummy );
      EntityImpl& fatherImpl = father.impl();
      EntityImpl& sonImpl    = son.impl();

      int newElements = 0;
      size_t overallElements = 0;
      for( topidx_t treeid = newForest->first_local_tree;
           treeid <= newForest->last_local_tree; ++ treeid )
      {
        tree_t* tree          = PForest :: tree_array_index (newForest->trees, treeid );
        tree_t* oldTree       = PForest :: tree_array_index (oldForest->trees, treeid );

        const quadidx_t elemCount = tree->quadrants.elem_count ;
        const quadidx_t oldElemCount = oldTree->quadrants.elem_count ;
        newElements += std::abs( elemCount - oldElemCount );
        overallElements += elemCount;
      }

      //std::cout << newElements << " new elem estimate " << std::endl;

      // reserve memory
      // datahandle.preAdapt( newElements );

      // do adaptation
      quadidx_t quadId = 0;
      quadidx_t oldQuadId = 0;
      for( topidx_t treeid = newForest->first_local_tree;
           treeid <= newForest->last_local_tree; ++ treeid )
      {
        //std::cout << "Start tree loop" << std::endl;
        tree_t* tree          = PForest :: tree_array_index (newForest->trees, treeid );
        tree_t* oldTree       = PForest :: tree_array_index (oldForest->trees, treeid );

        quadidx_t oldCount = 0 ;
        quadidx_t count = 0;

        /* index setup */
        const quadidx_t elemCount = tree->quadrants.elem_count ;
        const quadidx_t oldElemCount = oldTree->quadrants.elem_count ;
        while( count < elemCount && oldCount < oldElemCount )
        {
          quadrant_t* quad = PForest :: quadrant_array_index ( &tree->quadrants, count );
          quadrant_t* oldQuad = PForest :: quadrant_array_index ( &oldTree->quadrants, oldCount );

          if( quad->level == oldQuad->level ) // nothing happened
          {
            // copy hIndex for old to new element
            elements_.push_back( EntityInfo( quad, treeid, quadId, oldElements[ oldQuadId ].userIndex() ) );

            ++ count ;
            ++ oldCount ;

            ++quadId;
            ++oldQuadId;
          }
          else if( quad->level > oldQuad->level )
          {
            // obtain father quadrant from old list
            EntityInfo& fatherInfo = oldElements[ oldQuadId ];
            fatherInfo.setLeaf( false ); // no longer leaf element at this point
            // update entity object
            fatherImpl.update( fatherInfo );

            // obtain new hIndex
            elements_.push_back( EntityInfo( quad, treeid, quadId, getUserIndex() ) );

            // same for son entity
            sonImpl.update( elements_.back() );
            // prolong data
            datahandle.prolongLocal( father, son, true );
            // increase counters
            ++ count ;
            ++ quadId;

            // iterate over all other children and repeat the same process
            for( int i=1; i<numChildren; ++i )
            {
              // get quadrant from tree
              quad = PForest :: quadrant_array_index ( &tree->quadrants, count );

              // obtain new hIndex for entity info
              elements_.push_back( EntityInfo( quad, treeid, quadId, getUserIndex() ) );

              sonImpl.update( elements_.back() );
              datahandle.prolongLocal( father, son, false );
              ++ count ;
              ++ quadId;
            }

            // free father index for reuse
            freeUserIndex( fatherImpl.info() );
            ++ oldCount ; // also increase coarser element
            ++ oldQuadId;
          }
          // if old quadrant is finer the it was coarsened
          else if( oldQuad->level > quad->level )
          {
            // obtain new hIndex
            elements_.push_back( EntityInfo( quad, treeid, quadId,
                                 getUserIndex() ) );

            // no leaf element here
            elements_.back().setLeaf( false );

            // update father object
            fatherImpl.update( elements_.back() );

            // update son entity object
            sonImpl.update( oldElements[ oldQuadId ] );
            // call restrict on data object
            datahandle.restrictLocal( father, son, true );
            // increase old counter
            ++ oldCount ;
            ++ oldQuadId ;

            // free son's hIndex for later use
            freeUserIndex( sonImpl.info() );

            // repeat process for other children
            for( int i=1; i<numChildren; ++i )
            {
              oldQuad = PForest :: quadrant_array_index ( &oldTree->quadrants, oldCount );
              sonImpl.update( oldElements[ oldQuadId ] );
              // call restrict on data object
              datahandle.restrictLocal( father, son, false );
              ++ oldCount ;
              ++ oldQuadId;

              // free hIndex for later user
              freeUserIndex( sonImpl.info() );
            }
            ++ count ; // also increase coarsen element coutner
            ++ quadId;

            // element is now leaf element
            elements_.back().setLeaf( true );
          }
        }
      }

      //std::cout << "Finished adapt!" << std::endl;

      // delete old forest
      forest_ = newForest ;
      oldElements.clear();

      // update internal structures
      update();

#ifndef NDEBUG
      size_t i=0;
      for( const auto& item : elements_ )
      {
        assert( item.quadId() == i );
        ++ i;
      }
#endif

      PForest :: forest_destroy( oldForest );

      // call compress of dofmanager
      // datahandle.postAdapt();

      // clear all markers
      postAdapt();

      return refine ;
    }

    void postAdapt ()
    {
      PForest :: resetAdaptMarker( forest_ );
      std::fill( markerCounter_.begin(), markerCounter_.end(), 0 );
    }

    /** \name Parallel Data Distribution and Communication Methods
     *  \{ */

    /** \brief obtain size of overlap region for the leaf grid
     *
     *  \param[in]  codim  codimension for with the information is desired
     */
    int overlapSize ( int codim ) const
    {
      return 0 ; // hostGrid().overlapSize( codim );
    }

    /** \brief obtain size of ghost region for the leaf grid
     *
     *  \param[in]  codim  codimension for with the information is desired
     */
    int ghostSize( int codim ) const
    {
      return codim == 0 ? 1 : 0 ;
    }

    /** \brief obtain size of overlap region for a grid level
     *
     *  \param[in]  level  grid level (0, ..., maxLevel())
     *  \param[in]  codim  codimension (0, ..., dimension)
     */
    int overlapSize ( int level, int codim ) const
    {
      return overlapSize( codim );
    }

    /** \brief obtain size of ghost region for a grid level
     *
     *  \param[in]  level  grid level (0, ..., maxLevel())
     *  \param[in]  codim  codimension (0, ..., dimension)
     */
    int ghostSize ( int level, int codim ) const
    {
      return ghostSize( codim );
    }

    /** \brief communicate information on a grid level
     *
     *  \param      datahandle  communication data handle (user defined)
     *  \param[in]  interface   communication interface (one of
     *                          InteriorBorder_InteriorBorder_Interface,
     *                          InteriorBorder_All_Interface,
     *                          Overlap_OverlapFront_Interface,
     *                          Overlap_All_Interface,
     *                          All_All_Interface)
     *  \param[in]  direction   communication direction (one of
     *                          ForwardCommunication or BackwardCommunication)
     *  \param[in]  level       grid level to communicate
     */
    template< class DataHandle, class Data >
    LevelCommunication
    communicate ( CommDataHandleIF< DataHandle, Data > &datahandle,
                  InterfaceType interface,
                  CommunicationDirection direction,
                  int level ) const
    {
      if constexpr ( enableHierarchy )
      {
        DUNE_THROW(NotImplemented,"P4estGrid::communicate(level) not implemented yet!");
      }
      return LevelCommunication();
    }

    /** \brief communicate information on leaf entities
     *
     *  \param      datahandle  communication data handle (user defined)
     *  \param[in]  interface   communication interface (one of
     *                          InteriorBorder_InteriorBorder_Interface,
     *                          InteriorBorder_All_Interface,
     *                          Overlap_OverlapFront_Interface,
     *                          Overlap_All_Interface,
     *                          All_All_Interface)
     *  \param[in]  direction   communication direction (one of
     *                          ForwardCommunication, BackwardCommunication)
     */
    template< class DataHandle, class Data >
    LeafCommunication
    communicate ( CommDataHandleIF< DataHandle, Data > &datahandle,
                  const InterfaceType interface,
                  const CommunicationDirection direction ) const
    {
      if( comm().size() == 1 ) return LeafCommunication();

      switch( interface )
      {
        case InteriorBorder_All_Interface :
        case All_All_Interface : /// this is needed for dune-fem (not implemented correctly at the moment )
          return interiorGhostCommunication( datahandle, direction );
        default:
          DUNE_THROW(NotImplemented,"interface not implemented!" << interface);
      }
      return LeafCommunication();
    }

  protected:
    template <class DataHandle>
    class DUNE_PRIVATE P4estCommunicationFutureImpl : public P4estCommunicationFutureIF
    {
      typedef typename PForest :: ghost_exchange_t  ghost_exchange_t;
      typedef typename PForest :: quadidx_t quadidx_t ;
      typedef typename PForest :: quadrant_t quadrant_t ;

      typedef typename Traits :: template Codim< 0 > :: Entity  Entity ;
      typedef P4estGridEntity< 0, dimension, const Grid > EntityImpl;

      typedef typename detail:: SimpleStreamBuffer  SimpleStreamBuffer;

      typedef typename Traits :: ExtraData ExtraData;
      ExtraData grid_;
      DataHandle& dataHandle_;

      SimpleStreamBuffer mirrorData_;
      SimpleStreamBuffer readBuffer_;
      std::vector< void* > mirrorData2_;

      ghost_exchange_t* exc_;

      size_t entitySize_ ;
      bool ready_;

      P4estCommunicationFutureImpl( const P4estCommunicationFutureImpl& ) = delete;

    public:
      P4estCommunicationFutureImpl( ExtraData grid, DataHandle& dataHandle )
        : grid_( grid ), dataHandle_( dataHandle ),
          mirrorData_(), readBuffer_(), mirrorData2_(),
          exc_( nullptr ),
          entitySize_( 0 ),
          ready_( false )
      {
        packAndSend();
      }

      ~P4estCommunicationFutureImpl()
      {
        wait();
      }

      bool ready() const { return ready_; }
      void wait () { waitAndUnpack(); }

    protected:
      void packAndSend()
      {
        forest_t* forest = grid_->forest();
        ghost_t* ghost = grid_->ghostLayer();
        const ElementVectorType& elements = grid_->elements();

        EntityImpl dummy( grid_ );
        Entity entity( dummy );
        EntityImpl& realEntity = entity.impl();

        size_t data_size = 0 ;
        for( size_t m = 0; m < ghost->mirrors.elem_count; ++ m )
        {
          quadrant_t* mirror = PForest :: quadrant_array_index ( &ghost->mirrors, m );
          quadidx_t quadId = mirror->p.piggy3.local_num ;
          realEntity.update( elements[ quadId ] );

          dataHandle_.gather( mirrorData_, entity );
          if( data_size == 0 )
          {
            // after the first element the buffer size should
            // be equal to data size per element
            data_size = mirrorData_.size();
            // hack: this is needed for dune-fem cached comm
            entitySize_ = dataHandle_.size( entity );
          }
        }

        // create receive buffer with required data size
        readBuffer_.resize( ghost->ghosts.elem_count * data_size );

        // obtain vector with pointer that point to data of each mirror element
        mirrorData2_ = mirrorData_.dataPointers( ghost->mirrors.elem_count, data_size );

        /////////////////////////////////////////////////////
        //    exchange begin
        /////////////////////////////////////////////////////
        exc_ = PForest :: ghost_exchange_custom_begin( forest, ghost, data_size, mirrorData2_.data(), readBuffer_.data() );
        ready_ = false;
      }

      void waitAndUnpack()
      {
        if ( ready_ ) return ;

        assert( exc_ );
        // wait for communication to be finished
        PForest :: ghost_exchange_custom_end( exc_ );
        exc_ = nullptr;

        forest_t* forest = grid_->forest();
        ghost_t* ghost = grid_->ghostLayer();
        const ElementVectorType& elements = grid_->elements();

        EntityImpl dummy( grid_ );
        Entity entity( dummy );
        EntityImpl& realEntity = entity.impl();

        // make sure that buffer is at beginning
        readBuffer_.resetReadPosition();
        quadidx_t quadId = forest->local_num_quadrants;
        for( size_t g = 0; g < ghost->ghosts.elem_count; ++ g, ++ quadId )
        {
          realEntity.update( elements[ quadId ] );
          //dataHandle_.scatter( readBuffer_, entity, dataHandle_.size(entity) );
          dataHandle_.scatter( readBuffer_, entity, entitySize_ );
        }

        ready_ = true;
      }
    };

  public:
    template< class DataHandle, class Data >
    LeafCommunication
    interiorGhostCommunication( CommDataHandleIF< DataHandle, Data > &dataHandle,
                                const CommunicationDirection direction = ForwardCommunication ) const
    {
      if( direction == ForwardCommunication )
      {
        // check for higher codimension
        for( int i=1; i<= dimension; ++i )
          if( dataHandle.contains( i, dimension ) )
            DUNE_THROW(NotImplemented,"BorderBorder interface not implemented!");

        typedef P4estCommunicationFutureImpl< CommDataHandleIF< DataHandle, Data > > CommFutureImpl;
        std::shared_ptr< P4estCommunicationFutureIF > ptr( new CommFutureImpl(extraData(), dataHandle ) );
        return LeafCommunication( ptr );
      }
      else
      {
        DUNE_THROW(NotImplemented,"Backward not implemented!");
      }

      return LeafCommunication();
    }

    /** \brief obtain Communication object
     *
     *  The Communication object should be used to globally
     *  communicate information between all processes sharing this grid.
     *
     *  \note The Communication object returned is identical to the
     *        one returned by the host grid.
     */
    const Communication &comm () const
    {
      return comm_ ;
    }

    static MPICommunicatorType defaultCommunicator()
    {
      return MPIHelper :: getCommunicator();
    }

    /** \brief rebalance the load each process has to handle
     *
     *  A parallel grid is redistributed such that each process has about
     *  the same load (e.g., the same number of leaf entities).
     *
     *  \note DUNE does not specify, how the load is measured.
     *
     *  \returns \b true, if the grid has changed.
     */
    bool loadBalance ()
    {
      DoNothingDataHandle dh;
      // use loadBalance without data handle
      return loadBalanceImpl( dh );
    }

    /** \brief rebalance the load each process has to handle
     *
     *  A parallel grid is redistributed such that each process has about
     *  the same load (e.g., the same number of leaf entities).
     *
     *  \param dataHandle  A handle to access user data during the load
     *                     balancing process. This follows the CommDataHandleIF interface.
     *
     *  \note DUNE does not specify, how the load is measured.
     *
     *  \returns \b true, if the grid has changed.
     */
    template< class DataHandleImpl, class Data >
    bool loadBalance ( CommDataHandleIF< DataHandleImpl, Data > &dataHandleIF )
    {
      return loadBalanceImpl( dataHandleIF );
    }

  protected:
    template <class DataHandle>
    struct DebugDataHandle
    {
      typedef typename PForest :: gloidx_t gloidx_t;
      typedef typename DataHandle::DataType  DataType;
      typedef typename Traits :: template Codim< 0 > :: Entity  Entity ;

      DataHandle& dh_;
      mutable gloidx_t globalId_;

      DebugDataHandle( DataHandle& dh, gloidx_t start )
        : dh_( dh ), globalId_( start )
      {}

      bool contains ( int _dim, int codim ) const  { return dh_.contains( _dim, codim ); }
      bool fixedSize ( int _dim, int codim ) const { return dh_.fixedSize( _dim, codim ); }
      size_t size ( const Entity &entity ) const { return dh_.size( entity ); }
      template< class Buffer >
      void gather ( Buffer &buffer, const Entity &entity ) const
      {
        dh_.gather( buffer, entity );
        buffer.write( globalId_ );
        ++globalId_;
      }

      template< class Buffer >
      void scatter ( Buffer &buffer, const Entity &entity, size_t n ) const
      {
        dh_.scatter( buffer, entity, n );
        gloidx_t id;
        buffer.read( id );
        assert( globalId_ == id );
        ++globalId_;
      }

      size_t entitySize( const size_t s ) const { return (s-sizeof(gloidx_t))/sizeof(DataType); }
    };

    template< class DataHandleImpl, class Data >
    bool loadBalanceImpl ( CommDataHandleIF< DataHandleImpl, Data > &dh )
    {
      typedef CommDataHandleIF< DataHandleImpl, Data > DataHandle;
      // if DoNothingDataHandle was passed then no data is processed
      static constexpr bool hasData = ! std::is_same< DataHandleImpl, DoNothingDataHandle > :: value;
      static constexpr bool hasReserveAndCompress =
            std::is_base_of< P4estGridLoadBalanceHandleWithReserveAndCompress, DataHandleImpl >::value;

      //std::cout << "Calling loadBalance" << std::endl;
      const int rank = comm().rank();
      bool gridChanged = false ;

      // store current forest
      forest_t* oldForest = forest_;

      // delete ghostlayer and mesh here, maybe?

      typedef typename PForest :: gloidx_t gloidx_t;
      typedef typename PForest :: tree_t   tree_t ;
      typedef typename PForest :: topidx_t topidx_t ;
      typedef typename PForest :: quadidx_t quadidx_t ;
      typedef typename PForest :: quadrant_t quadrant_t ;

      gloidx_t old_begin = oldForest->global_first_quadrant[ rank ];
      gloidx_t old_end   = oldForest->global_first_quadrant[ rank+1 ];
      assert( (old_end - old_begin) == oldForest->local_num_quadrants );

#ifndef NDEBUG
      // store the globally unique quadrant id in user_long for debugging purpose
      {
        gloidx_t gloQuadId = old_begin; // globally unique first index
        for( topidx_t treeid = oldForest->first_local_tree;
             treeid <= oldForest->last_local_tree; ++ treeid )
        {
          tree_t* tree = PForest :: tree_array_index (oldForest->trees, treeid );

          const quadidx_t elemCount = tree->quadrants.elem_count ;
          for( quadidx_t count = 0; count < elemCount; ++count, ++gloQuadId )
          {
            // get quadrant
            quadrant_t* quad = PForest :: quadrant_array_index ( &tree->quadrants, count );
            quad->p.user_long = gloQuadId;
          }
        }
      }
#endif

      // copy forest and re-partition it
      forest_t* newForest = PForest :: forest_copy( forest_ );
      PForest :: partition( newForest );

      using detail:: SimpleStreamBuffer;

      SimpleStreamBuffer destData;
      SimpleStreamBuffer srcData;

      gloidx_t new_begin = newForest->global_first_quadrant[ rank ];
      gloidx_t new_end   = newForest->global_first_quadrant[ rank+1 ];
      assert( (new_end - new_begin) == newForest->local_num_quadrants );

      //std::cout << "old P " << rank << ": " << old_begin << " " << old_end << std::endl;
      //std::cout << "new P " << rank << ": " << new_begin << " " << new_end << std::endl;

      ElementVectorType oldElements;
      oldElements.swap( elements_ );

      typedef typename Traits :: template Codim< 0 > :: Entity  Entity ;
      typedef P4estGridEntity< 0, dimension, const Grid > EntityImpl;

      EntityImpl dummy( extraData() );
      Entity entity( dummy );
      EntityImpl& realEntity = entity.impl();

      ///////////////////////////////////////////////
      // pack user data
      ///////////////////////////////////////////////
      typedef typename PForest :: transfer_context_t  transfer_context_t;

      size_t data_size = 0;
      size_t entitySize = 0;

      transfer_context_t* tc = nullptr;
      if constexpr ( hasData )
      {
#ifndef NDEBUG
        // this data handle also keeps track of global quadrant ids
        typedef DebugDataHandle< DataHandle > DebugDataHandleType;
        DebugDataHandleType dataHandle( dh, old_begin );
        auto computeEntitySize = [&dataHandle]( const size_t s ) { return dataHandle.entitySize(s); };
#else
        DataHandle& dataHandle = dh;
        auto computeEntitySize = []( const size_t s ) { return s/sizeof(typename DataHandle::DataType); };
#endif
        for(quadidx_t quadId = 0; quadId < oldForest->local_num_quadrants; ++quadId )
        {
          realEntity.update( oldElements[ quadId ] );
          // write entity data to stream
          dataHandle.gather( srcData, entity );
          // can only handle fixed data size transfer at the moment
          if( data_size == 0 )
          {
            // after the first element buffer size should
            // be equal to data size per element
            data_size = srcData.size();
            entitySize = computeEntitySize( data_size );
            srcData.reserve( data_size * oldForest->local_num_quadrants );
          }
        }

        // create destination buffer with correct size
        destData.resize( data_size * newForest->local_num_quadrants );

        // transfer data from old to new forest
        tc = PForest :: transfer_fixed_begin( newForest, oldForest, destData.data(), srcData.data(), data_size );

      }

      assert( hasData == (data_size > 0) );

      // free all ghost indices
      freeGhostIndices( oldElements, oldForest->local_num_quadrants );

      gloidx_t overlap_begin = old_begin - new_begin;
      gloidx_t overlap_end   = new_end   - old_end;

      {
        const gloidx_t oldSize = oldForest->local_num_quadrants;
        // keep_begin min of new_begin - old_begin bounded by oldSize
        gloidx_t keep_begin = std::min(-overlap_begin, oldSize);
        gloidx_t keep_end   = std::min(-overlap_end  , oldSize );

        // std::cout << keep_begin << " " << keep_end << std::endl;

        for( gloidx_t quadId = 0; quadId < keep_begin; ++quadId )
        {
          freeUserIndex( oldElements[ quadId ] );
        }

        for( gloidx_t quadId = oldSize - keep_end; quadId < oldSize; ++quadId )
        {
          freeUserIndex( oldElements[ quadId ] );
        }
      }

      // reset element vector
      elements_.clear();
      elements_.reserve( newForest->local_num_quadrants );

      const gloidx_t newSize = newForest->local_num_quadrants;

      //std::cout << "P " << rank << ": " << overlap_begin << " " << overlap_end << std::endl;

      gloidx_t quadId = 0;
      for( topidx_t treeid = newForest->first_local_tree;
           treeid <= newForest->last_local_tree; ++ treeid )
      {
        tree_t* tree = PForest :: tree_array_index (newForest->trees, treeid );

        const quadidx_t elemCount = tree->quadrants.elem_count ;
        for( quadidx_t count = 0; count < elemCount; ++count, ++quadId )
        {
          // get quadrant
          quadrant_t* quad = PForest :: quadrant_array_index ( &tree->quadrants, count );
          assert( quad->p.user_long == (quadId + new_begin) );

          // check if quadrant is new
          if( quadId < overlap_begin || quadId >= (newSize - overlap_end) )
          {
            // create new element
            EntityInfo info( quad, treeid, quadId, getUserIndex() );
            elements_.push_back( info );
          }
          else // stayed the same, therefore copy userIndex
          {
            gloidx_t oldQuadId = (quadId - overlap_begin); // same as  + new_begin - old_begin);
            // if element existed before then we check here
            // whether the computed old quadId is correct
            assert( oldQuadId == quad->p.user_long - old_begin );
            assert( oldQuadId >= 0 && oldQuadId < oldElements.size() );

            // create new element
            EntityInfo info( quad, treeid, quadId, oldElements[ oldQuadId ].userIndex() );
            elements_.push_back( info );
          }
        }
      }

      // clear old element vector
      oldElements.clear();

      ///////////////////////////////////////////////
      // unpack user data
      ///////////////////////////////////////////////
      if constexpr ( hasData )
      {
        // finished transfer communication
        PForest :: transfer_fixed_end( tc );

        // free old data
        srcData.clear();

        // unpack data
        destData.resetReadPosition();

        if constexpr ( hasReserveAndCompress )
        {
          DataHandleImpl& dataHandle = static_cast< DataHandleImpl& > (dh);
          dataHandle.reserveMemory( newForest->local_num_quadrants );
        }

#ifndef NDEBUG
        // this data handle also keeps track of global quadrant ids
        typedef DebugDataHandle< DataHandle > DebugDataHandleType;
        DebugDataHandleType dataHandle( dh, new_begin );
#else
        DataHandle& dataHandle = dh;
#endif
        for(quadidx_t quadId = 0; quadId < newForest->local_num_quadrants; ++quadId )
        {
          realEntity.update( elements_[ quadId ] );
          // first read size that was transmitted
          dataHandle.scatter( destData, entity, entitySize );
        }

        // make sure all data was read
        assert( destData.position() == destData.size() );
        if( destData.position() != destData.size() )
        {
          DUNE_THROW(InvalidStateException,"P4estGrid::loadBalance: data size does not match during loadBalance!");
        }
      }

      // store new forest
      forest_ = newForest;

      // TODO: extract from PForest
      gridChanged = true;

      if( gridChanged )
      {
        update();
      }
      PForest :: forest_destroy( oldForest );

      if constexpr ( hasData && hasReserveAndCompress )
      {
        DataHandleImpl& dataHandle = static_cast< DataHandleImpl& > (dh);
        dataHandle.compress();
      }
      return gridChanged;
    }


  public:
    /** \brief obtain EntityPointer from EntitySeed */
    template< class EntitySeed >
    typename Traits::template Codim< EntitySeed::codimension >::Entity
    entity ( const EntitySeed &seed ) const
    {
      typedef typename Traits::template Codim< EntitySeed::codimension >::EntityImpl EntityImpl;
      return EntityImpl( extraData(), seed.info() );
    }

    template< PartitionIteratorType pitype >
    typename Traits::template Partition< pitype >::LeafGridView leafGridView () const
    {
      typedef typename Traits::template Partition< pitype >::LeafGridView LeafGridView;
      typedef typename LeafGridView::GridViewImp LeafGridViewImp;
      return LeafGridView( LeafGridViewImp( *this ) );
    }

    typename Traits::LeafGridView leafGridView() const
    {
      return this->template leafGridView< All_Partition > ();
    }


    template< PartitionIteratorType pitype >
    typename Traits::template Partition< pitype >::LevelGridView levelGridView (const int /* level */) const
    {
      return this->template leafGridView< pitype >();
    }

    typename Traits::LevelGridView levelGridView(const int /* level */) const
    {
      return leafGridView();
    }



    /** \} */

    /** \name Miscellaneous Methods
     *  \{ */

#if 0
    template< int codim >
    const typename Traits::template Codim< codim >::ReferenceCube &
    referenceCube () const
    {
      integral_constant< int, codim > codimVariable;
      return refCubes_[ codimVariable ];
    }

    const ReferenceCube &referenceCube () const
    {
      return referenceCube< 0 >();
    }

    //! return geometric information of given level
    const GridLevel& geometricLevel( const size_t level ) const
    {
      assert( level < gridLevels_.size() );
      assert( gridLevels_[ level ] );
      return *(gridLevels_[ level ]);
    }
#endif
    size_t hSize( const int codim ) const
    {
      return indexManager_[ codim ].getMaxIndex();
    }

    /*
    void setUserIndex( quadrant_t *quad ) const
    {
      const int cd = 0;
      quad->p.user_long = indexManager_[ cd ].getIndex();
    }
    */

    int getUserIndex() const
    {
      const int cd = 0;
      return indexManager_[ cd ].getIndex();
    }

    void copyUserIndex( const quadrant_t* oldQuad, quadrant_t *quad ) const
    {
      quad->p.user_long = oldQuad->p.user_long;
    }

    void freeUserIndex( const EntityInfo& info ) const
    {
      indexManager_[ info.codimension() ].freeIndex( info.freeUserIndex() );
    }

    void freeGhostIndices( ElementVectorType& elements, const size_t firstGhost ) const
    {
      // free all ghost indices
      const size_t nGhosts = elements.size();
      for( size_t gh=firstGhost; gh<nGhosts; ++gh )
      {
        freeUserIndex( elements[ gh ] );
      }
    }

    void init ()
    {
      typedef typename PForest :: topidx_t  topidx_t ;
      typedef typename PForest :: tree_t    tree_t ;
      typedef typename PForest :: quadidx_t quadidx_t ;

      elements_.clear();
      elements_.reserve( forest_->local_num_quadrants );

      quadidx_t quadId = 0;
      for( topidx_t treeid = forest_->first_local_tree;
           treeid <= forest_->last_local_tree; ++ treeid )
      {
        tree_t* tree = PForest :: tree_array_index (forest_->trees, treeid );

        /* index setup */
        const quadidx_t elemCount = tree->quadrants.elem_count ;
        for( quadidx_t count = 0; count < elemCount; ++count, ++quadId )
        {
          //std::cout << "old quad number " << oldCount << "  new number " << count << std::endl;
          quadrant_t* quad = PForest :: quadrant_array_index ( &tree->quadrants, count );
          EntityInfo info( quad, treeid, quadId, getUserIndex() );
          elements_.push_back( info );
        }
      }

#ifndef NDEBUG
      size_t i=0;
      for( const auto& item : elements_ )
      {
        assert( item.quadId() == i );
        ++ i;
      }
#endif
    }

    /** \brief update grid caches
     *
     *  This method has to be called whenever the underlying host grid changes.
     *
     *  \note If you adapt the host grid through this geometry grid's
     *        adaptation or load balancing methods, update is automatically
     *        called.
     */
    //- --update
    void update ()
    {
      // destroy old structures
      PForest :: mesh_destroy( mesh_ );
      //PForest :: numbers_destroy( numbers_ );
      PForest :: ghost_destroy( ghost_ );

      // recreate helper structures
      ghost_   = PForest :: ghost_new( forest_ );
      //numbers_ = PForest :: numbers_new( forest_, ghost_ );
      //numbers_ = PForest :: numbers_new( forest_, nullptr );
      mesh_    = PForest :: mesh_new( forest_, ghost_ );

      // TODO: This might not be needed
      maxLevel_ = 0 ;
      typedef typename PForest :: topidx_t topidx_t ;
      // typedef typename PForest :: level_t  level_t ;
      for( topidx_t tree = forest_->first_local_tree;
           tree <= forest_->last_local_tree; ++ tree )
      {
        const int maxlevel = PForest :: tree_array_index (forest_->trees, tree )->maxlevel;
        if( maxLevel_ < maxlevel ) maxLevel_ = maxlevel;
      }

      typedef typename PForest :: quadidx_t quadidx_t ;
      quadidx_t ghostId = elements_.size();
      elements_.reserve( ghostId + ghost_->ghosts.elem_count );
      for( size_t gh = 0 ; gh < ghost_->ghosts.elem_count ; ++ gh, ++ghostId )
      {
        quadrant_t* ghost = PForest :: quadrant_array_index( &ghost_->ghosts , gh );
        elements_.push_back( EntityInfo( ghost, ghost->p.piggy3.which_tree, ghostId, getUserIndex(), true ) );
      }

      // reconstruct hierarchy
      indices_.update();

      // reset cached sizes
      sizeCache_.reset();

      const int newNumLevels = maxLevel()+1;
      const int oldNumLevels = levelIndexVec_.size();

      for( int i = newNumLevels; i < oldNumLevels; ++i )
      {
        levelIndexVec_[ i ].reset();
      }
      levelIndexVec_.resize( newNumLevels );

      //leafIndexSet_.reset();
    }

    /** \} */

    /*
    typename DomainGeometryType::CoordinateType
    vertex2world(const typename DomainGeometryType::topidx_t treeId, const double* vxyz) const
    {
      assert( domain_ );
      return domain_->vertex2world( treeId, vxyz );
    }
    */

    connectivity_t* connectivity() const { return connectivity_ ; }
    forest_t* forest() const { return forest_ ; }
    mesh_t*  mesh() const { return mesh_ ; }
    ghost_t* ghostLayer() const { return ghost_ ; }

    numbers_t* numbers() const { return numbers_ ; }

    p4est_locidx_t localNumQuadrants() const
    {
      assert( forest_->local_num_quadrants == mesh_->local_num_quadrants );
      return forest_->local_num_quadrants;
    }

    p4est_locidx_t ghostNumQuadrants() const
    {
      assert( (p4est_locidx_t) ghostLayer()->ghosts.elem_count == mesh_->ghost_num_quadrants );
      return p4est_locidx_t(ghostLayer()->ghosts.elem_count);
    }

    const P4estIndexType& indices() const { return indices_ ; }

    const ReferenceElementType& referenceElement() const { return referenceElement_; }

    static const ReferenceFaceType& faceReferenceElement()
    {
      static const ReferenceFaceType& refFace = Dune::ReferenceElements< ctype, dimension-1 >::cube();
      return refFace;
    }

    const EntityInfo& element( const size_t i ) const
    {
      assert( i < elements_.size() );
      return elements_[ i ];
    }

    const ElementVectorType& elements() const { return elements_; }
    ElementVectorType& elements() { return elements_; }

    //! return amount of memory used by p4est
    // TODO add dune structures to the calculation
    size_t memoryUsed() const { return PForest :: memory_used( forest_, connectivity_, ghost_, mesh_ ); }

    typedef typename Traits::template Codim< 0 >::Geometry Geometry;
    typedef typename Geometry::GlobalCoordinate GlobalCoordinate;
    const GlobalCoordinate& gridWidth () const
    {
      return gridWidth_;
    }

  protected:
    void buildLocalIntersectionGeometries ()
    {
      /*
      typedef typename Codim< 1 >::LocalGeometry LocalGeo;
      typedef typename LocalGeo::GlobalCoordinate Coordinate;
      typedef SPLocalGeometry< dimension-1, dimension, const Grid > LocalGeoImpl;
      for( int face = 0; face < ReferenceCube::numFaces; ++face )
      {
        const unsigned int direction = ((1 << dimension) - 1) ^ (1 << (face/2));
        Coordinate origin( ctype( 0 ) );
        origin[ face/2 ] = ctype( face & 1 );
        const SPGeometryCache< ctype, dimension, 1 > cache( Coordinate( 1.0 ), direction );
        localFaceGeometry_[ face ] = new LocalGeo( LocalGeoImpl( referenceCube< 1 >(), cache, origin ) );

        for( int index = 0; index < (1 << (dimension-1)); ++index )
        {
          // origin[ face/2 ] = ctype( face & 1 );
          for( int i = 0; i < dimension-1; ++i )
          {
            const int j = (i < face/2 ? i : i+1 );
            origin[ j ] = 0.5 * ((index >> i) & 1);
          }
          const SPGeometryCache< ctype, dimension,1 > cache( Coordinate( 0.5 ), direction );
          localRefinedFaceGeometry_[ (1 << (dimension-1))*face + index ]
            = new LocalGeo( LocalGeoImpl( referenceCube< 1 >(), cache, origin ) );
        }
      }
      */
    }

    const LocalGeometry& geometryInFather( const unsigned int childIndex ) const
    {
      assert( childIndex < numChildren );
      return *(geometryInFather_[ childIndex ]);
    }

    void buildGeometryInFather ()
    {
      typedef P4estGridGeometry< dimension, dimension, const Grid > LocalGeometryImpl;
      for( unsigned int index = 0; index < numChildren; ++index )
      {
        geometryInFather_[ index ] = new LocalGeometry( LocalGeometryImpl( index ) );
      }
    }

  public:
    typedef typename Traits :: ExtraData ExtraData;
    //! return extra data passed to entities (e.g. const Grid*)
    ExtraData extraData () const  { return this; }

    //! duplicate MPI communicator to prevent interference
    CommImplType initMPIComm( const CommImplType& c )
    {
#if HAVE_MPI
      MPI_Comm_dup ( c, &commImpl_ );
#endif
      return commImpl_;
    }

  protected:
    CommImplType commImpl_;
    Communication comm_;
    //std::shared_ptr< const DomainGeometryType > domain_;
    const GlobalCoordinate gridWidth_;
    const ReferenceElementType& referenceElement_;
    const ReferenceFaceType& referenceFace_;

    mutable connectivity_t* connectivity_;
    mutable forest_t*       forest_;
    mutable ghost_t*        ghost_;
    mutable numbers_t*      numbers_;
    mutable mesh_t*         mesh_;
    int maxLevel_ ;

    P4estIndexType indices_ ;
    ElementVectorType elements_;

    std::vector< std::vector< GeometryType > > geomTypes_;
    SizeCacheType sizeCache_ ;

    const LocalGeometry*  geometryInFather_[ numChildren ];
    /*
    GenericGeometry::CodimTable< RefCube, dimension > refCubes_;
    std::vector< GridLevel * > gridLevels_;
    */
    // the level index set ( default type )
    mutable std::vector < std::shared_ptr< LevelIndexSetImp > > levelIndexVec_;
    // the leaf index set
    mutable std::unique_ptr< LeafIndexSetImp > leafIndexSet_;

    mutable HierarchicIndexSet hIndexSet_;
    mutable GlobalIdSet *globalIdSet_;
    mutable LocalIdSet *localIdSet_;

    mutable std::vector< IndexManagerType > indexManager_;

    std::vector< int > markerCounter_ ;

    /*
    const typename Codim< 0 >::LocalGeometry *geometryInFather_[ 1 << dimension ];
    const typename Codim< 1 >::LocalGeometry *localFaceGeometry_[ ReferenceCube::numFaces ];
    const typename Codim< 1 >::LocalGeometry *localRefinedFaceGeometry_[ (1 << (dimension-1))*ReferenceCube::numFaces ];
    */
  };



  // P4estGrid::Codim
  // --------------------

  template< int dim, int dimworld, P4estType elType, class ct >
  template< int codim >
  struct P4estGrid< dim, dimworld, elType, ct >::Codim
  : public Base::template Codim< codim >
  {
    /** \name Entity and Entity Pointer Types
     *  \{ */

    /** \brief type of entity
     *
     *  The entity is a model of Dune::Entity.
     */
    typedef typename Traits::template Codim< codim >::Entity Entity;

    /** \} */

    /** \name Geometry Types
     *  \{ */

    /** \brief type of world geometry
     *
     *  Models the geomtry mapping of the entity, i.e., the mapping from the
     *  reference element into world coordinates.
     *
     *  The geometry is a model of Dune::Geometry, implemented through the
     *  generic geometries provided by dune-grid.
     */
    typedef typename Traits::template Codim< codim >::Geometry Geometry;

    /** \brief type of local geometry
     *
     *  Models the geomtry mapping into the reference element of dimension
     *  \em dimension.
     *
     *  The local geometry is a model of Dune::Geometry, implemented through
     *  the generic geometries provided by dune-grid.
     */
    typedef typename Traits::template Codim< codim >::LocalGeometry LocalGeometry;

    /** \} */

    /** \name Iterator Types
     *  \{ */

    template< PartitionIteratorType pitype >
    struct Partition
    {
      typedef typename Traits::template Codim< codim >
        ::template Partition< pitype >::LeafIterator
        LeafIterator;
      typedef typename Traits::template Codim< codim >
        ::template Partition< pitype >::LevelIterator
        LevelIterator;
    };

    /** \brief type of level iterator
     *
     *  This iterator enumerates the entites of codimension \em codim of a
     *  grid level.
     *
     *  The level iterator is a model of Dune::LevelIterator.
     */
    typedef typename Partition< All_Partition >::LeafIterator LeafIterator;

    /** \brief type of leaf iterator
     *
     *  This iterator enumerates the entites of codimension \em codim of the
     *  leaf grid.
     *
     *  The leaf iterator is a model of Dune::LeafIterator.
     */
    typedef typename Partition< All_Partition >::LevelIterator LevelIterator;

    /** \} */
  };

} // namespace Dune

#include <dune/p4estgrid/persistentcontainer.hh>
#endif // #ifndef DUNE_P4ESTGRID_GRID_HH
