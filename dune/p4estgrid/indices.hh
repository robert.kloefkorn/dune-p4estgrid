/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_INDEX_HH
#define DUNE_P4ESTGRID_INDEX_HH

#include <algorithm>
#include <map>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/p4estgrid/p4est.hh>

namespace Dune
{


  template < class Traits >
  class P4estIndex
  {
    static const int dimension = Traits :: dimension ;
    typedef typename Traits :: ExtraData  ExtraData ;
    typedef P4est< dimension, Traits ::dimensionworld > PForest ;

    typedef typename PForest :: forest_t   forest_t;
    typedef typename PForest :: ghost_t    ghost_t;
    typedef typename PForest :: mesh_t     mesh_t;
    typedef typename PForest :: topidx_t   topidx_t ;
    typedef typename PForest :: quadidx_t  quadidx_t ;
    typedef typename PForest :: level_t    level_t ;
    typedef typename PForest :: tree_t     tree_t ;
    typedef typename PForest :: quadrant_t quadrant_t ;
    typedef typename PForest :: linearid_t linearid_t;

  public:
    typedef int index_t ;

    typedef typename PForest :: markervector_t     markervector_t;
    typedef typename PForest :: adaptationmarker_t adaptationmarker_t;

    enum { memFactor = (1 << dimension ) / ( (1<<dimension) -1 ) };

  public:
    P4estIndex( ExtraData data )
      : data_ ( data ),
        factors_( dimension + 1 ),
        leafSize_( 0 ),
        hSize_( 0 )
    {
      factors_[ 0  ]  = 1;
      factors_[ dimension ] = (1 << dimension);
      factors_[ 1 ] = 2 * dimension ;

      if( dimension == 3 ) factors_[ 2 ] = 12 ;
    }

    void update ()
    {
      // clear all adaptation markers
      adaptationMarker_.clear();
      adaptationMarker_.resize( data()->elements().size(), adaptationmarker_t(0) );

      // compute leaf index
      const auto& hIndexSet = data()->hierarchicIndexSet();
      leafIndex_.resize( data()->hSize( 0 ), (-1) );
      leafSize_ = 0;
      const auto end = data()->template leafend<0>();
      for( auto it = data()->template leafbegin<0>(); it != end; ++it )
      {
        const auto& entity = *it;
        leafIndex_[ hIndexSet.index( entity ) ] = leafSize_ ++;
      }
    }

    index_t leafIndex( const int userIdx,
                       const int subIndex,
                       const int codimension ) const
    {
      assert( codimension >= 0 && codimension <= dimension );
      //return factors_[ codimension ] * getItem( userIdx ).leafIndex() + subIndex;
      return factors_[ codimension ] * leafIndex( userIdx ) + subIndex;
    }

    index_t leafIndex(const int userIdx ) const
    {
      return leafIndex_[ userIdx ];
    }

    //! size of leaf index set
    index_t leafSize( const int codimension ) const
    {
      assert( codimension <= dimension );
      assert( codimension >= 0 );
      return factors_[ codimension ] * leafSize_ ;
    }

    ExtraData data () const { assert( data_ ); return data_ ; }

    adaptationmarker_t& getMark( const size_t userIndex ) const
    {
      assert( userIndex <= adaptationMarker_.size() );
      return adaptationMarker_[ userIndex ];
    }

    markervector_t* markerVector() const
    {
      return &adaptationMarker_;
    }

  protected:
    ExtraData  data_;

    std::vector< index_t > leafIndex_;
    mutable markervector_t adaptationMarker_;

    std::vector< int > factors_;
    index_t leafSize_ ;
    index_t hSize_ ;
  };

}
#endif // DUNE_P4ESTGRID_INDEX_HH
