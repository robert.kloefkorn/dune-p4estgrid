#ifndef DUNE_P4ESTGRID_STRUCTUREDGRIDFACTORY_HH
#define DUNE_P4ESTGRID_STRUCTUREDGRIDFACTORY_HH

#include <array>
#include <memory>
#include <vector>

#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/grid/common/exceptions.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/p4estgrid/grid.hh>

namespace Dune
{

  // External Forward Declarations
  // -----------------------------

  template< class Grid >
  class StructuredGridFactory;



  // StructuredGridFactory for ALUGrid
  // ---------------------------------

  template< int dim, int dimworld, P4estType eltype, class ct >
  class StructuredGridFactory< P4estGrid< dim, dimworld, eltype, ct > >
  {
  public:
    typedef P4estGrid< dim, dimworld, eltype, ct > Grid;
    typedef std::unique_ptr< Grid > SharedPtrType;

  protected:
    typedef StructuredGridFactory< Grid > This;

  public:
    typedef typename Grid::ctype ctype;
    typedef typename MPIHelper :: MPICommunicator MPICommunicatorType ;

    // type of communicator
    typedef Dune :: Communication< MPICommunicatorType >
        Communication ;

    static SharedPtrType
    createSimplexGrid ( const FieldVector<ctype,dimworld>& lowerLeft,
                        const FieldVector<ctype,dimworld>& upperRight,
                        const std::array<unsigned int, dim>& elements,
                        MPICommunicatorType mpiComm = MPIHelper :: getCommunicator() )
    {
      DUNE_THROW(NotImplemented,"P4estGrid does not support simplex type elements yet!");
    }

    static SharedPtrType
    createCubeGrid ( const FieldVector<ctype,dimworld>& lowerLeft,
                     const FieldVector<ctype,dimworld>& upperRight,
                     const std::array<unsigned int, dim>& elements,
                     MPICommunicatorType mpiComm = MPIHelper :: getCommunicator() )
    {
      Communication comm( mpiComm );
      return createCubeGrid( lowerLeft, upperRight, elements, comm );
    }

    static SharedPtrType
    createCubeGrid ( const FieldVector<ctype,dimworld>& lowerLeft,
                     const FieldVector<ctype,dimworld>& upperRight,
                     const std::array<unsigned int, dim>& elements,
                     const Communication& comm )
    {
      typedef GridFactory< Grid > GridFactoryType;
      if( comm.rank() == 0 )
      {
        typedef YaspGrid< dimworld, EquidistantOffsetCoordinates<double,dimworld> > CartesianGridType ;
        std::array< int, dim > dims;
        for( int i=0; i<dim; ++i ) dims[ i ] = elements[ i ];

        Communication commSelf( MPIHelper :: getLocalCommunicator() );
        // create YaspGrid to partition and insert elements that belong to process directly
        CartesianGridType sgrid( lowerLeft, upperRight, dims, std::bitset<dim>(0ULL), 1, commSelf );

        return SharedPtrType( GridFactoryType::createGrid( sgrid, comm ).release() );
      }
      else
      {
        // this will obtain the grid created above through communication
        return SharedPtrType( GridFactoryType::createGrid( comm ).release() );
      }
    }
  };

} // namespace Dune

#endif // #ifndef DUNE_P4ESTGRID_STRUCTUREDGRIDFACTORY_HH
