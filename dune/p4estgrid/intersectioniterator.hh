/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_INTERSECTIONITERATOR_HH
#define DUNE_P4ESTGRID_INTERSECTIONITERATOR_HH

#include <dune/p4estgrid/entitypointer.hh>
#include <dune/p4estgrid/intersection.hh>

namespace Dune
{

  // P4estGridIntersectionIterator
  // --------------------------

  template< class Traits >
  class P4estGridIntersectionIterator
  {
  public:
    typedef typename Traits::Intersection Intersection;
    typedef typename Traits::GridTraits::Grid Grid;
    typedef typename Grid::Traits::template Codim<0>::EntityImpl EntityImpl;

    typedef P4estEntityInfo< Grid::dimension >  EntityInfo;

  private:
    typedef typename Traits::GridTraits::ExtraData  ExtraDataType ;
    typedef typename Traits::IntersectionImpl IntersectionImpl;

  public:
    explicit P4estGridIntersectionIterator ( ExtraDataType data )
    : intersection_( IntersectionImpl() )
    {}

    P4estGridIntersectionIterator ( ExtraDataType data, const EntityImpl& entity )
    : intersection_( IntersectionImpl( data, entity ) )
    {}

    P4estGridIntersectionIterator ( const P4estGridIntersectionIterator &other )
    : intersection_( other.intersectionImpl() )
    {}

    const P4estGridIntersectionIterator &
    operator= ( const P4estGridIntersectionIterator &other )
    {
      intersectionImpl() = other.intersectionImpl();
      return *this;
    }

    bool equals ( const P4estGridIntersectionIterator &other ) const
    {
      return intersectionImpl().equals( other.intersectionImpl() );
    }

    void increment ()
    {
      intersectionImpl().increment();
    }

    const Intersection &dereference () const
    {
      return intersection_;
    }

  private:
    ExtraDataType data () const { return intersectionImpl().data(); }

    IntersectionImpl &intersectionImpl () const
    {
      return intersection_.impl();
    }

    mutable Intersection intersection_;
  };



  // P4estGridLeafIntersectionIteratorTraits
  // ------------------------------------

  template< class Grid >
  struct P4estGridLeafIntersectionIteratorTraits
  {
    typedef typename std::remove_const< Grid >::type::Traits GridTraits;

    typedef typename GridTraits::LeafIntersection Intersection;
    typedef P4estGridLeafIntersection< const Grid > IntersectionImpl;
  };



  // LeafIntersectionIterator
  // ------------------------

  template< class Grid >
  class P4estGridLeafIntersectionIterator
  : public P4estGridIntersectionIterator< P4estGridLeafIntersectionIteratorTraits< Grid > >
  {
    typedef P4estGridLeafIntersectionIteratorTraits< Grid > Traits;
    typedef P4estGridIntersectionIterator< Traits > Base;

    typedef typename Traits::GridTraits::ExtraData  ExtraDataType ;

  public:
    typedef typename Traits::Intersection Intersection;
    typedef typename Base :: EntityImpl  EntityImpl;

  public:
    P4estGridLeafIntersectionIterator ()
    : Base( nullptr )
    {}

    P4estGridLeafIntersectionIterator ( ExtraDataType data )
    : Base( data )
    {}

    P4estGridLeafIntersectionIterator ( ExtraDataType data, const EntityImpl& entity )
    : Base( data, entity )
    {}
  };


#if 0
  // P4estGridLevelIntersectionIteratorTraits
  // -------------------------------------

  template< class Grid >
  struct P4estGridLevelIntersectionIteratorTraits
  {
    typedef typename std::remove_const< Grid >::type::Traits GridTraits;

    typedef typename GridTraits::LevelIntersection Intersection;
    typedef P4estGridLevelIntersection< const Grid > IntersectionImpl;
  };



  // P4estGridLevelIntersectionIterator
  // -------------------------------

  template< class Grid >
  class P4estGridLevelIntersectionIterator
  : public P4estGridIntersectionIterator< P4estGridLevelIntersectionIteratorTraits< Grid > >
  {
    typedef P4estGridLevelIntersectionIteratorTraits< Grid > Traits;
    typedef P4estGridIntersectionIterator< Traits > Base;

    typedef typename Traits::GridTraits::ExtraData  ExtraDataType ;

  public:
    typedef typename Traits::Intersection Intersection;
    typedef typename Base :: EntityInfo  EntityInfo;

  public:
    P4estGridLevelIntersectionIterator ()
    : Base( nullptr )
    {}

    P4estGridLevelIntersectionIterator ( ExtraDataType data )
    : Base( data )
    {}

    P4estGridLevelIntersectionIterator ( ExtraDataType data, const EntityInfo& info )
    : Base( data, info )
    {}
  };
#endif

  template< class Grid >
  using P4estGridLevelIntersectionIterator = P4estGridLeafIntersectionIterator< Grid >;

}

#endif // #ifndef DUNE_P4ESTGRID_INTERSECTIONITERATOR_HH
