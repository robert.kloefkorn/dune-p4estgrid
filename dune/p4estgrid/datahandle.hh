/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_DATAHANDLE_HH
#define DUNE_P4ESTGRID_DATAHANDLE_HH

#include <dune/common/typetraits.hh>
#include <dune/grid/common/datahandleif.hh>
#include <dune/grid/common/grid.hh>

#include <dune/p4estgrid/capabilities.hh>

namespace Dune
{
  template< class Grid, class HostEntity >
  struct P4estGridEntityProxy
  {
    typedef typename remove_const< Grid >::type::Traits Traits;

    typedef typename Traits::ExtraData ExtraData;

    static const int dimension = HostEntity::dimension;
    static const int codimension = HostEntity::codimension;

    typedef Dune::Entity< codimension, dimension, const Grid, P4estGridEntity > Entity;

  private:
    typedef P4estGridEntity< codimension, dimension, const Grid > EntityImpl;

  public:
    P4estGridEntityProxy ( ExtraData data, const HostEntity &hostEntity )
    : entity_( EntityImpl( data, hostEntity ) )
    {}

    const Entity &operator* () const { return entity_; }
    Entity &operator* () { return entity_; }

  private:
    Entity entity_;
  };


  // P4estGridDataHandle
  // -----------------------

  template< class Grid, class WrappedHandle >
  class P4estGridDataHandle
  : public CommDataHandleIF< P4estGridDataHandle< Grid, WrappedHandle >, typename WrappedHandle::DataType >
  {
    typedef typename remove_const< Grid >::type::Traits Traits;

    typedef typename Traits::ExtraData ExtraData;

  public:
    P4estGridDataHandle ( ExtraData data, WrappedHandle &handle )
    : data_( data ),
      wrappedHandle_( handle )
    {}

    bool contains ( int dim, int codim ) const
    {
      return wrappedHandle_.contains( dim, codim );
    }

    bool fixedsize ( int dim, int codim ) const
    {
      return wrappedHandle_.fixedsize( dim, codim );
    }

    template< class HostEntity >
    size_t size ( const HostEntity &hostEntity ) const
    {
      P4estGridEntityProxy< Grid, HostEntity > proxy( data_, hostEntity );
      return wrappedHandle_.size( *proxy );
    }

    template< class MessageBuffer, class HostEntity >
    void gather ( MessageBuffer &buffer, const HostEntity &hostEntity ) const
    {
      P4estGridEntityProxy< Grid, HostEntity > proxy( data_, hostEntity );
      wrappedHandle_.gather( buffer, *proxy );
    }

    template< class MessageBuffer, class HostEntity >
    void scatter ( MessageBuffer &buffer, const HostEntity &hostEntity, size_t size )
    {
      P4estGridEntityProxy< Grid, HostEntity > proxy( data_, hostEntity );
      wrappedHandle_.scatter( buffer, *proxy, size );
    }

  private:
    ExtraData data_;
    WrappedHandle &wrappedHandle_;
  };

  // P4estGridDataHandle
  // ----------------

  template< class Grid, class WrappedHandle >
  class CartesianALULBDataHandle
  {
    typedef typename remove_const< Grid >::type::Traits Traits;
    typedef typename Traits::ExtraData ExtraData;

    typedef typename Grid :: HostGrid HostGrid ;
    typedef typename HostGrid :: ObjectStreamType  ObjectStream;
    typedef typename HostGrid :: template Codim< 0 > :: Entity  HostElement;

  public:
    CartesianALULBDataHandle( ExtraData data, WrappedHandle &handle )
    : data_( data ),
      wrappedHandle_( handle )
    {}

    void inlineData ( ObjectStream &stream, const HostElement &hostElement ) const
    {
      P4estGridEntityProxy< Grid, HostElement > proxy( data_, hostElement );
      wrappedHandle_.inlineData( stream, *proxy );
    }

    void xtractData ( ObjectStream &stream, const HostElement &hostElement, size_t newElements )
    {
      P4estGridEntityProxy< Grid, HostElement > proxy( data_, hostElement );
      wrappedHandle_.xtractData( stream, *proxy, newElements );
    }

    void compress ()
    {
      wrappedHandle_.compress();
    }

  private:
    ExtraData data_;
    WrappedHandle &wrappedHandle_;
  };

}

#endif // #ifndef DUNE_P4ESTGRID_DATAHANDLE_HH
