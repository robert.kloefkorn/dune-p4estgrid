/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_HIERARCHICITERATOR_HH
#define DUNE_P4ESTGRID_HIERARCHICITERATOR_HH

#include <dune/grid/common/entityiterator.hh>

#include <dune/p4estgrid/entityinfo.hh>
#include <dune/p4estgrid/entitypointer.hh>

namespace Dune
{

  // P4estGridHierarchicIteratorTraits
  // ---------------------------------

  template< class Grid >
  struct P4estGridHierarchicIteratorTraits
  : public P4estGridEntityPointerTraits< 0, Grid >
  {
    typedef typename std::remove_const< Grid >::type::Traits Traits;
  };



  // P4estGridHierarchicIterator
  // ---------------------------

  template< class Grid >
  class P4estGridHierarchicIterator
  : public P4estGridEntityPointer< P4estGridHierarchicIteratorTraits< Grid > >
  {
    typedef P4estGridEntityPointer< P4estGridHierarchicIteratorTraits< Grid > > Base;

  protected:
    using Base::data;
    using Base::update;
    using Base::info;
    using Base::clear;

    typedef typename Base::ExtraData ExtraData;

    typedef typename Base::EntityInfo EntityInfo;

    typedef Dune :: P4est< Grid :: dimension, Grid :: dimensionworld > PForest ;
    typedef typename PForest :: tree_t      tree_t ;
    typedef typename PForest :: forest_t    forest_t ;
    typedef typename PForest :: level_t     level_t;
    typedef typename PForest :: topidx_t    topidx_t ;
    typedef typename PForest :: quadidx_t   quadidx_t ;
    typedef typename PForest :: quadrant_t  quadrant_t ;

    typedef topidx_t  index_t;
    typedef void* ItemType;
    //typedef typename Grid :: P4estHierarchyType  HierarchyType ;
    //typedef typename HierarchyType :: index_t index_t;
    //typedef typename HierarchyType :: ItemType  ItemType;

  public:
    explicit P4estGridHierarchicIterator ( ExtraData data )
    : Base( data ),
      startInfo_(),
      treeid_( -1 ),
      startElem_( nullptr ),
      current_( nullptr ),
      maxLevel_( -1 )
    {}

    explicit P4estGridHierarchicIterator ( ExtraData data,
        const EntityInfo& info, const level_t maxLevel )
    : Base( data ),
      startElem_( &(data->hierarchy().getItem( info.userIndex() )) ),
      treeid_( info.treeId() ),
      current_( nullptr ),
      maxLevel_( maxLevel )
    {
      std::abort();
      // TODO: This needs to be fixed.
      clear();
      //increment();
    }

    template< class T >
    bool equals ( const P4estGridEntityPointer< T > &other ) const
    {
      return Base::equals( other );
    }

    const ItemType* goNextElement( const ItemType* current )
    {
      /*
      assert( current );
      const HierarchyType& hierarchy = data()->hierarchy();
      const ItemType* nextelem = hierarchy.down( current );
      if( nextelem && nextelem->level() <= maxLevel_ )
      {
        return nextelem ;
      }

      nextelem = hierarchy.next( current );
      if(nextelem && nextelem->level() <= maxLevel_ )
      {
        return nextelem;
      }

      nextelem = hierarchy.up( current );
      if(nextelem == startElem_ ) return nullptr;

      while( ! hierarchy.next( nextelem ) )
      {
        nextelem = hierarchy.up( nextelem );
        if( !nextelem || nextelem == startElem_ ) return nullptr;
      }

      if( nextelem ) nextelem = hierarchy.next( nextelem );

      return nextelem;
      */
      return nullptr;
    }

    void increment ()
    {
      /*
      current_ = goNextElement( current_ );
      if( ! current_ )
      {
        clear();
        return ;
      }
      update( EntityInfo( current_->quadrant(), treeid_, 0 ) );
      */
    }

  protected:
    EntityInfo startInfo_;
    topidx_t   treeid_;
    const ItemType* startElem_;

    const ItemType* current_;
    level_t maxLevel_;
  };

} // namespace Dune

#endif // #ifndef DUNE_P4ESTGRID_HIERARCHICITERATOR_HH
