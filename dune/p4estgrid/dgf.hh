/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_DGFPARSER_HH
#define DUNE_P4ESTGRID_DGFPARSER_HH

#include <dune/common/typetraits.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/io/file/dgfparser/dgfparser.hh>

#include <dune/grid/io/file/dgfparser/dgfyasp.hh>

#include <dune/p4estgrid/grid.hh>
#include <dune/p4estgrid/gridfactory.hh>
#include <dune/p4estgrid/structuredgridfactory.hh>

namespace Dune
{

  // DGFGridFactory for P4estGrid
  // --------------------------------

  template< int dim, int dimworld, P4estType elType, class ct >
  struct DGFGridFactory< P4estGrid< dim, dimworld, elType, ct > >
  {
    typedef P4estGrid< dim, dimworld, elType, ct > Grid;

    typedef MPIHelper::MPICommunicator MPICommunicatorType;
    typedef typename Grid::Communication  Communication;

    static const int dimension = Grid::dimension;
    typedef typename Grid::template Codim< 0 >::Entity Element;
    typedef typename Grid::template Codim< dimension >::Entity Vertex;


    explicit DGFGridFactory ( std::istream &input,
                              MPICommunicatorType comm = MPIHelper::getCommunicator() )
      : grid_(nullptr),
        dgf_( 0, 1)
    {
      generate( input, comm );
    }

    explicit DGFGridFactory ( const std::string &filename,
                              MPICommunicatorType comm = MPIHelper::getCommunicator() )
      : grid_(nullptr),
        dgf_( 0, 1)
    {
      std::ifstream input( filename.c_str() );
      if( !input )
        DUNE_THROW( DGFException, "Unable to open file: " << filename << "." );
      generate( input, comm, filename );
      input.close();
    }

    Grid *grid () const
    {
      return grid_;
    }

    template< class Intersection >
    bool wasInserted ( const Intersection &intersection ) const
    {
      return false;
    }

    template< class Intersection >
    int boundaryId ( const Intersection &intersection ) const
    {
      return intersection.impl().boundaryId();
    }

    template< int codim >
    int numParameters () const
    {
      return 0;
    }

    template< class Entity >
    std::vector< double > &parameter ( const Entity &entity )
    {
      DUNE_THROW( InvalidStateException,
                  "Calling DGFGridFactory::parameter is only allowed if there are parameters." );
    }

    bool haveBoundaryParameters () const
    {
      return false ;
    }

    template< class Intersection >
    const typename DGFBoundaryParameter::type &
    boundaryParameter ( const Intersection &intersection ) const
    {
      return DGFBoundaryParameter::defaultValue();
    }

  protected:
    typedef GridFactory< Grid > GridFactoryType;

    void generate ( std::istream &input, MPICommunicatorType mpiComm, const std::string &name = "" )
    {
      dgf_.element = DuneGridFormatParser::Cube ;
      dgf_.dimgrid = dim;
      dgf_.dimw = dimworld;

      Communication comm( mpiComm );

      // rank 0 creates connectivity which is then communicated to all other ranks
      if( comm.rank() == 0 )
      {
        // check that file is DGF
        const bool isDGF = dgf_.isDuneGridFormat( input );
        input.seekg( 0 );
        if( !isDGF )
          return ;

        // TODO: overlap
        // dgf::GridParameterBlock parameter( input );
        dgf::IntervalBlock intervalBlock( input );
        if( intervalBlock.isactive() && intervalBlock.numIntervals() == 1 )
        {
          const dgf::IntervalBlock::Interval &interval = intervalBlock.get( 0 );
          assert( int(interval.n.size()) == dim );
          std::array<unsigned int, dim> N;
          FieldVector<double, dimworld> lowerLeft;
          FieldVector<double, dimworld> upperRight;
          for( int i=0; i<dim; ++i )
          {
            N[ i ]          = interval.n[ i ] ;
            lowerLeft[ i ]  = interval.p[ 0 ][ i ];
            upperRight[ i ] = interval.p[ 1 ][ i ];
          }

          typedef StructuredGridFactory< Grid > SGF;
          grid_ = SGF::createCubeGrid( lowerLeft, upperRight, N, comm ).release();
        }
        else // unstructured case
        {
          typedef Dune::ALUGrid<Grid::dimension, Grid::dimensionworld, Dune::cube, Dune::nonconforming, Dune::ALUGridNoComm > ALUGridType;
          std::shared_ptr< ALUGridType > gridPtr( Dune::GridPtr< ALUGridType >(input).release() );
          grid_ = GridFactoryType::createGrid( *gridPtr, comm ).release();
        }
      }
      else // all other ranks
      {
        // grid is obtained through communication
        grid_ = GridFactoryType::createGrid( comm ).release();
      }
    }

    Grid *grid_;
    DuneGridFormatParser dgf_;
  };


  // DGFGridInfo for P4estGrid
  // -----------------------------

  template< int dim, int dimworld, P4estType elType, class ct >
  struct DGFGridInfo< P4estGrid< dim, dimworld, elType, ct > >
  {
    static int refineStepsForHalf ()
    {
      return 1 ;
    }

    static double refineWeight ()
    {
      return 1.0 / ( std::pow( 2.0, dim ) );
    }
  };

} // namespace Dune

#endif // #ifndef DUNE_P4ESTGRID_DGFPARSER_HH
