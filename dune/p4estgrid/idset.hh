/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_IDSET_HH
#define DUNE_P4ESTGRID_IDSET_HH

#include <dune/common/hash.hh>
#include <dune/grid/common/indexidset.hh>

namespace Dune
{

  // P4estGridIdSet
  // -----------

  template < int dim >
  class P4estEntityId
  {
    // todo add dimension world
    typedef P4est< dim, dim > PForest ;
    typedef typename PForest :: topidx_t  topidx_t;
    typedef typename PForest :: level_t   level_t;
    typedef typename PForest :: gloidx_t  gloidx_t;

    gloidx_t  linearId_ ;
    level_t   level_;
    int8_t    codim_;
    int8_t    direction_;

    typedef P4estEntityInfo< dim > EntityInfo ;

  public:
    P4estEntityId( )
      : linearId_( 0 ),
        level_( 0 ),
        codim_( 0 ),
        direction_( 0 )
    {
    }

    explicit P4estEntityId( const gloidx_t start, const EntityInfo& info )
      : linearId_( info.codimension() == 0 ? start + info.quadId() : PForest::quadrant_linear_id( &info.quadrant(), info.level() ) ),
        level_( info.level() ),
        codim_( info.codimension() ),
        direction_( getDirection( codim_, info.subEntity() ) )
    {}

    explicit P4estEntityId( const EntityInfo& info )
      : linearId_( quadrant_linear_id( &info.quadrant(), info.level() ) ),
        level_( info.level() ),
        codim_( info.codimension() ),
        direction_( getDirection( codim_, info.subEntity() ) )
    {}

    int8_t getDirection( const int codimension, const int subEntity ) const
    {
      return 0;
    }

    bool operator == (const P4estEntityId& other) const
    {
      if( codim_     != other.codim_ )    return false ;
      if( linearId_  != other.linearId_ ) return false ;
      if( level_     != other.level_    ) return false ;
      if( direction_ != other.direction_ ) return false ;

      return true ;
    }

    bool operator != (const P4estEntityId& other) const
    {
      return ! this->operator == ( other );
    }

    // operator < is already implemented in BaseType
    bool operator < (const P4estEntityId& other ) const
    {
      if( codim_ != other.codim_  )
        return ( codim_ < other.codim_ );

      if( linearId_ != other.linearId_ )
        return ( linearId_ < other.linearId_ );

      if( level_ != other.level_ )
        return ( level_ < other.level_ );

      if( direction_ != other.direction_ )
        return direction_ < other.direction_ ;

      return false;
    }

    // operator < is already implemented in BaseType
    bool operator <= (const P4estEntityId& other ) const
    {
      if( this->operator ==(other) )
        return true;

      return this->operator<(other);
    }

    // operator < is already implemented in BaseType
    bool operator > (const P4estEntityId& org) const
    {
      return ( (!this->operator == (org)) && (!this->operator <(org)) );
    }

    bool operator >= (const P4estEntityId& other ) const
    {
      if( this->operator ==(other) )
        return true;

      return this->operator > (other);
    }

    void print( std::ostream& out ) const
    {
      out << "Id [" << linearId_ << ", " << int(level_) << ", "<<
        int(codim_)<< ", " << int(direction_) << "]" << std::endl;
    }

    inline friend std::size_t hash_value(const P4estEntityId& id)
    {
      std::size_t seed = hash<uint64_t>()(id.linearId_);
      hash_combine(seed, id.level_);
      hash_combine(seed, id.codim_);
      hash_combine(seed, id.direction_);
      return seed;
    }

  };

  template <int dim>
  inline std::ostream& operator<< (std::ostream& s, const P4estEntityId< dim >& key)
  {
     key.print(s);
     return s;
  }

} // namespace Dune

DUNE_DEFINE_HASH(DUNE_HASH_TEMPLATE_ARGS(int dim),DUNE_HASH_TYPE(Dune::P4estEntityId<dim>))

namespace Dune
{
  template< class Grid >
  class P4estGridIdSet
  : public IdSet< Grid, P4estGridIdSet< Grid >, size_t >
  {
    typedef IdSet< Grid, P4estGridIdSet< Grid >, size_t > Base;

    typedef typename std::remove_const< Grid >::type::Traits Traits;

  public:
    typedef P4estEntityId< Grid:: dimension >  IdType;

    using Base::subId;

    P4estGridIdSet ( const Grid& grid )
    : grid_( grid )
    {}

    //! id meethod for entity and specific codim
    template< int codim >
    IdType id ( const typename Traits::template Codim< codim >::Entity &entity ) const
    {
      auto start = grid_.forest()->global_first_quadrant[ grid_.comm().rank() ];
      return IdType( start, entity.impl().info() ) ;
    }

    //! id method of all entities
    template< class Entity >
    IdType id ( const Entity &entity ) const
    {
      return id< Entity::codimension >( entity );
    }

    IdType subId ( const typename Traits::template Codim< 0 >::Entity &entity, int i, unsigned int codim ) const
    {
      if( codim == 0 )
        return id( entity );

      // not implemented yet
      assert(false);
      std::abort();
      return IdType( ) ;// hostIdSet_.subId( Grid::template getHostEntity< 0 >( entity ), i, codim );
    }

  private:
    P4estGridIdSet ( const P4estGridIdSet & );
    P4estGridIdSet &operator= ( const P4estGridIdSet & );

    const Grid &grid_;
  };

}

#endif // #ifndef DUNE_P4ESTGRID_IDSET_HH
