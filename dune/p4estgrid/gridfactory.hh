#ifndef DUNE_P4ESTGRID_FACTORY_HH
#define DUNE_P4ESTGRID_FACTORY_HH

#include <dune/grid/common/gridfactory.hh>

#include <dune/p4estgrid/declaration.hh>

#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#endif

namespace Dune
{

  /** \brief Specialization of the GridFactory for P4estGrid
   *  \ingroup GridFactory
   */
  template<int dim, int dimworld, P4estType eltype, class ct >
  class GridFactory< P4estGrid< dim, dimworld, eltype, ct > >
    : public GridFactoryInterface< P4estGrid< dim, dimworld, eltype, ct > >
  {
    typedef GridFactory< P4estGrid< dim, dimworld, eltype, ct > > ThisType;
  public:
    typedef P4estGrid< dim, dimworld, eltype, ct > Grid;
    typedef std::unique_ptr< Grid > GridPtrType;

  protected:
    typedef ALUGrid< dim, dimworld, Dune::cube, Dune::nonconforming, ALUGridNoComm > ALUGridType;
    typedef GridFactory< ALUGridType > ALUGridFactoryType;

  public:
    template< int codim >
    struct Codim
    {
      typedef typename Grid::template Codim< codim >::Entity Entity;
    };

    typedef typename Grid::ctype ctype;

    static const P4estType elementType = Grid::elementType;

    static const unsigned int dimension      = Grid::dimension;
    static const unsigned int dimensionworld = Grid::dimensionworld;

    typedef typename Grid::MPICommunicatorType  MPICommunicatorType;
    typedef typename Grid::Communication        Communication;

    //! \brief type of boundary projection class
    typedef DuneBoundaryProjection< dimensionworld >  DuneBoundaryProjectionType;

    typedef Dune::BoundarySegmentWrapper< dimension, dimensionworld > BoundarySegmentWrapperType;

    typedef unsigned int VertexId;
    typedef unsigned int GlobalIdType;

    // type of vertex coordinates put into the factory
    typedef FieldVector< ctype, dimensionworld > VertexInputType;

    /** \brief default constructor */
    explicit GridFactory ( const MPICommunicatorType &communicator = Grid::defaultCommunicator() )
      : gridFactory_(communicator)
    {}

    /***************************************************************
     *
     * Interface Methods
     *
     ***************************************************************/

    /** \copydoc Dune::GridFactory::insertVertex */
    virtual void insertVertex ( const VertexInputType &pos ) { gridFactory_.insertVertex( pos ); }

    /** \copydoc Dune::GridFactory::insertVertex */
    void insertVertex ( const VertexInputType &pos, const VertexId globalId );

    /** \copydoc Dune::GridFactory::insertElement */
    virtual void
    insertElement ( const GeometryType &geometry,
                    const std::vector< VertexId > &vertices )
    {
      if( ! geometry.isCube() )
      {
        DUNE_THROW(InvalidStateException,"P4estGrid can only handle cube geometries currently!");
      }

      gridFactory_.insertElement( geometry, vertices );
    }

    /** \copydoc Dune::GridFactory::insertBoundary */
    virtual void
    insertBoundary ( const GeometryType &geometry, const std::vector< VertexId > &faceVertices, int boundaryId = 1 )
    {
      gridFactory_.insertBoundary( geometry, faceVertices, boundaryId );
    }

    /** \copydoc Dune::GridFactory::insertBoundary */
    virtual void insertBoundary ( int element, int face, int boundaryId = 1 )
    {
      gridFactory_.insertBoundary(element, face, boundaryId );
    }

    /** \copydoc Dune::GridFactory::insertBoundaryProjection */
    virtual void
    insertBoundaryProjection ( const GeometryType &type,
                               const std::vector< VertexId > &vertices,
                               const DuneBoundaryProjectionType *projection )
    {
      gridFactory_.insertBoundaryProjection( type, vertices, projection );
    }

    /** \copydoc Dune::GridFactory::insertBoundaryProjection */
    virtual void insertBoundaryProjection ( const DuneBoundaryProjectionType& bndProjection, const bool isSurfaceProjection = (dimension != dimensionworld) )
    {
      gridFactory_.insertBoundaryProjection( bndProjection, isSurfaceProjection );
    }

    /** \copydoc Dune::GridFactory::insertBoundarySegement */
    virtual void
    insertBoundarySegment ( const std::vector< VertexId >& vertices )
    {
      gridFactory_.insertBoundarySegment( vertices );
    }

    /** \copydoc Dune::GridFactory::insertBoundarySegment */
    virtual void
    insertBoundarySegment ( const std::vector< VertexId >& vertices,
                            const std::shared_ptr<BoundarySegment<dimension,dimensionworld> >& boundarySegment )
    {
      gridFactory_.insertBoundarySegment( vertices, boundarySegment );
    }

#if 0
    /** \copydoc Dune::GridFactory::insertFaceTransformation  */
    void insertFaceTransformation ( const WorldMatrix &matrix, const WorldVector &shift )
    {
      gridFactory_.insertFaceTransformation( matrix, shift );
    }
#endif

    /** \brief finalize the grid creation and hand over the grid
     *
     *  The caller takes responsibility for deleing the grid.
     */
    GridPtrType createGrid ()
    {
      auto comm = gridFactory_.comm();
      if( comm.rank() == 0 )
      {
        // create ALUGrid and convert to connectivity and destroy ALUGrid
        auto grd = gridFactory_.createGrid();
        return ThisType::createGrid( *grd , comm );
      }
      else
        return ThisType::createGrid( comm );
    }

    /************************************************************************
     *
     *  Non interface methods for DGF
     *
     ************************************************************************/

  protected:
    typedef P4est< dim, dimworld > PForest ;
    typedef typename PForest::connectivity_t  connectivity_t;

    /** Create a P4est connectivity given a DUNE Grid. */
    template <class DuneGrid >
    static connectivity_t* createConnectivity( const DuneGrid& grid )
    {
      // DuneGrid is either ALUGrid or YaspGrid
      static const bool hasTwists = std::is_same< DuneGrid, ALUGridType > :: value;

      typedef typename PForest :: topidx_t topidx_t;
      const size_t numVertices = grid.size(dim);
      // default value for all coordinates is 0.0
      std::vector< double > vertices( numVertices*3, 0.0 );

      auto gv = grid.leafGridView();

      const int nVx = std::pow(2, dim);
      const int nFaces = 2*dim;
      const size_t nElements = grid.size(0);
      std::vector< topidx_t > tree_to_vertex( nElements * nVx );
      std::vector< int8_t   > tree_to_face  ( nElements * nFaces );
      std::vector< topidx_t > tree_to_tree  ( nElements * nFaces );

      const auto& indexSet = gv.indexSet();
      for( const auto& elem :  Dune::elements( gv ) )
      {
        // element vertices
        auto elIdx = indexSet.index( elem );

        auto geometry = elem.geometry();

        assert( nVx == elem.subEntities( dim ) );
        for( int vx=0; vx<nVx; ++vx )
        {
          auto vxIdx = indexSet.subIndex( elem, vx, dim );
          tree_to_vertex[ elIdx*nVx + vx ] = vxIdx;

          auto vertex = geometry.corner( vx );
          for( int d=0; d<dim; ++d )
            vertices[ vxIdx*3 + d ] = vertex[d];
        }

        // element neighbors
        for( const auto& intersection : Dune::intersections( gv, elem ) )
        {
          const int idxInside = intersection.indexInInside();
          if ( intersection.boundary() )
          {
            // boundary faces connect to the same element
            tree_to_tree[ elIdx*nFaces + idxInside ] = elIdx;

            // store face number in the same element
            tree_to_face[ elIdx*nFaces + idxInside ] = idxInside;
          }
          else if( intersection.neighbor() )
          {
            auto outside = intersection.outside();
            auto nbIdx = indexSet.index( outside );
            tree_to_tree[ elIdx*nFaces + idxInside ] = nbIdx;

            int twst = 0;
            if constexpr ( hasTwists )
            {
              twst = std::abs( intersection.impl().twistInInside() - intersection.impl().twistInOutside());
            }

            // store face number in neighboring element
            tree_to_face[ elIdx*nFaces + idxInside ] = 4*twst + intersection.indexInOutside();
          }
        }
      }

      return PForest::connectivity_new_copy( numVertices, nElements,
                                             vertices, tree_to_vertex,
                                             tree_to_tree, tree_to_face );

      /*
      const topidx_t num_ctt = 0;
      if constexpr ( dim == 2 )
      {
        return PForest::connectivity_new_copy (numVertices, nElements, 0,
                                               vertices.data(), tree_to_vertex.data(),
                                               tree_to_tree.data(), tree_to_face.data(),
                                               NULL, &num_ctt, NULL, NULL);
      }

      if constexpr ( dim == 3 )
      {
        return PForest::connectivity_new_copy (numVertices, nElements, 0, 0,
                                               vertices.data(), tree_to_vertex.data(),
                                               tree_to_tree.data(), tree_to_face.data(),
                                               NULL, &num_ctt, NULL, NULL, NULL, NULL);
      }
      */
    }

    /** check that connectivity is valid and distribute to all cores */
    static connectivity_t* checkAndDistribute( connectivity_t* connectivity, const Communication& comm )
    {
      // only on rank 0 should the connectivity be valid
      if( comm.rank() > 0 && connectivity != nullptr)
      {
        DUNE_THROW(InvalidStateException,"P4est: connectivity should only be set on rank 0 before broadcasting!");
      }

      // distribute to all cores
      connectivity = PForest :: connectivity_bcast( connectivity, 0 /* root */, comm );

      // check that connectivity is valid on all cores
      if ( PForest :: connectivity_is_valid( connectivity ) == 0 )
        DUNE_THROW(InvalidStateException,"P4est: connectivity not valid!");

      return connectivity;
    }

    /** Create a P4estGrid give a connectivity. If connectivity is only given on
     * rank 0 then distribute should be true. */
    static GridPtrType createGrid( connectivity_t* connectivity, const Communication& comm, const bool distribute = false )
    {
      if( distribute )
      {
        connectivity = ThisType::checkAndDistribute( connectivity, comm );
      }

      return GridPtrType( new Grid( connectivity ) );
    }

  public:
    /** Create a P4estGrid given a DUNE grid. */
    template <class DuneGrid>
    static GridPtrType createGrid( const DuneGrid& grd, const Communication& comm )
    {
      connectivity_t* connectivity = nullptr;
      // create connectivity on rank 0
      if( comm.rank() == 0 )
      {
        connectivity = createConnectivity( grd );
      }
      return createGrid( connectivity, comm, true );
    }

    /** Create a P4estGrid given a DUNE grid. connectivity is obtained by communication */
    static GridPtrType createGrid( const Communication& comm )
    {
      connectivity_t* connectivity = nullptr;
      return createGrid( connectivity, comm, true );
    }

  protected:
    /** Give a connectivity, print it's content. For debugging. */
    static void printConnectivity( connectivity_t* connect )
    {
      typedef typename PForest :: topidx_t topidx_t;
      std::cout << "Vertices: " << connect->num_vertices << std::endl;
      for( topidx_t i=0; i<connect->num_vertices; ++i )
      {
        std::cout << connect->vertices[ i*3 ] << " " << connect->vertices[ i*3 + 1 ] << " " << connect->vertices[ i*3 + 2 ]  << std::endl;
      }

      std::cout << "tree_to_vertex: " << connect->num_trees << std::endl;
      const int nVx = ( dim == 2 ) ? 4 : 8;
      for( topidx_t i=0; i<connect->num_trees; ++i )
      {
        for( int k=0; k<nVx; ++k )
        {
          std::cout << connect->tree_to_vertex[ i*nVx + k ] << " ";
        }
        std::cout << std::endl;
      }

      std::cout << "tree_to_tree: " << connect->num_trees << std::endl;
      const int nFce = dim * 2;
      for( topidx_t i=0; i<connect->num_trees; ++i )
      {
        for( int k=0; k<nFce; ++k )
        {
          std::cout << connect->tree_to_tree[ i*nFce + k ] << " ";
        }
        std::cout << std::endl;
      }

      std::cout << "tree_to_face: " << connect->num_trees << std::endl;
      for( topidx_t i=0; i<connect->num_trees; ++i )
      {
        for( int k=0; k<nFce; ++k )
        {
          std::cout << int(connect->tree_to_face[ i*nFce + k ]) << " ";
        }
        std::cout << std::endl;
      }

    }

  protected:
    // ALUGrid factory for creation of unstructured grids
    ALUGridFactoryType gridFactory_;
  };

} // namespace Dune
#endif // DUNE_P4ESTGRID_FACTORY_HH
