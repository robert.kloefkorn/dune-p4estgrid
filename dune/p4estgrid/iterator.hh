/*
  This file is part of dune-p4estgrid.
  dune-p4estgrid is a DUNE module to provide an implementation of
  the DUNE grid interface using the p4est library.

  Copyright (C) 2011 Robert Kloefkorn, Martin Nolte, and Carsten Burstedde.

  dune-p4estgrid is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  dune-p4estgrid is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with dune-p4estgrid; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef DUNE_P4ESTGRID_ITERATOR_HH
#define DUNE_P4ESTGRID_ITERATOR_HH

#include <dune/grid/common/entityiterator.hh>
#include <dune/p4estgrid/entitypointer.hh>

namespace Dune
{

  // Iterator
  // --------

  template< class Traits >
  class P4estGridIterator
  : public P4estGridEntityPointer< Traits >
  {
    typedef P4estGridEntityPointer< Traits > Base;
    typedef typename Traits::Grid Grid;

  protected:
    using Base::data;
    using Base::update;
    using Base::info;
    using Base::clear;
    typedef typename Base::ExtraData ExtraData;

    typedef typename Base::EntityInfo EntityInfo;

    typedef Dune :: P4est< Grid :: dimension, Grid :: dimensionworld > PForest ;
    typedef typename PForest :: forest_t    forest_t ;
    typedef typename PForest :: mesh_t      mesh_t ;
    typedef typename PForest :: tree_t      tree_t ;
    typedef typename PForest :: ghost_t     ghost_t ;
    typedef typename PForest :: topidx_t    topidx_t ;
    typedef typename PForest :: quadidx_t   quadidx_t ;
    typedef typename PForest :: quadrant_t  quadrant_t ;

  public:
    static const PartitionIteratorType partitionType = Traits :: partitionType ;

    // constructor for end iterator
    explicit P4estGridIterator( ExtraData data )
    : Base( data ),
      current_( -1 ),
      end_( -1 )
    {
    }

    // constructor for begin iterator
    P4estGridIterator( ExtraData data,
                       const size_t start,
                       const size_t end )
    : Base( data ),
      current_( start ), // set in leafbegin
      end_( end )        // set in leafbegin
    {
      checkValidAndUpdate();
    }

    // constructor for begin iterator
    P4estGridIterator( const P4estGridIterator& other )
    : Base( other ),
      current_( other.current_ ), // set in leafbegin
      end_( other.end_ )
    {
      checkValidAndUpdate();
    }

    P4estGridIterator& operator= ( const P4estGridIterator& other )
    {
      Base::operator=( other );
      current_ = other.current_;
      end_     = other.end_;

      checkValidAndUpdate();
      return *this;
    }


    void checkValidAndUpdate()
    {
      if( current_ < end_ )
        update( data()->element( current_ ) );
      else
        clear();
    }

    template< class T >
    bool equals ( const P4estGridEntityPointer< T > &other ) const
    {
      return Base::equals( other );
    }

    void increment ()
    {
      ++ current_ ;
      checkValidAndUpdate();
    }
  protected:
    size_t current_ ;
    size_t end_;
  };


  // this is disabled because it is not used and not needed
  // when no hierarchy is available
#if 0
  template< class Traits >
  class P4estGridLevelIterator
  : public P4estGridEntityPointer< Traits >
  {
    typedef P4estGridEntityPointer< Traits > Base;
    typedef typename Traits::Grid Grid;

  protected:
    using Base::data;
    using Base::update;
    using Base::info;
    using Base::clear;
    typedef typename Base::ExtraData ExtraData;

    typedef typename Base::EntityInfo EntityInfo;

    typedef Dune :: P4est< Grid :: dimension, Grid :: dimensionworld > PForest ;
    typedef typename PForest :: connectivity_t connectivity_t;
    typedef typename PForest :: quadrant_t  quadrant_t ;
    typedef typename PForest :: topidx_t    topidx_t ;
    typedef typename PForest :: quadidx_t   quadidx_t ;

    typedef typename Grid:: P4estHierarchyType HierarchyType ;
    typedef typename HierarchyType :: ItemType ItemType ;
    typedef typename HierarchyType :: index_t  IndexType ;
  public:
    static const PartitionIteratorType partitionType = Traits :: partitionType ;

    // constructor for end iterator
    explicit P4estGridLevelIterator( ExtraData data )
      : Base( data ),
        level_( -1 ),
        current_( -1 ), // set in lbegin
        end_( -1 )
    {
    }

    // constructor for begin iterator
    P4estGridLevelIterator( ExtraData data,
                            const int level,
                            const size_t start,
                            const size_t end )
    : Base( data ),
      level_( level ),
      current_( start ),
      end_( end )
    {
      if( ! checkValidAndUpdate() )
        increment();
    }

    // constructor for begin iterator
    P4estGridLevelIterator( const P4estGridLevelIterator& other )
    : Base( other ),
      level_( other.level_ ),
      current_( other.current_ ), // set in leafbegin
      end_( other.end_ )
    {
    }

    P4estGridLevelIterator& operator= ( const P4estGridLevelIterator& other )
    {
      Base::operator=( other );
      level_   = other.level_;
      current_ = other.current_;
      end_  = other.end_;
      return *this;
    }

    bool checkValidAndUpdate()
    {
      if( current_ < end_ )
      {
        const auto& info = data()->element( current_ );
        if( info.level() == level_ )
        {
          update( info );
          return true;
        }

        return false;
      }
      else
      {
        clear();
        return true;
      }
    }

    void increment ()
    {
      ++ current_ ;
      if( ! checkValidAndUpdate() )
        increment();
    }

    template< class T >
    bool equals ( const P4estGridEntityPointer< T > &other ) const
    {
      return Base::equals( other );
    }
  protected:
    int level_ ;
    size_t current_ ;
    size_t end_;
  };
#endif


  // P4estGridLeafIteratorTraits
  // ---------------------------

  template< int codim, PartitionIteratorType pitype, class Grid >
  struct P4estGridLeafIteratorTraits
  : public P4estGridEntityPointerTraits< codim, Grid >
  {
    static const PartitionIteratorType partitionType = pitype ;
  };


#if 0
  // P4estGridLevelIteratorTraits
  // -------------------------

  template< int codim, PartitionIteratorType pitype, class Grid >
  struct P4estGridLevelIteratorTraits
  : public P4estGridEntityPointerTraits< codim, Grid >
  {
    static const PartitionIteratorType partitionType = pitype ;
  };
#endif

} // namespace Dune

#endif // #ifndef DUNE_P4ESTGRID_ITERATOR_HH
