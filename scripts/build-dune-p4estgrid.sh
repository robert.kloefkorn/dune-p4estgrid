#!/bin/bash

WORKDIR=`pwd`
echo "This script will download and build all DUNE modules"
echo "necessary to run the examples in dune-p4estgrid."
echo
echo "The installation directory is: $WORKDIR"
echo "Some third party libraries have to be downloaded manually."
echo "Please take a look at this script for parameters and options."
echo
read -p "Install DUNE modules to $WORKDIR? (Y/N) " YN
if [ "$YN" != "Y" ] ;then
  exit 1
fi

if [ "$P4EST_DIR" == "" ]; then
  echo "p4est installation directory needed! export P4EST_DIR=path_to_p4est"
  exit 1
fi

#change appropriately, i.e. 2.10 or empty (which refers to master)
DUNEVERSION=
URL=https://gitlab.dune-project.org

# your favorite compiler optimization flags
FLAGS="-Ofast -DNDEBUG -Wall"

# dune modules needed to build dune-p4estgrid
DUNEMODULES="dune-common dune-geometry dune-grid dune-alugrid dune-p4estgrid"

echo "
MAKE_FLAGS=-j12 \\
CMAKE_FLAGS=\"\\
  -DCMAKE_BUILD_TYPE=Release \\
  -DCMAKE_POSITION_INDEPENDENT_CODE=ON \\
  -DDUNE_GRID_GRIDTYPE_SELECTOR=ON \\
  -DDUNE_ENABLE_PYTHONBINDINGS=OFF \\
  -DCMAKE_DISABLE_FIND_PACKAGE_Vc=TRUE \\
  -Dp4est_ROOT=$P4EST_DIR \"" > config.opts

DUNEBRANCH=
if [ "$DUNEVERSION" != "" ] ; then
  DUNEBRANCH="-b releases/$DUNEVERSION"
fi

# get all dune modules necessary
for MOD in $DUNEMODULES ; do
  if [ "$MOD" == "dune-alugrid" ] ; then
    EXT=extensions
  elif [ "$MOD" == "dune-p4estgrid" ] ; then
    EXT=robert.kloefkorn
  else
    EXT=core
  fi
  git clone $URL/$EXT/$MOD
done

# build all DUNE modules in the correct order
./dune-common/bin/dunecontrol --opts=config.opts all
